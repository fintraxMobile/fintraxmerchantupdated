﻿var express = require('express');
var router = express.Router();
var url = require('url');
// routes
router.get('/merchants/current', function (req, res) {
    console.log('GET MERCHANTS');
    res.render('ok', {
        name: 'merchants',
        method: 'get'
    });
});
router.head('/merchants/current', function (req, res) {
    console.log('HEAD MERCHANTS');
    res.render('ok', {
        name: 'merchants',
        method: 'head'
    });
});
router.post('/merchants/current', function (req, res) {
    console.log('POST MERCHANTS');
    res.render('ok', {
        name: 'merchants',
        method: 'post'
    });
});

module.exports = router;