﻿"use strict";
var express = require('express');
var http = require('http');
var path = require('path');
var server = express();

server.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS, HEAD');
    //res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization');
    next();
});

server.set('view engine', 'ejs');
server.set('views', path.join(__dirname, 'views'));

server.use(require('./tourist'));
server.use(require('./merchants'));
server.use(require('./vouchers'));

server.listen(1234, function () {

});
