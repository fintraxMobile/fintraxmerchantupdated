module.exports = function(config){
    config.set({

        basePath : './',

        files : [

            "src/bower_components/jquery/dist/jquery.js",
            "src/bower_components/angular/angular.js",
            "src/bower_components/angular-animate/angular-animate.js",
            "src/bower_components/angular-ui-router/release/angular-ui-router.js",
            "src/bower_components/ngCordova/dist/ng-cordova.js",
            "src/bower_components/fastclick/lib/fastclick.js",

            "src/bower_components/angular-jwt/dist/angular-jwt.js",
            "src/bower_components/a0-angular-storage/dist/angular-storage.js",

            "src/bower_components/auth0-angular/build/auth0-angular.js",
            "src/bower_components/angular-ui-select/dist/select.js",
            "src/bower_components/angular-sanitize/angular-sanitize.js",
            "src/bower_components/angular-translate/angular-translate.js",
            "src/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js",
            "src/bower_components/ngDialog/js/ngDialog.js",
            "src/bower_components/angular-toastr/dist/angular-toastr.tpls.js",
            "src/bower_components/cryptojslib/rollups/aes.js",
            "src/bower_components/cryptojslib/rollups/hmac-sha1.js",
            "src/bower_components/cryptojslib/components/enc-base64-min.js",

            "src/bower_components/angulartics/dist/angulartics.min.js",
            "src/bower_components/angulartics-google-analytics/dist/angulartics-google-analytics.min.js",

            'src/bower_components/angular-mocks/angular-mocks.js',

            'src/app/core/core.module.js',
            'src/app/core/services/refundCalculationService.js',
            'test/fixtures/*.json',
            'test/*.js'
        ],

        preprocessors: {
            'test/fixtures/*.json': ['html2js']
        },

        jsonFixturesPreprocessor: {
            // strip this from the file path \ fixture name
            stripPrefix: 'test/fixtures',
            basePath : './'
        },

        autoWatch : true,

        frameworks: ['jasmine'],

        browsers : ['PhantomJS', 'Chrome'],

        plugins : [
            'karma-phantomjs-launcher',
            'karma-chrome-launcher',
            'karma-jasmine',
            'karma-html2js-preprocessor'
        ]
    });
};