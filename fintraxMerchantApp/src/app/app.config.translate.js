(function () {
    'use strict';

    angular
        .module('merchantApp')
        .config(translations);

    function translations($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix: 'app/languages/',
            suffix: '.json'
        }).registerAvailableLanguageKeys(['en', 'fr', 'de', 'es', 'it', 'pt'], {
            'en_US': 'en',
            'en_UK': 'en'
        }).determinePreferredLanguage().fallbackLanguage('en');

    }

})();
