(function () {
    angular
        .module('merchantApp.dashboards')
        .controller('voucherFlexibleDashboardController', Voucher);

    /* @ngInject */
    function Voucher(titleConstants,newVoucherValue, navigationService, commandService, $state, $scope, $q, $log,
        connectivityStatusService, merchantConfig, offlineQueue, fintraxApi,
        translatedToastr, $rootScope, createVoucherService, advancedReceiptValue,dateDropdownConstants,blinkIdPassportScan,utilsService,manateeBarcode) {

        /* jshint validthis: true */
    	$scope.titles=titleConstants;
        var vm = this;
        
        vm.createVoucher = createVoucher;
        vm.premierPassScan = premierPassScan;
        vm.isFromScanning=false;
        vm.scanPassport = scanPassport;
        vm.countries = [];
        vm.customer={};
        vm.country
        vm.dateDropdowns = dateDropdownConstants;
        vm.passportScanEnabled = false;
        vm.searchForTouristUsingPremierPassId=searchForTouristUsingPremierPassId;
        vm.countryChange=genericCountryChange;
        vm.checkpassportNumberCharacters=checkpassportNumberCharacters;

        connectivityStatusService.isOnline().then(
            function(online){
                // We can let the voucher go back to online mode here
                navigationService.offline(!online);

            }
        );

        activate();

        function activate(){
        	 clearVoucher();
            utilsService.setRequiredAndHiddenFieldsOnScope(vm);
            navigationService.setTitle('DASHBOARDS.CREATE_VOUCHER');
            navigationService.pageClass('voucher-dashboard');
            vm.customer.Passport = vm.customer.Passport || {};
            commandService.removeAllButtons();
            commandService.addButton('COMMANDS.NEXT', next, 'right', false,isDataInvalid);
            commandService.addSvgButton('home', back, 'left'); 
            $scope.$on('$datevalidator', validatorForDates);
         // commandService.removeAllButtons();
            //commandService.addSvgButton('back', back, 'left');
           // commandService.removeButtonThatHasText('back');
         //  commandService.replaceIcon('back','settings');
            //commandService.addButton('COMMANDS.SETTINGS', next, 'left', false, function () { return true; },'svg-icon','settings');
           
            
          //  commandService.addButton('COMMANDS.NEXT', next, 'right', true, function () { return $scope.myForm.$invalid; });
            $scope.$on('$destroy', commandService.restoreDefaultButtons);


            
            fintraxApi.getCountries().then(function(countries){
                vm.countries = countries;
            });

        }
        
        function validatorForDates(attributes,condition)
        {
        	
        	utilsService.dateValidateMessenger(condition);
        	
        }
        function genericCountryChange()
        {
        	
          if(!vm.isFromScanning)
        	  {
        	  
        	  
        	  if(angular.isUndefined(vm.customer.Address))
        	      {
        		  
        		  	vm.customer.Address={};
        		  	vm.customer.Address.Country=vm.customer.Passport.IssuingCountry;
        		  
        		  
        		  
        		  }
        	  
        	  if(angular.isUndefined(vm.customer.Passport.IssuingCountry ))
        	  	{
        		  
        		  
        		  	vm.customer.Passport.IssuingCountry =  vm.customer.Address.Country
        		  
        	  	}
        	  
        	  }
        	
        }
        
        
        function checkpassportNumberCharacters()
        {
        	
        if(!angular.isUndefined(vm.customer.Passport.Number))
        	
        	{
        	
        	vm.customer.Passport.Number=utilsService.removeSpecialCharacters(vm.customer.Passport.Number);	
        	
        	
        	}
        
       
        	
        	
        }
        
        
       

        function back() {
            $state.transitionTo('navigation.home');
        }
        
        function next() {
        	newVoucherValue.Tourist=vm.customer;
        	 navigationService.pageValidityChanged(true, undefined, 'navigation.receiptList');
             navigationService.gotoNextState();

        }


        function goToPremierPassScan(){

            clearVoucher();
            
            vm.customer={};

            connectivityStatusService.isOnline().then(function(online){

                navigationService.offline(!online);

                if(online){

                    merchantConfig.getMerchantConfig().then(function (mc) {

                        if(mc.Store.AllowOfflineVouchers){

                            return refreshVoucherRangeIfNeeded(online, mc)
                                .then(warnIfVoucherRangeIsExhausted)
                                .then(function () {
                                    $state.go('navigation.premierpass');
                                });
                        } else {

                            $state.go('navigation.premierpass');
                        }
                    });

                } else {
                    translatedToastr.error('TOAST.CANT_SCAN_OFFLINE');
                }

            });
        }

        
        function premierPassScan(){

            if(utilsService.isOnWindowsPlatform()){
              cordova.plugins.barcodeScanner.scan(stoppedScanning,
                   function(){
                     stoppedScanning(false);
                   }
                );
            } else {
if (device.platform=="Android") {
  cordova.plugins.barcodeScanner.scan(stoppedScanning,
       function(){
         stoppedScanning(false);
       }
    );
}
else {
manateeBarcode.startScanning(stoppedScanning);
}


            }

        }
        
        function isDataInvalid() {
           
            //Returns true if there are no items, the receipt number / date is missing or if an item is incomplete.
            return  $scope.myForm.$invalid;
        }

        function stoppedScanning(result){

            if(result === false){
                translatedToastr.warning('TOAST.NO_CORDOVA', 'TOAST.ERROR', {preventDuplicates:false});
            } else if(result && result.type !== 'Cancel' && !result.cancelled){
                if(result.code || result.text){

                    var code = result.code || result.text;
                    var premierPassId = code.split('|')[0];
                    vm.premierPassNumber = premierPassId;
                    searchForTourist(premierPassId);

                } else {
                    $log.error('Unable to open barcode scanner');
                    translatedToastr.warning('PREMIER_PASS.SCAN_FAILED', 'TOAST.ERROR', { preventDuplicates:false });
                }
            }

        }

        function searchForTourist(premierPassId){

            connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(online){

                if(online){

                    fintraxApi.getTourist(premierPassId).then(getTouristResponse, getTouristError);
                } else {

                    translatedToastr.error('TOAST.CANT_SCAN_OFFLINE');
                    navigationService.endWorkflow();
                    $state.go('navigation.home');
                }
            });

        }
        
        function searchForTouristUsingPremierPassId()
        {
        	
        	searchForTourist(vm.premierPassNumber);
        	
        	
        }
        
        
        function getTouristResponse(response){
            newVoucherValue.Tourist = response;
             vm.isFromScanning=true;
             response.Passport.ExpirationDate=utilsService.validExpiryDateforPast( response.Passport.ExpirationDate);
             vm.customer=response;
           
            utilsService.requiredFieldsArePresent(newVoucherValue).then(function (allPresent) {

                if (allPresent) {
                	               	              
                 //   navigationService.pageValidityChanged(true, undefined, 'navigation.shopperDetails');
                    translatedToastr.success('PREMIER_PASS.TOURIST_FOUND');
                } else {

                   // navigationService.pageValidityChanged(true, undefined, 'navigation.passportdetails');
                    translatedToastr.warning('PREMIER_PASS.TOURIST_FOUND_MISSING_FIELDS');
                }

              //  navigationService.gotoNextState();
            });

        }

        function getTouristError(error) {
            console.error('TOURIST_ERROR',error);
            translatedToastr.error('PREMIER_PASS.TOURIST_NOT_FOUND', 'TOAST.WARNING', { preventDuplicates:false });
        }
        function createVoucher() {

            clearVoucher();

            return connectivityStatusService.isOnline().then(function (online) {

                navigationService.offline(!online);

                merchantConfig.getMerchantConfig().then(function (mc) {

                    if(mc.Store.AllowOfflineVouchers){

                        return refreshVoucherRangeIfNeeded(online, mc)
                            .then(warnIfVoucherRangeIsExhausted)
                            .then(startVoucherCreation);

                    } else {

                        if(online){

                            return startVoucherCreation();

                        } else {

                            translatedToastr.error('TOAST.NOT_ALLOWED_OFFLINE');
                        }

                    }

                });

            });
        }

        function refreshVoucherRangeIfNeeded(online, mc) {

            if (online) {

                var rangeCount = mc.VoucherRanges ? mc.VoucherRanges.length : 0;

                if (rangeCount <= 2) {

                    return fintraxApi.getVoucherRanges(mc.Store.PtfShopNumber).then(function (voucherRanges) {

                        $log.debug('Refresh Voucher Range If Needed. Got Voucher Range: ' + voucherRanges);

                        console.log('voucherRanges', voucherRanges);

                        // check voucherRanges aren't extinguished
                        if ( angular.isDefined( voucherRanges ) && voucherRanges.length && voucherRanges.length > 0 ) {

                            return merchantConfig.replaceVoucherRanges(voucherRanges);

                        } else {

                            translatedToastr.warning('TOAST.CONTACT_FINTRAX_FOR_MORE_VOUCHER_RANGES');
                            return mc;
                        }

                    }, function ( e ) {

                        translatedToastr.error('TOAST.UNABLE_TO_GET_MORE_VOUCHER_RANGES');

                    });

                } else {

                    $log.debug('Voucher range refresh not needed as you have enough vouchers');
                }

            } else {

                $log.debug('Unable to refresh voucher ranges due to unprocessed items in the queue or being offline.');
            }

            return $q.when(mc); //wrap up MC as a resolved promise so that we can chain this method
        }


        function warnIfVoucherRangeIsExhausted(mc) {

            var numVouchers = mc.VoucherRanges ? mc.VoucherRanges.length : 0;

            $log.debug('Vouchers remaining: ' + numVouchers);

            if (numVouchers <= 2 && navigationService.offline()) {

                if (numVouchers === 0) {

                    translatedToastr.error('TOAST.NO_VOUCHER_RANGES');

                    return $q.reject('you\'re offline, and have no more vouchers');

                } else {

                    translatedToastr.warning('TOAST.VOUCHER_RANGE_WARNING');
                }

            }

            return $q.defer().resolve();
        }


        function startVoucherCreation() {

            $state.go('navigation.passportscan');
        }
        
        
        
        function scanPassport(){
        	
        	  vm.customer={};
        	  vm.customer.Passport={};
            blinkIdPassportScan.scanPassport(vm.countries, success, failure);

            function success(result){
            	 vm.isFromScanning=true;
            	 vm.customer.Passport={};
                translatedToastr.success('TOAST.PASSPORT_SCAN_SUCCESS');

                vm.customer.Passport.IssuingCountry = result.countryOfIssue;

                //assume customer is from where the passport is issued
                vm.customer.Nationality = result.countryOfIssue;
                vm.customer.Passport.Number = utilsService.removeSpecialCharacters(result.passportNumber);
                
               //we need to update the plugin
                vm.customer.Passport.ExpirationDate= utilsService.validDate(result.dateOfExpiry);
                // vm.customer.Passport.ExpirationDate = result.dateOfExpiry;
                vm.customer.FirstName = result.firstName;
                vm.customer.LastName = result.lastName;
                vm.customer.DateOfBirth = result.dateOfBirth
               // vm.customer.DateOfBirth = result.dateOfBirth;

              //  next();
            }

            function failure(result){

                if(result !== 'cancelled'){
                    translatedToastr.error('TOAST.PASSPORT_SCAN_ERROR');
                }

            }
        }

        function clearVoucher() {

            //Wipe any existing voucher and create a new one.
            for (var variableKey in newVoucherValue) {
                if (newVoucherValue.hasOwnProperty(variableKey)) {
                    delete newVoucherValue[variableKey];
                }
            }

            //kill the value used on the 'add reciept' screen
            advancedReceiptValue.receipt = {};

            //also kill the result from the last successful API call
            createVoucherService.clearVoucherCreationResult();

            //also clear the footer refund amount
            $rootScope.$broadcast('RECALCULATE_REFUND', {});
        }

    }
    
})();