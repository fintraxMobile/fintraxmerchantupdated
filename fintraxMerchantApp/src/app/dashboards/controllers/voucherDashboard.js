(function () {
    angular
        .module('merchantApp.dashboards')
        .controller('VoucherDashboardController', Voucher);

    /* @ngInject */
    function Voucher($timeout,newVoucherValue, navigationService, commandService, $state, $scope, $q, $log,
        connectivityStatusService, merchantConfig, offlineQueue, fintraxApi,
        translatedToastr, $rootScope, createVoucherService, advancedReceiptValue) {

        /* jshint validthis: true */
        var vm = this;
        vm.createVoucher = createVoucher;
        vm.goToPremierPassScan = goToPremierPassScan;

        connectivityStatusService.isOnline().then(
            function(online){
                // We can let the voucher go back to online mode here
                navigationService.offline(!online);

            }
        );
        var connectionPoll;
        activate();

        function activate(){

            navigationService.setTitle('DASHBOARDS.CREATE_VOUCHER');
            navigationService.pageClass('voucher-dashboard');
            
         
            
           
       
          
            commandService.removeAllButtons();
            //For Uk Only
            if(localStorage.profile)
        	{
        
        		if(angular.fromJson(localStorage.profile).isoCountryCode==826)
    	   
        			{
        			   // clearVoucher();
        				commandService.shopperEnableMenu(false);
    	   
    	   
        			}
       
      
        	}
            commandService.addSvgButton('back', back, 'left');

            $scope.$on('$destroy', destroy);
            //check connection for all screen
            //checkConnection();
        }

        function back() {
            $state.transitionTo('navigation.home');
        }

        function destroy() {
            commandService.restoreDefaultButtons();

            if (typeof connectionPoll !== 'undefined') {
                $timeout.cancel(connectionPoll);
            }
        }

        function checkConnection() {
            return connectivityStatusService.isOnline().then(function(online) {
                if (online) {
                    connectionPoll = undefined;
                    return navigationService.offline(false);
                }

                navigationService.offline(true);

                connectionPoll = $timeout(function() {
                    return checkConnection();
                }, 1000);
            });
        }

        function goToPremierPassScan(){

            clearVoucher();

            connectivityStatusService.isOnline().then(function(online){

                navigationService.offline(!online);

                if(online){

                    merchantConfig.getMerchantConfig().then(function (mc) {

                        if(mc.Store.AllowOfflineVouchers){

                            return refreshVoucherRangeIfNeeded(online, mc)
                                .then(warnIfVoucherRangeIsExhausted)
                                .then(function () {
                                    $state.go('navigation.premierpass');
                                });
                        } else {

                            $state.go('navigation.premierpass');
                        }
                    });

                } else {
                    translatedToastr.error('TOAST.CANT_SCAN_OFFLINE');
                }

            });
        }

        function createVoucher() {

            clearVoucher();

            return connectivityStatusService.isOnline().then(function (online) {

                navigationService.offline(!online);

                merchantConfig.getMerchantConfig().then(function (mc) {

                    if(mc.Store.AllowOfflineVouchers){

                        return refreshVoucherRangeIfNeeded(online, mc)
                            .then(warnIfVoucherRangeIsExhausted)
                            .then(startVoucherCreation);

                    } else {

                        if(online){

                            return startVoucherCreation();

                        } else {

                            translatedToastr.error('TOAST.NOT_ALLOWED_OFFLINE');
                        }

                    }

                });

            });
        }

        function refreshVoucherRangeIfNeeded(online, mc) {

            if (online) {

                var rangeCount = mc.VoucherRanges ? mc.VoucherRanges.length : 0;

                if (rangeCount <= 2) {

                    return fintraxApi.getVoucherRanges(mc.Store.PtfShopNumber).then(function (voucherRanges) {

                        $log.debug('Refresh Voucher Range If Needed. Got Voucher Range: ' + voucherRanges);

                        console.log('voucherRanges', voucherRanges);

                        // check voucherRanges aren't extinguished
                        if ( angular.isDefined( voucherRanges ) && voucherRanges.length && voucherRanges.length > 0 ) {

                            return merchantConfig.replaceVoucherRanges(voucherRanges);

                        } else {

                            translatedToastr.warning('TOAST.CONTACT_FINTRAX_FOR_MORE_VOUCHER_RANGES');
                            return mc;
                        }

                    }, function ( e ) {

                        translatedToastr.error('TOAST.UNABLE_TO_GET_MORE_VOUCHER_RANGES');

                    });

                } else {

                    $log.debug('Voucher range refresh not needed as you have enough vouchers');
                }

            } else {

                $log.debug('Unable to refresh voucher ranges due to unprocessed items in the queue or being offline.');
            }

            return $q.when(mc); //wrap up MC as a resolved promise so that we can chain this method
        }


        function warnIfVoucherRangeIsExhausted(mc) {

            var numVouchers = mc.VoucherRanges ? mc.VoucherRanges.length : 0;

            $log.debug('Vouchers remaining: ' + numVouchers);

            if (numVouchers <= 2 && navigationService.offline()) {

                if (numVouchers === 0) {

                    translatedToastr.error('TOAST.NO_VOUCHER_RANGES');

                    return $q.reject('you\'re offline, and have no more vouchers');

                } else {

                    translatedToastr.warning('TOAST.VOUCHER_RANGE_WARNING');
                }

            }

            return $q.defer().resolve();
        }


        function startVoucherCreation() {

          //  $state.go('navigation.simpleReceipt');
        	$state.go('navigation.passportscan');
        }

        function clearVoucher() {

            //Wipe any existing voucher and create a new one.
            for (var variableKey in newVoucherValue) {
                if (newVoucherValue.hasOwnProperty(variableKey)) {
                    delete newVoucherValue[variableKey];
                }
            }

            //kill the value used on the 'add reciept' screen
            advancedReceiptValue.receipt = {};

            //also kill the result from the last successful API call
            createVoucherService.clearVoucherCreationResult();

            //also clear the footer refund amount
            $rootScope.$broadcast('RECALCULATE_REFUND', {});
        }




    }
})();
