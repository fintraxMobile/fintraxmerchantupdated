(function() {
    angular
        .module('merchantApp.dashboards')
        .controller('HomeDashboardController', Home);

    /* @ngInject */
    function Home($scope, $timeout, navigationService, commandService, ngDialog, connectivityStatusService, utilsService,merchantConfig,merchantAppConfig,$window,authentication) {
        /* jshint validthis: true */
        //var vm = this;

        var connectionPoll;

        activate();

        function activate() {
            navigationService.setTitle('DASHBOARDS.HOME');
            navigationService.pageClass('home');
            $window.localStorage.getItem('FlexibleOrientation')=='true'? $scope.navigation="navigation.voucherdashboardflexible":$scope.navigation="navigation.voucherdashboard";  
            commandService.removeAllButtons();
            commandService.addSvgButton('settings', openSettings, 'right');

            $scope.$on('$destroy', destroy);

            //checkConnection();
        }

        function openSettings() {
            ngDialog.open({
                template: 'app/dashboards/templates/settingsDialog.html',
                className: 'ngdialog-theme-plain',
                showClose: false,
                controller: 'settingsDialogController',
                //because the animations are screwy on windows
                disableAnimation: utilsService.isOnWindowsPlatform()
            });
        	
                       
            
        }

        function destroy() {
            commandService.restoreDefaultButtons();

            if (typeof connectionPoll !== 'undefined') {
                $timeout.cancel(connectionPoll);
            }
        }

        function checkConnection() {
            return connectivityStatusService.isOnline().then(function(online) {
                if (online) {
                    connectionPoll = undefined;
                    return navigationService.offline(false);
                }

                navigationService.offline(true);

                connectionPoll = $timeout(function() {
                    return checkConnection();
                }, 1000);
            });
        }


    }
})();
