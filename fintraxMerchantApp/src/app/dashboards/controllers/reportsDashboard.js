(function () {
    angular
        .module('merchantApp.dashboards')
        .controller('ReportsDashboardController', Reports);

    /* @ngInject */
    function Reports(navigationService, commandService, $state, $scope) {
        /* jshint validthis: true */
        //var vm = this;

        activate();

        function activate(){

            //this dashboard is not in use for the initial phase
            $state.transitionTo('navigation.reportcustom');

            navigationService.setTitle('DASHBOARDS.REPORTS');
            navigationService.pageClass('reports dashboard');

            commandService.removeAllButtons();
            commandService.addSvgButton('back', back, 'left');
            $scope.$on('$destroy', commandService.restoreDefaultButtons);
        }

        function back()
        {
            $state.transitionTo('navigation.home');
        }
    }
})();