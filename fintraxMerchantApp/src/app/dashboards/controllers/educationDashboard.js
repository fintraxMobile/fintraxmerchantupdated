(function () {
    angular
        .module('merchantApp.dashboards')
        .controller('EducationDashboardController', Education);

    /* @ngInject */
    function Education(navigationService, commandService, $state, $scope) {
        /* jshint validthis: true */
        //var vm = this;

        activate();

        function activate(){
           // vm.createEducation = createEducation;
            navigationService.setTitle('EDUCATION.DASHBOARD_TITLE');
            navigationService.pageClass('education-dashboard');

            commandService.removeAllButtons();
            commandService.addSvgButton('back', back, 'left');

            $scope.$on('$destroy', commandService.restoreDefaultButtons);
        }

        function back() {
            $state.transitionTo('navigation.home');
        }



    }
})();