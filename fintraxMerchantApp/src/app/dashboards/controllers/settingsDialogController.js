(function () {
    angular
        .module('merchantApp.dashboards')
        .controller('settingsDialogController', settingsDialogController);

    /* @ngInject */
    function settingsDialogController($scope, $window, $state, authentication, merchantConfig,
                                      offlineQueue, ngDialog, cacheService, utilsService, merchantAppConfig) {

        //using $scope here as the 'controller as' syntax doesn't work for ngDialog
        $scope.logout = logout;
        $scope.address = [];
        $scope.voucherFormulas = [];
        $scope.settings = {};
        $scope.showWalkthrough = false;
        $scope.walkthroughChanged = walkthroughChanged;
        $scope.showWalkthroughNow = showWalkthroughNow;
        $scope.version = $window.config.version || 'N/A';
        $scope.build = $window.config.build || 'N/A';

        activate();


        function activate() {
            merchantConfig.getMerchantConfig().then(function (config) {
                $scope.merchantConfig = config;

                var address = [];
                //pull address out of config for easier use in the template
                for (var name in config.Store) {

                    if (config.Store.hasOwnProperty(name) &&
                        name.indexOf('Address') > -1 && config.Store[name] !== null) {
                        address.push(config.Store[name]);
                    }
                }

                $scope.address = address;

                config.Settings.forEach(function(setting){

                    switch(setting.Name){

                        case 'VipRefundAllowed':
                            $scope.settings.vipRefundAllowed =  setting.Value;
                            break;

                        case 'AddReceiptScenario':
                            $scope.settings.addReceiptScenario =  setting.Value;
                            break;

                        case 'EnableTouristSignature':
                            $scope.settings.enableTouristSignature =  setting.Value;
                            break;

                        case 'EnableMerchantSignature':
                            $scope.settings.enableMerchantSignature =  setting.Value;
                            break;
                    }

                });

                findAndSetRefundTableIds(config);

                if(config.VoucherFormulas){
                    $scope.voucherFormulas = config.VoucherFormulas;
                }

                $scope.showWalkthrough = $window.localStorage.getItem('hideWalkthrough') !== 'true';

            });
        }

        function findAndSetRefundTableIds(config){
            $scope.settings.refundTableIds = 'N/A';

            var uniqueIds = [];

            config.ChargeStructures.map(function(item){

                if(!uniqueIds.some(
                        function(existing){
                            return existing === item.Id;
                        }
                    )){

                        uniqueIds.push(item.Id);
                    }

            });

            if(uniqueIds.length > 0) {

                $scope.settings.refundTableIds = uniqueIds.join(',');
            }
        }

        function walkthroughChanged(){

            $window.localStorage.setItem('hideWalkthrough', !$scope.showWalkthrough);
        }

        function logout() {
            if (offlineQueue.queueLength() > 0) {
                ngDialog.open({
                    template: 'app/dashboards/templates/nonEmptyQueueDialog.html',
                    className: 'ngdialog-theme-plain',
                    //because the animations are screwy on windows
                    disableAnimation: utilsService.isOnWindowsPlatform(),
                    showClose: false,
                    data: { queueLength: offlineQueue.queueLength() }
                }).closePromise.then(function (data) {
                    if (data.value && data.value !== '$document') {
                        doLogout();
                    }
                });
            } else {
                doLogout();
            }
        }

        function showWalkthroughNow(){
            $state.go('navigation.walkthrough');
            $scope.closeThisDialog(true);
        }

        function doLogout() {
        	utilsService.clearStorages();
            cacheService.destroyDatabase().then(function () {
                offlineQueue.init(); //remove in-memory queue
                authentication.logout();
                merchantConfig.removeMerchantConfig();
                $state.go('login');
                $scope.closeThisDialog(true);
            });
        }

    }
})();
