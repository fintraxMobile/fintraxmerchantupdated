(function () {
    'use strict';

    angular
        .module('merchantApp.dashboards')
        .config(configureRoutes);

    function configureRoutes($stateProvider) {

        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.home', {
            url: '^/home',
            controller: 'HomeDashboardController as home',
            templateUrl: 'app/dashboards/templates/home.html',
            /* @ngInject */
            onEnter: function($state, authentication){

                //if the user's not authenticated, they need to log in
                if(!authentication.isAuthenticated()){
                    $state.go('login');
                }
            }
        }).state('navigation.voucherdashboard', {
            url: '^/voucherdashboard',
            controller: 'VoucherDashboardController as vm',
            templateUrl: 'app/dashboards/templates/voucher.html'
        }).state('navigation.reports', {
            url: '^/reports',
            controller: 'ReportsDashboardController as vm',
            templateUrl: 'app/dashboards/templates/reports.html'
        }).state('navigation.education', {
            url: '^/education',
            controller: 'EducationDashboardController as vm',
            templateUrl: 'app/dashboards/templates/education.html'
        }).state('navigation.voucherdashboardflexible', {
            url: '^/voucherdashboardflexible',
            controller: 'voucherFlexibleDashboardController as vm',
            templateUrl: 'app/dashboards/templates/voucherflexible.html'
        });
    }

})();
