﻿(function () {
    'use strict';
    angular
        .module('merchantApp.tourist')
        .controller('createAccountDialogController', Controller);

    function Controller($scope) {
        if (angular.isDefined($scope.ngDialogData.customer.createAccount) === false) {
            $scope.ngDialogData.customer.createAccount = true;
        }
    }
})();

