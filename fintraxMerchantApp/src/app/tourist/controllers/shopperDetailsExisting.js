(function () {
    angular
        .module('merchantApp.tourist')
        .controller('shopperDetailsExistingController', controller);

    function controller(newVoucherValue, navigationService) {
        /* jshint validthis: true */
        var vm = this;
        navigationService.pageClass('refundReview');

        vm.voucher = newVoucherValue;

        navigationService.pageValidityChanged(true, undefined, 'navigation.receiptList');
    }
})();