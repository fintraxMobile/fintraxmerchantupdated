﻿(function () {
    'use strict';
    angular
        .module('merchantApp.tourist')
        .controller('touristAddressController', Controller);

    function Controller(navigationService, newVoucherValue, commandService, $log, translatedToastr,
                        ngDialog, $scope, connectivityStatusService, createTouristService,
                        fintraxApi, merchantConfig, utilsService) {
        /* jshint validthis: true */

        var vm = this;
        vm.countries = [];
        activate();

        function activate() {

            if (angular.isDefined(newVoucherValue.Tourist) === false) {
                newVoucherValue.Tourist = {};
            }

            if (angular.isDefined(newVoucherValue.Tourist.Address) === false) {
                newVoucherValue.Tourist.Address = {};
            }

            vm.tourist = newVoucherValue.Tourist;

            navigationService.setTitle('NEWCUSTOMER.CUSTOMER_DETAILS_TITLE', 1);

            commandService.addButton('COMMANDS.NEXT', onlyOpenCreateDialogIfNeeded, 'right', true, function () {
                return !vm.isValid;
            });

            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            fintraxApi.getCountries().then(function(countries){
                vm.countries = countries;
            });

            connectivityStatusService.checkOnlineIfOnlineSetNav();
        }

        function onlyOpenCreateDialogIfNeeded() {

            if (!newVoucherValue.Tourist.Id) {

                merchantConfig.getSetting('EnablePTFAccountCreation').then(function (enablePTFAccountCreation) {

                    if(enablePTFAccountCreation){
                        openCreateAccountDialog();

                    } else {

                        vm.tourist.createAccount = false;
                        vm.tourist.email = undefined;

                        navigationService.pageValidityChanged(true, undefined, 'navigation.receiptList');
                        navigationService.gotoNextState();
                    }

                });


            } else {

                //don't need to ask if we're creating a tourist, as it's an existing tourist
                navigationService.pageValidityChanged(true, undefined, 'navigation.receiptList');
                navigationService.gotoNextState();
            }
        }

        function openCreateAccountDialog() {
            var dialog = ngDialog.open({
                template: 'app/tourist/templates/createAccountDialog.html',
                className: 'ngdialog-theme-plain',
                showClose: false,
                controller: 'createAccountDialogController',
                //because the animations are screwy on windows
                disableAnimation: utilsService.isOnWindowsPlatform(),
                data: {
                    'customer': vm.tourist
                }
            }).closePromise.then(function (data) {

                if (data.value && typeof data.value !== 'string' && !(data.value instanceof String)) {

                    if (vm.tourist.createAccount === false) {
                        vm.tourist.email = undefined;

                        navigationService.pageValidityChanged(true, undefined, 'navigation.receiptList');
                        navigationService.gotoNextState();

                    } else {

                        connectivityStatusService.checkOnlineIfOnlineSetNav().then(function (online) {

                            if (online) {

                                //try to create the account now, we'll edit it later
                                attemptTouristCreation();
                            } else {

                                navigationService.pageValidityChanged(true, undefined, 'navigation.receiptList');
                                navigationService.gotoNextState();

                            }
                        });
                    }

                }
            });
        }

        function attemptTouristCreation() {

            createTouristService.fullyCreateTourist(vm.tourist).then(function (tourist) {

                vm.tourist = tourist;
                navigationService.pageValidityChanged(true, undefined, 'navigation.receiptList');
                navigationService.gotoNextState();

            }).catch(function (result) {

                $log.error(result);

                if (result.data && result.data.code === 'user_exists') {
                    translatedToastr.error('VOUCHER_ERROR_CODES.ALREADY_HAS_AUTH0');
                } else {
                    translatedToastr.error('TOAST.TOURIST_CREATION_FAILED');
                }
            });

        }

    }

})();
