(function () {
    'use strict';
    angular
        .module('merchantApp.tourist')
        .controller('passportDetailsController', Controller);

    function Controller(navigationService, newVoucherValue, $scope, translatedToastr, fintraxApi,merchantConfig,commandService,confirmExitVoucherService) {
        /* jshint validthis: true */
        var vm = this;

        if(!newVoucherValue.Tourist)
        	{
        	var Tourist={ };
        	Tourist.Passport={};
        	newVoucherValue.Tourist=Tourist;
        	}
        
        vm.tourist = newVoucherValue.Tourist;
        //for uk merchant clearance
       
        vm.countries = [];
        vm.merchantConfig = merchantConfig.getMerchantConfig();
       //append merchant configuration for dynamic date validation for under age in directory service




        activate();

        function activate() {

            navigationService.pageClass('voucher-dashboard');
            navigationService.setTitle('NEWCUSTOMER.PASSPORTDETAILS_TITLE', 1);
           // commandService.addSvgButton('back', back, 'left');
         //  commandService.addButton('COMMANDS.CLOSE', close, 'left', false,
           //         undefined, commandService.BUTTON_TYPE.tertiary);
           // commandService.removeAllButtons();
            
            
            $scope.$watch('vm.isValid', function (newval, oldval) {
                navigationService.pageValidityChanged(newval, oldval, 'navigation.touristaddress');
            });

            fintraxApi.getCountries().then(function(countries){
                vm.countries = countries;
            });

            checkIfExistingInvalidTourist();
        }

        function checkIfExistingInvalidTourist(){

            if(vm.Tourist && typeof vm.tourist.Id !== 'undefined'){

                //must be an existing customer with missing required fields
                translatedToastr.warning('VALIDATION.EXISTING_MISSING_FIELDS');

                //TODO, ideally we need to automatically trigger validation at this point,
                //so the missing fields are highlighted
            }

        }
        
        function close() {

            confirmExitVoucherService.confirmExit().then(function(result){

                //checking for index 1 for cordova, true for web dev
                if(result === 1 || result === true){
                    navigationService.endWorkflow();
                    $state.go('navigation.home');
                }
            });

        }


    }
})();
