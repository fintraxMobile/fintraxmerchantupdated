(function () {
    'use strict';

    angular
        .module('merchantApp.tourist')
        .controller('premierPassController', premierPassController);


    /* @ngInject */
    function premierPassController(navigationService, commandService, $log, $state, $scope, utilsService,
                                   fintraxApi, newVoucherValue, translatedToastr, manateeBarcode,
                                   connectivityStatusService, confirmExitVoucherService) {

        /* jshint validthis: true */
        var vm = this;
        vm.premierPassScan = premierPassScan;

        //to avoid scoping issue
        vm.premierPass = {};
        vm.premierPassNumber =  '';

        activate();

        function activate() {

            connectivityStatusService.checkOnlineIfOnlineSetNav();

            navigationService.pageClass('premier-pass');
            navigationService.setTitle('PREMIER_PASS.PREMIER_PASS_SCAN_TITLE');
            commandService.addSvgButton('back', back, 'left', true);
            commandService.addButton('COMMANDS.NEXT', next, 'right', true, function () { return false; });
            $scope.$on('$destroy', commandService.restoreDefaultButtons);
        }

        function next() {
            searchForTourist(vm.premierPassNumber);
        }

        function back() {

            confirmExitVoucherService.confirmExit().then(function(result){

                //checking for index 1 for cordova, true for web dev
                if(result === 1 || result === true){
                    navigationService.endWorkflow();
                    $state.go('navigation.voucherdashboard');
                }
            });

        }

        function premierPassScan(){

            if(utilsService.isOnWindowsPlatform()){
              cordova.plugins.barcodeScanner.scan(stoppedScanning,
                   function(){
                     stoppedScanning(false);
                   }
                );
            } else {
if (device.platform=="Android") {
  cordova.plugins.barcodeScanner.scan(stoppedScanning,
       function(){
         stoppedScanning(false);
       }
    );
}
else {
manateeBarcode.startScanning(stoppedScanning);
}


            }

        }

        function stoppedScanning(result){

            if(result === false){
                translatedToastr.warning('TOAST.NO_CORDOVA', 'TOAST.ERROR', {preventDuplicates:false});
            } else if(result && result.type !== 'Cancel' && !result.cancelled){
                if(result.code || result.text){

                    var code = result.code || result.text;
                    var premierPassId = code.split('|')[0];
                    vm.premierPassNumber = premierPassId;
                    searchForTourist(premierPassId);

                } else {
                    $log.error('Unable to open barcode scanner');
                    translatedToastr.warning('PREMIER_PASS.SCAN_FAILED', 'TOAST.ERROR', { preventDuplicates:false });
                }
            }

        }

        function searchForTourist(premierPassId){

            connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(online){

                if(online){

                    fintraxApi.getTourist(premierPassId).then(getTouristResponse, getTouristError);
                } else {

                    translatedToastr.error('TOAST.CANT_SCAN_OFFLINE');
                    navigationService.endWorkflow();
                    $state.go('navigation.home');
                }
            });

        }

        function getTouristResponse(response){
            newVoucherValue.Tourist = response;
           	
            //If we have all the details then go straight to shopperDetails screen, but if any required fields are missing then we
            //need to go through the regular new user workflow.
            utilsService.requiredFieldsArePresent(newVoucherValue).then(function (allPresent) {

                if (allPresent) {

                    navigationService.pageValidityChanged(true, undefined, 'navigation.shopperDetails');
                    translatedToastr.success('PREMIER_PASS.TOURIST_FOUND');
                } else {

                    navigationService.pageValidityChanged(true, undefined, 'navigation.passportdetails');
                    translatedToastr.warning('PREMIER_PASS.TOURIST_FOUND_MISSING_FIELDS');
                }

                navigationService.gotoNextState();
            });

        }

        function getTouristError(error) {
            console.error('TOURIST_ERROR',error);
            translatedToastr.error('PREMIER_PASS.TOURIST_NOT_FOUND', 'TOAST.WARNING', { preventDuplicates:false });
        }


    }
})();
