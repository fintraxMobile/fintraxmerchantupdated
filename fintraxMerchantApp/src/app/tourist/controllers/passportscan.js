(function () {

    'use strict';

    angular
        .module('merchantApp.tourist')
        .controller('passportScanController', Controller);

    function Controller(navigationService, newVoucherValue, commandService, $scope, $state, fintraxApi, utilsService,
                        dateDropdownConstants, translatedToastr, connectivityStatusService,
                        blinkIdPassportScan, confirmExitVoucherService) {
        /* jshint validthis: true */

        var vm = this;

        vm.countries = [];
        vm.dateDropdowns = dateDropdownConstants;
        vm.passportScanEnabled = false;
        vm.scanPassport = scanPassport;
        vm.checkpassportNumberCharacters=checkpassportNumberCharacters;
        activate();

        function activate(){

            navigationService.pageClass('passport-scan');
            connectivityStatusService.checkOnlineIfOnlineSetNav();

            if (angular.isDefined(newVoucherValue.Tourist) === false) {
                newVoucherValue.Tourist = {};
            }

            vm.customer = newVoucherValue.Tourist;
            vm.customer.Passport = vm.customer.Passport || {};
            navigationService.setTitle('NEWCUSTOMER.PASSPORTSCAN_TITLE', 1);

            commandService.addSvgButton('back', back, 'left', true);
            commandService.addButton('COMMANDS.NEXT', next, 'right', true, function () { return $scope.myForm.$invalid; });
            
            $scope.$on('$destroy', commandService.restoreDefaultButtons);
           //$state.go('navigation.passportdetails');
            
           // navigationService.pageValidityChanged(true, undefined, 'navigation.passportdetails');
            //navigationService.gotoNextState();
            
            fintraxApi.getCountries().then(function(countries){
                vm.countries = countries;
            });

            utilsService.setRequiredAndHiddenFieldsOnScope(vm);

            if(!utilsService.isOnWindowsPlatform()){
                vm.passportScanEnabled = true;
            }
        }
        
        
        function checkpassportNumberCharacters()
        {
        	
        if(!angular.isUndefined(vm.customer.Passport.Number))
        	
        	{
        	
        	vm.customer.Passport.Number=utilsService.removeSpecialCharacters(vm.customer.Passport.Number);	
        	
        	
        	}
        
          	
        	
        }

        function scanPassport(){

            blinkIdPassportScan.scanPassport(vm.countries, success, failure);

            function success(result){
                translatedToastr.success('TOAST.PASSPORT_SCAN_SUCCESS');

                vm.customer.Passport.IssuingCountry = result.countryOfIssue;

                //assume customer is from where the passport is issued
                vm.customer.Nationality = result.countryOfIssue;
                vm.customer.Passport.Number = result.passportNumber;
                vm.customer.Passport.ExpirationDate = result.dateOfExpiry;
                vm.customer.FirstName = result.firstName;
                vm.customer.LastName = result.lastName;
                vm.customer.DateOfBirth = result.dateOfBirth;

                next();
            }

            function failure(result){

                if(result !== 'cancelled'){
                    translatedToastr.error('TOAST.PASSPORT_SCAN_ERROR');
                }

            }
        }

        function next() {

            if(vm.customer.Passport.Number && vm.customer.Passport.IssuingCountry){

                connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(online){

                    if(online){
                        fintraxApi.getTouristFromPassport(vm.customer.Passport.Number,vm.customer.Passport.IssuingCountry.Iso3Digit)
                            .then(getTouristResponse, getTouristError);
                    } else {
                        getTouristError();
                    }

                });
            } else {

                //skip passport lookup if it's not required and hasn't been entered.
                navigationService.pageValidityChanged(true, undefined, 'navigation.passportdetails');
                navigationService.gotoNextState();
            }

        }

        function back() {

            confirmExitVoucherService.confirmExit().then(function(result){

                //checking for index 1 for cordova, true for web dev
                if(result === 1 || result === true){
                    navigationService.endWorkflow();
                    $state.go('navigation.voucherdashboard');
                }
            });

        }

        function getTouristResponse(response) {
            console.log('Back in the controller we saw success!');
            console.log(response);

            if(!response.Passport.IssuingCountry.Iso3Digit){
                response.Passport.IssuingCountry.Iso3Digit = newVoucherValue.Tourist.Passport.IssuingCountry.Iso3Digit;
            }

            newVoucherValue.Tourist = response;

            //If we have all the details then go straight to shopperDetails screen, but if any required fields are missing then we
            //need to go through the regular new user workflow.
            utilsService.requiredFieldsArePresent(newVoucherValue).then(function (allPresent) {

                if (allPresent) {
                    translatedToastr.success('NEWCUSTOMER.TOURIST_FOUND');
                    navigationService.pageValidityChanged(true, undefined, 'navigation.shopperDetails');
                } else {
                    translatedToastr.warning('NEWCUSTOMER.TOURIST_FOUND_MISSING_FIELDS');
                    navigationService.pageValidityChanged(true, undefined, 'navigation.passportdetails');
                }

                navigationService.gotoNextState();
            });
        }

        function getTouristError(err) {

            translatedToastr.warning('NEWCUSTOMER.PASSPORT_TOURIST_NOT_FOUND', 'TOAST.WARNING', { preventDuplicates:false });

            //We expect to see  response.status === 404 if the passport number was un-recognised, but we might equally see a 401 if not logged in,
            //or some other message if we're offline. Let's always navigate to the next page on any sort of failure
            navigationService.pageValidityChanged(true, undefined, 'navigation.passportdetails');
            navigationService.gotoNextState();
        }
    }
})();
