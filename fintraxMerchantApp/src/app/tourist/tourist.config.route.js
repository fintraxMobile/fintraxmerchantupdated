﻿(function () {
    'use strict';

    angular
        .module('merchantApp.tourist')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.passportscan', {
            url: '^/passportscan',
            controller: 'passportScanController as vm',
            templateUrl: 'app/tourist/templates/passportscan.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.passportdetails', {
            url: '^/passportdetails',
            controller: 'passportDetailsController as vm',
            templateUrl: 'app/tourist/templates/passportdetails.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.touristaddress', {
            url: '^/touristaddress',
            controller: 'touristAddressController as vm',
            templateUrl: 'app/tourist/templates/touristaddress.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.shopperDetails', {
            url: '^/shopperdetails',
            controller: 'shopperDetailsExistingController as vm',
            templateUrl: 'app/tourist/templates/shopperDetailsExisting.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.premierpass', {
            url: '^/premierpass',
            controller: 'premierPassController as vm',
            templateUrl: 'app/tourist/templates/premierPass.html',
            data: {
                isVoucherCreationRoute:true
            }
        });

    }

})();
