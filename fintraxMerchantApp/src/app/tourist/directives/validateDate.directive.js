﻿(function () {
    'use strict';

    angular
        .module('merchantApp.tourist')
        .directive('muValidateDate', function (dateDropdownConstants, $parse,cacheService,$rootScope) {

            function isDateComplete(dateObjectToCheck) {
                var isValidDay = angular.isDefined(dateObjectToCheck.day) && dateObjectToCheck.day !== null &&
                    dateObjectToCheck.day.toString().length > 0;

                var isValidMonth = angular.isDefined(dateObjectToCheck.month) && dateObjectToCheck.month !== null &&
                    dateObjectToCheck.month.toString().length > 0;

                var isValidYear = angular.isDefined(dateObjectToCheck.year) && dateObjectToCheck.year !== null &&
                    dateObjectToCheck.year.toString().length > 0;

                return isValidMonth && isValidDay && isValidYear;
            }

            function isDateValid(dateObjectToCheck) {
                var date = new Date(dateObjectToCheck.year, dateObjectToCheck.month, dateObjectToCheck.day);

                return date.getDate() === dateObjectToCheck.day && date.getMonth() === dateObjectToCheck.month &&
                    date.getFullYear() === dateObjectToCheck.year;
            }

            function isFutureDate(dateObjectToCheck) {
                var date = new Date(dateObjectToCheck.year, dateObjectToCheck.month, dateObjectToCheck.day);
                return date > new Date();
            }


            function isAdult(dateObjectToCheck,scope) {
                var date = new Date(dateObjectToCheck.year, dateObjectToCheck.month, dateObjectToCheck.day);
                var age = calculateAge(date);
                if(scope.$parent.$parent.vm)
                {
               if(scope.$parent.$parent.vm.merchantConfig)
            	   {
                if(scope.$parent.$parent.vm.merchantConfig.$$state.status>0)
                {
                   var merchantObject=scope.$parent.$parent.vm.merchantConfig.$$state.value.Settings;
                   var ageDateOfBirth=searchKey("AgeDateOfBirth",scope.$parent.$parent.vm.merchantConfig.$$state.value.Settings);
                 if(ageDateOfBirth)
                   {
                   if(angular.isNumber(parseInt(ageDateOfBirth.Value)))
                   {
                      return age >=parseInt(ageDateOfBirth.Value);

                   }
                   }

                 }
                else if(localStorage.ageDateOfBirth) {

                  if(angular.isNumber(parseInt(localStorage.ageDateOfBirth)))
                  {
                    return age >=parseInt(localStorage.ageDateOfBirth);

                  }


                   }
            	   }
              }
                return age >= 10;
            }

            function calculateAge(birthday) { // birthday is a date
                var n = new Date();
                var dateNowWithoutTime = Date.now(n.getFullYear(), n.getMonth(), n.getDate());
                var ageDifMs = dateNowWithoutTime - birthday.getTime();
                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                return Math.abs(ageDate.getUTCFullYear() - 1970);
            }


            function shouldValidate(attrs, scope) {
                return $parse(attrs.muValidateDate)(scope);
            }
            function searchKey(nameKey, myArray){
             for (var i=0; i < myArray.length; i++) {
           if (myArray[i].Name === nameKey) {
            return myArray[i];
              }
             }
            }
            function setValidationAsPerModel(ctrl,validationname,attrs,condition,scope)
            {
            	//the main issue in validation for all builds
            	if(attrs.validatorName)
            		{
            		ctrl.$setValidity(attrs.validatorName+"_"+validationname,condition); 
            		if(typeof scope.$parent !=="undefined")
            			{
            			var attributes={
            					validationname:validationname,
            					condition:condition,
            					validatorName:attrs.validatorName,
            					scope:scope
            					
            			};
            			
            			 scope.$parent.$broadcast('$datevalidator',attributes);
            			}
            		}
            	
            	
            }

            return {
                restrict: 'A',
                require: 'ngModel', // element must have ng-model attribute.
                link: function (scope, ele, attrs, ctrl) {

                    //Create day month year collections on the scope for the dropdowns to bind to
                    scope.dateDropdowns = dateDropdownConstants;

                    //Could also do: scope.$watch('vm.customer.dateofbirth'..., but of course this is the string that we assigned
                    //to the ng-model attribute on the directive.
                    //NB if you have time you could refactor this into an inherited scope so that we can hide the '_' from the parent scope
                    scope.$watch(attrs.ngModel + '_', function (newDate) {

                        if (angular.isDefined(newDate)) {
                            if (shouldValidate(attrs, scope) === false) {
                                return; //don't do any validation if this field is not required.
                            }

                            var isComplete = isDateComplete(newDate);
                            window.localStorage.getItem('FlexibleOrientation')=='true'?setValidationAsPerModel(ctrl,'incompleteDate',attrs,isComplete,scope): ctrl.$setValidity('incompleteDate', isComplete);
                           // ctrl.$setValidity('incompleteDate', isComplete);

                            if (isComplete) {

                                var validDate = isDateValid(newDate);
                                window.localStorage.getItem('FlexibleOrientation')=='true'?setValidationAsPerModel(ctrl,'invalidDate',attrs,validDate,scope): ctrl.$setValidity('invalidDate', validDate);
                               // ctrl.$setValidity('invalidDate', validDate);

                                if (validDate) {

                                    var validatingTheFuture = angular.isDefined(attrs.futureDate);

                                    //only validate if date is in the past, if there's no 'future-date' attribute
                                    //we want to use this for both passport (future) and date of birth (past)

                                    if(validatingTheFuture){

                                        var newFutureDate = new Date(Date.UTC(newDate.year, newDate.month, newDate.day));

                                        $parse(attrs.ngModel).assign(scope, newFutureDate);
                                        return;

                                    } else {

                                        var futureDate = isFutureDate(newDate);
                                        window.localStorage.getItem('FlexibleOrientation')=='true'?setValidationAsPerModel(ctrl,'futureDate',attrs,!futureDate,scope): ctrl.$setValidity('futureDate', !futureDate);
                                        //ctrl.$setValidity('futureDate', !futureDate);
                                        ctrl.$setValidity('underAgeDateOfBirth', true);

                                        if (!futureDate) {

                                            //We want to allow the user to proceed if they are under 10 years old, so make this an info setting:

                                            if (angular.isDefined(attrs.underAge)) {
                                            	var condition =isAdult(newDate,scope);
                                                ctrl.$setValidity('underAgeDateOfBirth', condition);
                                                if(typeof scope.$parent !=="undefined")
                                    			{
                                    			var attributes={
                                    					validationname:'underAgeDateOfBirth',
                                    					condition:condition,
                                    					validatorName:attrs.validatorName,
                                    					scope:scope
                                    					
                                    			};
                                    			
                                    			 scope.$parent.$broadcast('$datevalidator',attributes);
                                    			}
                                                
                                            //    $parse(attrs.underAge).assign(scope, !isAdult(newDate,scope));
                                            }

                                            //Parse it into a proper Date object
                                            //Note to self, this doesn't work because of the dot notation:
                                            //scope['vm.customer.dateOfBirth'] = new Date(newDateOfBirth.year, newDateOfBirth.month, newDateOfBirth.day);
                                            //We must use $parse.assign instead
                                            var newDateObject = new Date(Date.UTC(newDate.year, newDate.month, newDate.day));

                                            $parse(attrs.ngModel).assign(scope, newDateObject);
                                            return;
                                        }
                                    }

                                }
                            }

                            if (angular.isDefined(attrs.underAge)) {
                                $parse(attrs.underAge).assign(scope, false);
                            }

                        } else {
                            if (!shouldValidate(attrs, scope)) {
                                return; //don't do any validation if this field is not required.
                            }
                            window.localStorage.getItem('FlexibleOrientation')=='true'?setValidationAsPerModel(ctrl,'incompleteDate',attrs,false): ctrl.$setValidity('incompleteDate', false);
                           // ctrl.$setValidity('incompleteDate', false);
                        }
                    }, true);

                    //This does the same thing going the other way. When we set a DateOfBirth, then assign values for the day,
                    //month and year dropdown.
                    //assignDateValues($parse(attrs.ngModel)(scope));
                    scope.$watch(attrs.ngModel, assignDateValues, true);

                    //This assigns new date to DateOfBirth_ (assuming ngModel = "Date Of Birth")
                    function assignDateValues(newDate) {
                        if (typeof newDate === 'string') {
                            newDate = new Date(newDate);
                        }
                        if (angular.isDefined(newDate) && (newDate instanceof Date)) {
                            $parse(attrs.ngModel + '_').assign(scope, { day: newDate.getUTCDate(), month: newDate.getUTCMonth(), year: newDate.getUTCFullYear() });
                        }
                    }
                }
            };
        });

})();
