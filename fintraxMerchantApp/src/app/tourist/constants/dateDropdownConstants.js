﻿(function () {
    'use strict';

    angular.module('merchantApp.tourist').constant('dateDropdownConstants', (function () {
        var days = [];
        for (var d = 1; d <= 31; d++) {
            days.push(d);
        }

        var months = [
            { translationKey: 'DROPDOWNS.MONTHS.JANUARY', id: 0 },
            { translationKey: 'DROPDOWNS.MONTHS.FEBRUARY', id: 1 },
            { translationKey: 'DROPDOWNS.MONTHS.MARCH', id: 2 },
            { translationKey: 'DROPDOWNS.MONTHS.APRIL', id: 3 },
            { translationKey: 'DROPDOWNS.MONTHS.MAY', id: 4 },
            { translationKey: 'DROPDOWNS.MONTHS.JUNE', id: 5 },
            { translationKey: 'DROPDOWNS.MONTHS.JULY', id: 6 },
            { translationKey: 'DROPDOWNS.MONTHS.AUGUST', id: 7 },
            { translationKey: 'DROPDOWNS.MONTHS.SEPTEMBER', id: 8 },
            { translationKey: 'DROPDOWNS.MONTHS.OCTOBER', id: 9 },
            { translationKey: 'DROPDOWNS.MONTHS.NOVEMBER', id: 10 },
            { translationKey: 'DROPDOWNS.MONTHS.DECEMBER', id: 11 }
        ];

        var years = [];
        var year = (new Date()).getFullYear();
        for (var y = year; y > year - 110; y--) {
            years.push(y);
        }

        var futureYears = [];
        var futureYear = (new Date()).getFullYear();

        for (var y2 = futureYear; y2 < futureYear + 31; y2++) {
            futureYears.push(y2);
        }

        return { days: days, months: months, years: years, futureYears: futureYears };
    })());


})();