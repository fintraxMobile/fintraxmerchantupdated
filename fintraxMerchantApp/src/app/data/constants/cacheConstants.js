﻿(function () {
    'use strict';

    angular.module('merchantApp.data').constant('cacheConstants', {
        MERCHANT_CONFIG: 'merchantConfig',
        VOUCHER_LIST: 'voucher',
        VOUCHER_LIST_QUEUED: 'voucher_queued',
        OFFLINE_QUEUE: 'offline_queue',
        COUNTRIES: 'countries',
        FAQS: 'faqs',
        PHRASE_BOOK: 'phrase_book'
    });

})();
