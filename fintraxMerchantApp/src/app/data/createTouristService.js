(function () {

    angular
        .module('merchantApp.data')
        .factory('createTouristService', createTouristService);


    /* @ngInject */
    function createTouristService(fintraxApi, dataService, merchantAppConfig, diacriticsService) {

        var service = {
            fullyCreateTourist:fullyCreateTourist,
            formatTourist:formatTourist
        };

        return service;

        function fullyCreateTourist(tourist) {

            return signupTouristWithAuth0(tourist)
                .then(createTouristWithFintrax);
        }

        function signupTouristWithAuth0(tourist){
            tourist.Password = createRandomPassword();

            var data = {
                email:tourist.Email,
                password: tourist.Password,
                /* jshint ignore:start */
                client_id: merchantAppConfig.auth0Tourist.clientId,
                /* jshint ignore:end */
                connection:merchantAppConfig.auth0Tourist.connection,
                scope: 'openid role'
            };

            var url = 'https://' + merchantAppConfig.auth0Tourist.domain + '/dbconnections/signup';

            return dataService.getData(url, data, 'POST').then(function(result){

                tourist.AuthenticationId = 'auth0|' + result._id;
                return tourist;

            });

        }

        function formatTourist(tourist){
            delete tourist.DateOfBirth_;
            delete tourist.underAge;
            delete tourist.createAccount;

            if(tourist.Nationality){
                delete tourist.Nationality.Name;
            }

            if(tourist.Address && tourist.Address.Country){
                delete tourist.Address.Country.Name;
            }

            if(tourist.AddressAdditional && tourist.AddressAdditional.Country){
                delete tourist.AddressAdditional.Country.Name;
            }

            if(tourist.Passport && tourist.Passport.IssuingCountry){
                delete tourist.Passport.IssuingCountry.Name;
            }

            //Removing non-ASCII characters because POST Tourist endpoint doesn't support UTF-8!
            tourist.FirstName = diacriticsService.removeDiacritics(tourist.FirstName);
            tourist.LastName = diacriticsService.removeDiacritics(tourist.LastName);

            return tourist;
        }


        function createTouristWithFintrax(touristToCreate){

            touristToCreate = formatTourist(touristToCreate);


            return fintraxApi.createTourist(touristToCreate).then(function(tourist){
                touristToCreate.Id = tourist.Id;
                return touristToCreate;

            });

        }

        //Ensure we have at least one upper, one number, one lower and then a mix of random chars and numbers.
        function createRandomPassword() {
            var length = 10;
            var upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            var lower = 'abcdefghijklmnopqrstuvwxyz';
            var digit = '0123456789';
            var all = upper + lower + digit;

            var result = [];
            result = result.concat(generate(1, upper)); // 1 upper case
            result = result.concat(generate(1, lower)); // 1 lower case
            result = result.concat(generate(1, digit)); // 1 digit
            result = result.concat(generate(length - 3, all)); // remaining random mix of all types of character
            return shuffle(result).join(''); // shuffle and make a string
        }


        function rand(max) {
            return Math.floor(Math.random() * max);
        }


        // generate an array with the given 'length' of characters of the given 'set'
        function generate(length, set) {
            var result = [];
            while (length--) {
                result.push(set[rand(set.length - 1)]);
            }
            return result;
        }


        // Give it a bit of a shuffle
        function shuffle(arr) {
            var result = [];
            while (arr.length) {
                result = result.concat(arr.splice(rand[arr.length - 1]));
            }
            return result;
        }

    }


})();
