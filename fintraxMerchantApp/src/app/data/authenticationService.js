(function() {
    'use strict';
    /*jshint camelcase: true */
    angular
        .module('merchantApp.data')
        .factory('authentication', authenticationFactory);

    /* @ngInject */
    function authenticationFactory( store, jwtHelper, merchantAppConfig, merchantConfig,utilsService,angularAuth0,translatedToastr) {

        var service = {
            login: login,
            reset:reset,
            logout:logout,
            getProfile:getProfile,
            getToken:getToken,
            isAuthenticated:isAuthenticated
        };
        
        var  isAuthenticated=false;
        //replace all with recent libraries
//        var webAuth = new auth0.WebAuth({
//    	    domain:     merchantAppConfig.auth0Merchant.domain,
//    	    clientID:   merchantAppConfig.auth0Merchant.clientId
//    	  });
        
        return service;

        function login(user, onLoginSuccess, onLoginFailed) {
        	angularAuth0.client.login({
       		  realm:   merchantAppConfig.auth0Merchant.connection,
       		  username:  user.username,
       		  password: user.password,
       		  scope:'openid profile merchantId app_metadata isoCountryCode vatNumber',       		 
       		  device: 'mobile',
              sso:false,
              audience:  merchantAppConfig.auth0Merchant.audience,
              responseType: "token"
       		}, function(err, authResult,profile) {
       		  
       		//	authResultLocal=authResult;
       			
       			if(authResult)
       				{
       				
       				angularAuth0.client.userInfo(authResult.accessToken, function(err, user) {
       				isAuthenticated=true;	
       			    successfulLogin(user, authResult.idToken, authResult.accessToken, '', authResult.refreshToken, onLoginSuccess);
       				 //  debugger;
       				 
       				  });
       			 
       				}
       			
       			else
       				{
       				
       				onLoginFailed(err);
       				
       				
       				}
       			
       			
       		});
        }
        function reset(user,callback)
        
        {
        	
        //reset password logic
        	
        	angularAuth0.changePassword({
        	      connection: merchantAppConfig.auth0Merchant.connection,
        	      email:    user.email
        	    }, function (err, resp) {
        	      if(err){
        	    	  	    	  
        	    	  errorReset(err);
        	      }else{
        	    	 
        	    	  successReset(resp,callback)
        	      }
        	    });
       	
        	
        	
        	 
        }

        function successfulLogin(profile, idToken, accessToken, state, refreshToken, callback){
            store.set('profile', profile);
            //replaced with accessToken as per september release.
            store.set('token', accessToken);
            store.set('refreshToken', refreshToken);            
            callback(profile);
        }
        
       function successReset(responseMessage,callback)
       {
    	   
    	      translatedToastr.success(responseMessage)
    	      callback();
    	   
       }
       
       
       function errorReset(Error)
       {
    	   	   
    	      translatedToastr.error('VOUCHER_ERROR_CODES.SOMETHING_WENT_WRONG');
       }
        

        function getToken(){
            var idToken = store.get('token');
            var refreshToken = store.get('refreshToken');

            if (!idToken || !refreshToken) {
                return null;
            }

            if (jwtHelper.isTokenExpired(idToken)) {

                /*
                //unable to set scope in refreshIdToken function due to
                //https://github.com/auth0/auth0-angular/issues/147
                return auth.refreshIdToken(refreshToken).then(function(resultIdToken) {
                    store.set('token', resultIdToken);
                    return resultIdToken;
                });
                */

             //Replaced all with recent angular auth0 stuffs
            	
            	
            	angularAuth0.client.oauthToken({
	        	     
					 clientID:merchantAppConfig.auth0Merchant.clientId,
	        	     grantType:'refresh_token',
	        	     refreshToken:refreshToken
	        	      
	        	      
	        	    }, function (err, resp) {
	        	    if(resp)
					{						
	        	    	 store.set('token', resp.accessToken);
	                     //also refresh the merchant config file at this point
	                     merchantConfig.refreshMerchantConfig();

	                     return resp.accessToken;
					}
	        	    	
	        	    	
	        	    });
            	
           //commented old methods means v7 	
            	
//            	 return webAuth.getToken({
//                     /*jshint camelcase: false */
//                     refresh_token: refreshToken,
//                     scope: 'openid profile offline_access role',
//                     device: 'mobile'
//                 }).then(function (result) {
//
//                     store.set('token', result.accessToken);
//
//                     //also refresh the merchant config file at this point
//                     merchantConfig.refreshMerchantConfig();
//
//                     return result.id_token;
//                 }); 	
            	
            	
            	            	
//                return auth.getToken({
//                    /*jshint camelcase: false */
//                    refresh_token: refreshToken,
//                    scope: 'openid profile offline_access role',
//                    device: 'mobile'
//                }).then(function (result) {
//
//                    store.set('token', result.id_token);
//
//                    //also refresh the merchant config file at this point
//                    merchantConfig.refreshMerchantConfig();
//
//                    return result.id_token;
//                });

                /*jshint camelcase: true */

            } else {                
                return idToken;
            }
        }

        function getProfile(){
            return store.get('profile');
        }

        function isAuthenticated(){
        	
            if (!isAuthenticated) {
                var token = getToken();
                var profile = store.get('profile');
             

                if (!token || !profile) {
                    return false;
                    
                } else {

                	return angularAuth0.client.userInfo(token, function(err, user) { return user});
                }

            } else {
                return true;
            }
        		        	
        
        }

        function logout(){
        //    auth.signout();
            
            
            store.remove('profile');
            store.remove('token');
            store.remove('refreshToken');
        }
        
       

    }

})();