(function () {
    'use strict';

    angular
        .module('merchantApp.data')
        .factory('createVoucherService', createVoucherService);

    /* @ngInject */
    function createVoucherService(fintraxApi, merchantConfig, $q, createTouristService, merchantAppConfig) {

        var voucherCreationResult = {};

        return {
            createVoucherOnline:createVoucherOnline,
            //used by the printVoucher screen
            voucherCreationResult:getVoucherCreationResult,
            clearVoucherCreationResult: clearVoucherCreationResult
        };

        function getVoucherCreationResult(){
            return voucherCreationResult;
        }

        function clearVoucherCreationResult(){
            voucherCreationResult = {};
        }

        function createVoucherOnline(voucher){

            return createOrUpdateTourist()
                .then(formatVoucher)
                .then(sendVoucherToFintrax)
                .then(appendSignatureToVoucher);

            function createOrUpdateTourist(){

                voucher.Tourist = createTouristService.formatTourist(voucher.Tourist);

                if (voucher.Tourist.createAccount && typeof voucher.Tourist.Id === 'undefined') {

                    return attemptTouristCreation(voucher.Tourist);

                } else if(merchantAppConfig.enableTouristUpdate && typeof voucher.Tourist.Id !== 'undefined') {

                    var touristToUpdate = angular.copy(voucher.Tourist);
                    return fintraxApi.updateTourist(touristToUpdate);
                } else {

                    //don't need to do anything
                    return $q.resolve();
                }
            }

            function attemptTouristCreation(tourist){

                return createTouristService.fullyCreateTourist(tourist).then(function(tourist){
                    return tourist;
                }, function(response){


                    //auth0 error, user already exists
                    if(response.data && response.data.code === 'user_exists'){

                        //try to find a match on the passport number
                        return fintraxApi.getTourist(tourist.Passport.Number).then(function(existingTourist){

                            if(merchantAppConfig.enableTouristUpdate && existingTourist.Email === tourist.Email) {

                                //assume the information provided to the merchant is the most up-to-date
                                return updateExistingTourist(existingTourist, tourist);
                            } else {

                                //can't safely assume the tourist is a match
                                return null;
                            }
                        });

                    } else {

                        //some other API error, can't do anything about this
                        return null;
                    }

                });
            }

            function updateExistingTourist(tourist, newTouristInfo){

                tourist.Address = newTouristInfo.Address;
                tourist.AddressAdditional = newTouristInfo.AddressAdditional;

                return fintraxApi.updateTourist(tourist);

            }

            function generateInvoiceNumber(){

            
            	
                return 'MERCHAPPxxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
                   return v.toString(16);
                    
                  
                });
            }

            function formatVoucher() {

                return merchantConfig.getMerchantConfig().then(function (mc) {

                    voucher.MerchantId = mc.Store.PtfShopNumber;

                    //Remove any left over rubbish from UI.
                    delete voucher._originalMerchantSignature;
                    delete voucher._originalCustomerSignature;

                    if(!voucher._isOffline){
                        delete voucher.RefundAmount;
                    }

                    delete voucher._isOffline;

                    voucher.Invoices.forEach(function (invoice) {
                        delete invoice.InvoiceDateTime_;
                        delete invoice['$$hashKey'];
                        delete invoice.pricing;
                        delete invoice._wasFetchedFromAPI;
                        delete invoice._isExpanded;

                        //must be simple2 workflow, need to autogenerate barcode & number
                        if(!invoice.Barcode && !invoice.Number){
                            invoice.Barcode = generateInvoiceNumber();
                            invoice.Number = invoice.Barcode;
                        }

                        invoice.LineItems.forEach(function (li) {

                            //description is always required by the API, even if it's not in the app's UI
                            if(!li.Description) {

                                if(li._itemtype){
                                    li.Description = li._itemtype.ProductDescription;
                                } else {
                                    li.Description = 'See attached sales receipt';
                                }
                            }

                            delete li._itemtype;
                            delete li['$$hashKey'];

                        });

                        if(invoice.PaymentMethodDetails[0] && invoice.PaymentMethodDetails[0].PaymentMethodOtherName === ''){
                            delete invoice.PaymentMethodDetails[0].PaymentMethodOtherName;
                        }
                    });

                    return mc;

                });
            }

            function sendVoucherToFintrax(merchantConfig) {

                voucherCreationResult = {};

                //Post the voucher to API.
                return fintraxApi.createVoucher(voucher).then(function (res) {

                    return {
                        merchantConfig: merchantConfig,
                        response:res
                    };
                });

            }

            function appendSignatureToVoucher(createResult) {

                //we want to use the resultant voucher from the API on the print voucher screen
                voucherCreationResult = createResult.response;

                //POST vouchers response for offline vouchers is null with a 201 response
                if(voucherCreationResult){
                    //however, we want to keep the customer & merchant signature images
                    voucherCreationResult.CustomerSignature = voucher.CustomerSignature;
                    voucherCreationResult.MerchantSignature = voucher.MerchantSignature;
                }

                return voucherCreationResult;
            }
        }

    }
})();
