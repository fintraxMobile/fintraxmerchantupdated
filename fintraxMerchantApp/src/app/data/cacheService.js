(function(){
    angular
        .module('merchantApp.data')
        .factory('cacheService', cacheService);

    /* @ngInject */
    function cacheService(pouchDB) {

        var db = pouchDB('fintraxMerchantAPICache'); //keep a separate database for this simple API caching

        var service = {
            get:get,
            set: set,
            remove: remove,
            bulkRemove:bulkRemove,
            bulkGet:bulkGet,
            bulkSet: bulkSet,
            destroyDatabase: destroyDatabase
        };

        return service;

        //get all items whose _id starts equals the cacheKey
        //Note: Set method wraps the data up in its own object so that we won't have problems storing arrays. 
        //Get method unwraps it so users of this service are unaware of this detail.
        function get(cacheKey) {
            return db.get(cacheKey).then(function(doc){ return doc.data; }); //returns a promise
        }

        //add stuff to database, _id prepended with cacheKey
        //Note: We wrap the data up in its own object so that we won't have problems with arrays.
        function set(cacheKey, data) {
            //We are not interested in maintaining a revision history of the document (thus using ever more storage space),
            //so we can't simply 'put' it. Instead we remove it and re-add it.
            return remove(cacheKey).then(function () {
                return db.put({ _id: cacheKey, data: data }).then(function () { return data; });
            });
        }

        function remove(cacheKey) {
            return db.get(cacheKey).then(function (doc) {                
                return db.remove(doc);
            }, angular.noop); //If the data wasn't in the DB then no operation. (This ensures the promise resolves OK).
        }
        
        //find all documents whose _Id starts with cachekey
        function bulkGet(cacheKey) {
            return db.allDocs({
                startkey:cacheKey,
                endkey:cacheKey + '\uffff',
                /* jshint ignore:start */
                include_docs:true
                /* jshint ignore:end */
            }).then(function (docs) {
                return docs.rows.map(function (item) { return item.doc.data; });
            });
        }

        //bulk set a bunch of records, give them an id starting with cacheKey
        function bulkSet(cacheKey, data){            
            var wrappedData = data.map(function (item) {
                return {
                    //add a guid to the end of the key to ensure it's unique
                    _id: cacheKey + '_' + gooey(),
                    data: item
                };
            });

            return db.bulkDocs(wrappedData);
        }


        //bulk remove all documents whose _id starts with cacheKey
        function bulkRemove(cacheKey) {
            return db.allDocs({
                startkey: cacheKey, endkey: cacheKey + '\uffff',
                /* jshint ignore:start */
                include_docs: true
                /* jshint ignore:end */
            }).then(function (docs) {
                var docsToDelete = docs.rows.map(function (item) {
                    item.doc._deleted = true;
                    return item.doc;
                });
                return db.bulkDocs(docsToDelete);
            });            
        }

        function gooey(){
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        }

        function destroyDatabase() {
            return db.destroy().then(function () {
                db = pouchDB('fintraxMerchantAPICache');
            });
        }

    }

})();