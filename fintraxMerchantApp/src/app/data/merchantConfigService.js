(function(){
    'use strict';

    angular
        .module('merchantApp.data')
        .factory('merchantConfig', merchantConfig);


    function merchantConfig(fintraxApi, cacheService, cacheConstants) {

        var service = {
            getMerchantConfig: getMerchantConfig,
            removeVoucherRange: removeVoucherRange,
            replaceVoucherRanges: replaceVoucherRanges,
            refreshMerchantConfig: refreshMerchantConfig,
            removeMerchantConfig: removeMerchantConfig,
            getMerchantConfigFromCache:getMerchantConfigFromCache,
            getSetting: getSetting
        };

        return service;

        function getMerchantConfig() {

            //getMerchantConfig will automatically cache the result in pouch
            return fintraxApi.getMerchantConfig();
        }

        //whenever a voucher is created, a voucher number should be removed
        function removeVoucherRange(voucherRange){

            return getMerchantConfig().then(function (merchantConfig) {

                removeItemFromVoucherRangesArray(merchantConfig, voucherRange);

                return cacheService.set(cacheConstants.MERCHANT_CONFIG, merchantConfig);
            });
        }

        function removeItemFromVoucherRangesArray(merchantConfig, voucherRange) {

            var index = merchantConfig.VoucherRanges.indexOf(voucherRange);
            if (index > -1) {
                merchantConfig.VoucherRanges.splice(index, 1); //remove the required range
            }
        }

        //refresh just the voucher ranges
        function replaceVoucherRanges(voucherRange){
            return getMerchantConfig().then(function (merchantConfig) {

                //replace the existing voucher ranges
                merchantConfig.VoucherRanges = voucherRange;
                return cacheService.set(cacheConstants.MERCHANT_CONFIG, merchantConfig);
            });
        }


        function refreshMerchantConfig(wipeEverything) {

            //If we have it in the cache then remove it, otherwise attempt to re-load it from the web
            return cacheService.get(cacheConstants.MERCHANT_CONFIG)
                .then(function(config){
                    safelyRefreshMerchantConfig(config, wipeEverything);
                }, getMerchantConfig);
        }
        function getMerchantConfigFromCache(wipeEverything) {

            //If we have it in the cache then return it.
            return cacheService.get(cacheConstants.MERCHANT_CONFIG);

        }

        function safelyRefreshMerchantConfig(merchantConfig, wipeEverything) {

            var currentVoucherRanges = merchantConfig.VoucherRanges;

            return removeMerchantConfig().then(function () {
                return getMerchantConfig();

            }).then(function(config){
            	//ideally implement the flow for every config
            	
            	
            	activateFlow(config);
            	 
            	
                //need to keep a hold of the voucher ranges stored in the config
                if(currentVoucherRanges && currentVoucherRanges.length > 0 && !wipeEverything){
                    return replaceVoucherRanges(currentVoucherRanges);
                } else {
                    return config;
                }

            });
        }

        function removeMerchantConfig(){
            return cacheService.remove(cacheConstants.MERCHANT_CONFIG);
        }

        function getSetting(settingName) {

            return getMerchantConfig().then(function (mc) {

                var matching = mc.Settings.filter(function (s) {
                    return s.Name === settingName;
                });

                var setting = null;

                if(matching.length === 0){
                    return false;
                } else {
                    setting = matching[0];
                }

                //booleanify the value
                if(setting.Value.toLowerCase() === 'true'){

                    return true;
                } else if(setting.Value.toLowerCase() === 'false'){

                    return false;
                } else {

                    return setting.Value;
                }

            });
        }
        
     function activateFlow(config)
   	 {    
    	
 		var	$window=angular.isUndefined($window)?window:$window; 
   		 //will implement on next release on all empty places
   		  config.Settings.forEach(function(setting){

                 switch(setting.Name){

                     case 'VipRefundAllowed':
                      //replace with local storage
                         break;

                     case 'AddReceiptScenario':
                   	//replace with local storage 
                         break;

                     case 'EnableTouristSignature':
                   	//replace with local storage
                         break;

                     case 'EnableMerchantSignature':
                   	//replace with local storage 
                         break;
                     case 'EnableOrientation':
                   	 if($window.screen)
                   		 {
                   		 setting.Value=='True'?$window.screen.orientation.unlock() : $window.screen.orientation.lock('landscape');
                   		 setting.Value=='True'?$window.localStorage.setItem('FlexibleOrientation', true)  :$window.localStorage.setItem('FlexibleOrientation', false); 
                   		 }
                         break;
                     case 'EnableQuickPrint':
                     	  $window.localStorage.setItem('EnableQuickPrint',  setting.Value=='True'?true:false);
                         break;
                         
                     case 'EnableCtrip':
                     	  $window.localStorage.setItem('EnableCtrip',setting.Value=='True'?true:false);
                         break;      
                         
                         
                         
                         
                 }
             });
   		 
   		 
   		 
   	 }
        

    }


})();
