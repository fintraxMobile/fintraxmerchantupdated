(function () {
    'use strict';

    angular
        .module('merchantApp.data')
        .factory('createVoucherOfflineService', createVoucherOfflineService);

    /* @ngInject */
    function createVoucherOfflineService(merchantConfig, $q, $log, offlineQueue) {

        //this service exists to avoid a circular dependency between offlineQueue and createVoucherService

        return {
            createVoucherOffline: createVoucherOffline
        };

        function createVoucherOffline(voucher) {

            return allocateVoucher()
                .then(queueVoucher)
                .then(removeVoucherRangeFromMerchantConfig);

            function queueVoucher(config) {

                $log.debug('Enqueue create voucher');

                return offlineQueue.enqueueCreateVoucher(angular.copy(voucher)).then(function (res) {

                    return config;
                });

            }

            function removeVoucherRangeFromMerchantConfig(config) {
                return merchantConfig.removeVoucherRange(config.VoucherRanges[0]).then(function () {

                    return voucher;
                });
            }

            function allocateVoucher() {
                return merchantConfig.getMerchantConfig().then(function (config) {

                    if (config.VoucherRanges.length > 0) {

                        $log.debug('Using offline voucher range: ' + config.VoucherRanges[0]);

                        //all other voucher formatting happens in the createVoucherService createVoucherOnline function
                        formatOfflineVoucher(config, voucher, config.VoucherRanges[0]);

                        return config;

                    } else {

                        return $q.reject('No vouchers available!');
                    }
                });
            }

            function roundToTwoDecimalPoints(num){

                return +(Math.round(num + 'e+2')  + 'e-2');
            }

            function formatOfflineVoucher(config, voucher, voucherRangeNumber) {

                voucher._isOffline = true;
                voucher._voucherRange = voucherRangeNumber;
                voucher.VoucherSaleType = config.Store.VoucherSaleType;
                voucher.AdminFee = voucher.VatAmount - voucher.RefundAmount;

                //SalesDate should be the date of the most recent invoice
                voucher.Invoices.forEach(function (invoice) {

                    if (!voucher.SalesDate || invoice.InvoiceDateTime > voucher.SalesDate) {
                        voucher.SalesDate = invoice.InvoiceDateTime;
                    }

                });

                voucher.VoucherNumber = calculateVoucherBarcode(config, voucherRangeNumber, voucher.RefundOption.RefundOptionCode);
                voucher.VoucherBarcode = voucher.VoucherNumber;
                voucher.SecurityNumber = calculateVoucherSecurityNumber(voucherRangeNumber, roundToTwoDecimalPoints(voucher.RefundAmount), config.Store.Country.Iso);
            }

            //functions below are all provided by fintrax
            function calculateVoucherBarcode(mcfg, nextVoucherRangeNumber, refundOptionCode) {
                var res = '';

                //Add the Barcode format (Will always be 2 for the merchant App)
                res += '2';

                //Add the iso 3 digit value for the merchant country
                //Country code should be padded with 0 is length is < 3
                //Using lodash's padRight function for simplicity see definition here https://lodash.com/docs#padRight
                res += _.padRight(mcfg.Store.Country.Iso, 3, '0');

                //Add the Sale Type
                res += getSaleType(mcfg.Store.VoucherSaleType, refundOptionCode);

                //Add the Issuing system (Merchant app Issuing system id is 25)
                res += '25';

                //Add the backOffice id - take the last char of Store.BackOfficeID
                var boId = mcfg.Store.BackOfficeID.substring(1);
                res += boId;

                //Add the Merchant Back Office Store Number padded to 6 with leading 0
                res += _.padRight(mcfg.Store.PtfShopNumber, 6, '0');

                //Add The Till id (00)
                //(1) if backOfficeId is '1' Do not set the terminal id otherwise set it to '00'
                res += boId === '1' ? '' : '00';

                //Add the voucher range number
                //(2) if backOfficeId is '1' add a checkDigit to the voucherNumber
                res += boId === '1' ?
                _.padLeft(nextVoucherRangeNumber, 9, '0') + calculateCheckDigit(nextVoucherRangeNumber) :
                    _.padLeft(nextVoucherRangeNumber, 8, '0');

                return res;
            }

            function getSaleType(salesType, refundOptionCode) {

                var isCashRefund = refundOptionCode.indexOf('08') === 0;

                switch (salesType) {
                    case 'S':
                        return isCashRefund ? '1' : '2';
                    case 'D':
                        return isCashRefund ? '3' : '4';
                    default:
                        return '0';
                }
            }

            function calculateCheckDigit(voucherRangeNumber) {
                var ckArr = [2, 3, 4, 5, 6, 7, 8, 9];
                var vNumberSplitted = voucherRangeNumber.toString().split('').map(function (x) {
                    return parseInt(x);
                });

                var comp = vNumberSplitted
                    .reverse()
                    .map(function (x, idx) {
                        return x * ckArr[idx];
                    })
                    .reduce(function (x, y) {
                        return x + y;
                    });

                var ck = comp % 11;

                return ck === 10 ? 0 : ck;
            }


            function calculateVoucherSecurityNumber(voucherRangeNumber, voucherRefundAmount, iso3CountryCode) {
                var date = new Date();
                var dateNum = parseInt(date.getUTCDate().toString() + _.padRight((date.getUTCMonth() + 1).toString(), 2, '0') +
                    date.getUTCFullYear().toString().substring(2));

                var pRefundAmount = parseInt(_.padLeft(voucherRefundAmount.toString().replace('.', ''), 8, '0'));
                var pIso = _.padRight(iso3CountryCode, 3, '0');

                var comp = _.padRight(dateNum + pRefundAmount + voucherRangeNumber, 8, '0').substring(1);

                var isOdd = function (idx) {
                    return idx % 2 !== 0;
                };

                var secNum = comp.split('')
                    .map(function (x, idx) {
                        return isOdd(idx) ? pIso[parseInt(idx / 2)] : x;
                    })
                    .reduce(function (x, y) {
                        return x + y;
                    });

                return _.padRight(secNum, 9, '0');
            }

        }
    }


})();
