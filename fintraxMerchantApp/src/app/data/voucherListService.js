(function(){
    angular
        .module('merchantApp.data')
        .factory('voucherListService', voucherListService);

    /* @ngInject */
    function voucherListService(cacheService, fintraxApi, cacheConstants) {
        var service = {
            getVoucherList: getVoucherList,
            refreshVoucherList: refreshVoucherList
        };

        return service;

        function getVoucherList(merchantConfig) {

            return cacheService.bulkGet(cacheConstants.VOUCHER_LIST).then(afterCacheSearch);

            function afterCacheSearch(results){
                return results.length > 0 ? results : refreshVoucherList(merchantConfig);
            }
        }

        function refreshVoucherList(merchantConfig) {

            //call API, replace current content of cache with new result
            return fintraxApi.getVoucherList(merchantConfig).then(function(apiResult){

                //only refresh if api call was successful
                return removeVoucherList().then(function (resultOfDelete){
                    return cacheService.bulkSet(cacheConstants.VOUCHER_LIST, apiResult).then(function(resultOfSet){

                        //return data as from the API service
                        return apiResult;
                    });
                });
            });

        }

        function removeVoucherList() {
            return cacheService.bulkRemove(cacheConstants.VOUCHER_LIST);
        }

    }

})();