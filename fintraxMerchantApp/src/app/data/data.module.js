(function() {
    'use strict';

    angular.module('merchantApp.data',
        [
            'angular-jwt',
            'auth0.auth0',
            'angular-storage',
            'pouchdb'
        ])
        .config(dataConfig)
       .run(run);

    /* @ngInject */
    function dataConfig($httpProvider, jwtInterceptorProvider,angularAuth0Provider, merchantAppConfig){

        //initialise auth0
    	//No angular SDK's were available for recent CDN auth0 8 so we creted our own factory service with Web cdn
    	
      //  authProvider.init({
      //      domain: merchantAppConfig.auth0Merchant.domain,
       //     clientID: merchantAppConfig.auth0Merchant.clientId
       // });

    	angularAuth0Provider.init({
  		  domain:       merchantAppConfig.auth0Merchant.domain,
  		  audience:      merchantAppConfig.auth0Merchant.audience,
  	      clientID:      merchantAppConfig.auth0Merchant.clientId
      });
    	
        //for every http request, add the jwt auth token to the header
        jwtInterceptorProvider.tokenGetter = ['authentication','config', function(authentication, config){
            //only send it to fintrax API, not for other requests
            if (config.url.indexOf && config.url.indexOf(merchantAppConfig.fintraxApi.url) === -1) {
                return null;
            }

            return authentication.getToken();
        }];

        $httpProvider.interceptors.push('jwtInterceptor');
    }

    /* @ngInject */
    function run(angularAuth0){
        //required auth0 setup
      //  auth.hookEvents();
    }

})();