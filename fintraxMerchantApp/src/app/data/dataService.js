﻿(function () {
    'use strict';

    angular
        .module('merchantApp.data')
        .factory('dataService', dataService);

    //This is a simple wrapper around $http to allow us to implement data-related behaviour such as back-off an retry,
    //preventing multiple copies of the same request (eg, pressing the button lots of times) and handling session
    //expiry, etc. This is also a good place to put caching, returning data from pocketDB if a GET request fails .
    function dataService($http, $log, $q, cacheService, $rootScope, cacheConstants) {
        var requestList = {};

        return { getData: getData };

        function getData(url, params, method, cacheKey, timeout) {
            var requestKey = url + JSON.stringify(params); //Just a unique identifier for this request

            if (requestList[requestKey] === undefined) {
                if (cacheKey) {

                    requestList[requestKey] = cacheService.get(cacheKey).then(function (data) {

                        if(Array.isArray(data) && data.length === 0){

                            $log.debug('cache hit for ' + requestKey + ' but array is empty! try again.');
                            return callApi(url, params, method, requestKey, cacheKey, timeout);

                        } else {

                            $log.debug('cache hit for ' + requestKey);
                            //We don't pass cache key to success as it's already in the cache.
                            return successHandler(data, requestKey);
                        }


                    }, function () {

                        $log.debug('cache miss for ' + requestKey);
                        return callApi(url, params, method, requestKey, cacheKey, timeout);
                    });

                } else {
                    requestList[requestKey] = callApi(url, params, method, requestKey, cacheKey, timeout);
                }

            } else {
                $log.debug('Request is already in flight for ' + requestKey);
            }

            return requestList[requestKey];
        }

        function callApi(url, params, method, requestKey, cacheKey, timeout) {
            $rootScope.loading = true;

            timeout = timeout ? timeout : 5000;

            var config = method === 'POST' || method === 'PUT' ?
                    { url: url, method: method, data: params, timeout: timeout }
                : { url: url, method: method, params: params, timeout: timeout };
                return $http(config)
                .then(function (response) {

                    return successHandler(response.data, requestKey, cacheKey);

                }, function (response) {

                    return failureHandler(response, requestKey);
                });
        }


        //on success mark the request as no longer in flight
        function successHandler(data, requestKey, cacheKey) {

            removeFromRequestList(requestKey);

            if (cacheKey) {

                //Historically voucher ranges were included in the merchant config
                //they're not any more, but we're still caching them there for the moment.
                if(cacheKey === cacheConstants.MERCHANT_CONFIG){
                    data.VoucherRanges = [];
                }

                return cacheService.set(cacheKey, data);
            }

            return data;
        }


        function failureHandler(response, requestKey) {
            //mark the request as no longer in flight
            removeFromRequestList(requestKey);

            //TODO - we need some proper error handling
            $log.error('Failed to load data. The server responded with:');
            $log.error(response);
            return $q.reject(response);
        }


        function removeFromRequestList(requestKey) {
            delete requestList[requestKey];
            if (Object.keys(requestList).length === 0) {
                $rootScope.loading = false;
            }
        }
    }
})();
