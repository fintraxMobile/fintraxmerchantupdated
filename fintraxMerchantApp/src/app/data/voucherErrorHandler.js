(function () {
    'use strict';

    angular
        .module('merchantApp.data')
        .factory('voucherErrorHandler', voucherErrorHandler);

    /* @ngInject */
    function voucherErrorHandler(translatedToastr, $http) {

        var possibleErrors = {
            '1': 'VOUCHER_ERROR_CODES.ERROR_1_INVOICE_ALREADY_EXISTS',
            '3': 'VOUCHER_ERROR_CODES.ERROR_3_NO_XML',
            '4': 'VOUCHER_ERROR_CODES.ERROR_4_BARCODE_NULL',
            '5': 'VOUCHER_ERROR_CODES.ERROR_5_MULTIPLE_INVOICES',
            '6': 'VOUCHER_ERROR_CODES.ERROR_6_NO_INVOICE',
            '8': 'VOUCHER_ERROR_CODES.ERROR_8_INVOICE_IN_DIFFERENT_STORE',
            '9': 'VOUCHER_ERROR_CODES.ERROR_9_INVOICE_IN_MULTIPLE_VOUCHERS',
            '10': 'VOUCHER_ERROR_CODES.ERROR_10_INVOICE_NOT_VALID',
            '20': 'VOUCHER_ERROR_CODES.ERROR_20_NO_VOUCHER_FOUND',
            '21': 'VOUCHER_ERROR_CODES.ERROR_23_INVOICE_ALREADY_ADDED',
            '24': 'VOUCHER_ERROR_CODES.ERROR_24_INVOICE_ALREADY_ADDED_DIFFERENT',
            '27': 'VOUCHER_ERROR_CODES.ERROR_27_ALREADY_VOIDED',
            '29': 'VOUCHER_ERROR_CODES.ERROR_29_ALREADY_REFUNDED_CANT_VOID',
            '30': 'VOUCHER_ERROR_CODES.ERROR_30_INVOICE_EXPIRED',
            '32': 'VOUCHER_ERROR_CODES.ERROR_32_INVALID_MERCHANT',
            '33': 'VOUCHER_ERROR_CODES.ERROR_33_VOUCHER_RANGE_NOT_FOUND',
            '36': 'VOUCHER_ERROR_CODES.ERROR_36_INVOICE_NOT_FOUND',
            '37': 'VOUCHER_ERROR_CODES.ERROR_37_NO_ELIGIBLE_ITEMS',
            '38': 'VOUCHER_ERROR_CODES.ERROR_38_REFERENCE_TO_INVOICE_MISSING',
            '42': 'VOUCHER_ERROR_CODES.ERROR_42_TRAINING_MODE',
            '43': 'VOUCHER_ERROR_CODES.ERROR_43_XML_INVALID',
            '44': 'VOUCHER_ERROR_CODES.ERROR_44_VOUCHER_DESIGN_NOT_SET',
            '45': 'VOUCHER_ERROR_CODES.ERROR_45_VOUCHER_RANGE_UNALLOCATED',
            '46': 'VOUCHER_ERROR_CODES.ERROR_46_REFUND_METHOD_NOT_ALLOWED',
            '47': 'VOUCHER_ERROR_CODES.ERROR_47_CUSTOMER_NOT_FOUND',
            '51': 'VOUCHER_ERROR_CODES.ERROR_51_INVALID_VOUCHER_FORMULA',
            '52': 'VOUCHER_ERROR_CODES.ERROR_52_INVOICE_ALREADY_EXISTS',
            '55': 'VOUCHER_ERROR_CODES.ERROR_55_CANNOT_VOID_REFUNDED',
            '56': 'VOUCHER_ERROR_CODES.ERROR_56_CONSOLIDATION_REQUIRED',
            '57': 'VOUCHER_ERROR_CODES.ERROR_57_WEBSERVICE',
            '58': 'VOUCHER_ERROR_CODES.ERROR_58_NO_VALID_INVOICES',
            '60': 'VOUCHER_ERROR_CODES.ERROR_60_MAXIMUM_QUANTITY',
            '100': 'VOUCHER_ERROR_CODES.ERROR_100_CARD_NUMBER',
            '101': 'VOUCHER_ERROR_CODES.ERROR_101_CARD_EXPIRY',
            '102': 'VOUCHER_ERROR_CODES.ERROR_102_INVALID_ITEM_DESCRIPTION',
            '103': 'VOUCHER_ERROR_CODES.ERROR_103_INVALID_COUNTRY',
            '104': 'VOUCHER_ERROR_CODES.ERROR_104_PASSPORT_NUMBER',
            '105': 'VOUCHER_ERROR_CODES.ERROR_105_NO_VALID_DOB',
            '106': 'VOUCHER_ERROR_CODES.ERROR_106_INVALID_ALIPAY_PHONENUMBER',
            '107': 'VOUCHER_ERROR_CODES.ERROR_107_INVALID_PASSPORT_NUMBER',
            '112': 'VOUCHER_ERROR_CODES.ERROR_112_MINIMUM_AGE_INCORRECT',
            '120': 'VOUCHER_ERROR_CODES.ERROR_120_INVALID_IBAN'
        };

        return {
            attemptToParseError: attemptToParseError
        };

        function attemptToParseError(response){

            //these two come from connectivityStatusService.js, which makes it's own toast
            if(response === 'merchant not allowed to create vouchers offline' ||
                response === 'offline eligible, but no voucher ranges left!'){
                return;
            }

            if(response.data && response.data.code === 'user_exists') {

                translatedToastr.error('VOUCHER_ERROR_CODES.ALREADY_HAS_AUTH0');

            } else if (response.data && response.data.Code) {

                findCorrectMessageFromErrorCode(response.data.Code);

            } else {
                translatedToastr.error('VOUCHER_ERROR_CODES.SOMETHING_WENT_WRONG');
            }
        }

        function findCorrectMessageFromErrorCode(errorCode){

            var error = possibleErrors[errorCode];

            if(typeof error === 'undefined'){
                error = 'VOUCHER_ERROR_CODES.SOMETHING_WENT_WRONG';
            }

            translatedToastr.error(error);

        }

    }
})();
