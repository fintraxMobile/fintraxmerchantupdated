(function () {
    angular
        .module('merchantApp.data')
        .factory('fintraxApi', fintraxApi);

    function fintraxApi(merchantAppConfig, dataService, cacheConstants, localeService, contentParserService) {

        var apiLocation = merchantAppConfig.fintraxApi.url;
        var cmsApiLocation = merchantAppConfig.cmsApi.url;

        var service = {
            getMerchantConfig: getMerchantConfig,
            getTouristFromPassport: getTouristFromPassport,
            getTourist: getTourist,
            createTourist: createTourist,
            updateTourist: updateTourist,
            getInvoice: getInvoice,
            getVoucherList: getVoucherList,
            getVoucherByVoucherNumber:getVoucherByVoucherNumber,
            getVoucherRanges: getVoucherRanges,
            createVoucher: createVoucher,
            voidVoucher: voidVoucher,
            getPhrasebook:getPhrasebook,
            getFaqs:getFaqs,
            getCountries:getCountriesForCurrentLocale
        };

        return service;

        function getMerchantConfig() {
            return dataService.getData(apiLocation + 'merchants/current', {}, 'GET',
                cacheConstants.MERCHANT_CONFIG, 35000).then(function(config){

                    config.Store.AddressLineOne = config.Store.AddressLineOne || '';
                    config.Store.AddressLineTwo = config.Store.AddressLineTwo || '';
                    config.Store.AddressLineThree = config.Store.AddressLineThree || '';
                    config.Store.AddressLineFour = config.Store.AddressLineFour || '';
                    config.Store.AddressLineFive = config.Store.AddressLineFive || '';
                    config.Store.AddressLineSix = config.Store.AddressLineSix || '';
                    config.Store.AddressLineSeven = config.Store.AddressLineSeven || '';

                    config.MerchantHeadOfficeDetails.AddressLineOne = config.MerchantHeadOfficeDetails.AddressLineOne || '';
                    config.MerchantHeadOfficeDetails.AddressLineTwo = config.MerchantHeadOfficeDetails.AddressLineTwo || '';
                    config.MerchantHeadOfficeDetails.AddressLineThree = config.MerchantHeadOfficeDetails.AddressLineThree || '';
                    config.MerchantHeadOfficeDetails.AddressLineFour = config.MerchantHeadOfficeDetails.AddressLineFour || '';
                    config.MerchantHeadOfficeDetails.AddressLineFive = config.MerchantHeadOfficeDetails.AddressLineFive || '';
                    config.MerchantHeadOfficeDetails.AddressLineSix = config.MerchantHeadOfficeDetails.AddressLineSix || '';
                    config.MerchantHeadOfficeDetails.AddressLineSeven = config.MerchantHeadOfficeDetails.AddressLineSeven || '';

                   
                   //We need to change this code in standard place 
                    var ageDateOfBirth=searchKey("AgeDateOfBirth",  config.Settings);
                    if(ageDateOfBirth)
                    {
                    	if(angular.isNumber(parseInt(ageDateOfBirth)))
                    	{
                      //check for alternative
                    		localStorage.ageDateOfBirth=ageDateOfBirth.Value;

                    	}

                    }
                    //Fix to get the profile for EAPI v2 to append country code 
                   var fromJsonObjectToAddCountryCode=angular.fromJson(localStorage.profile);
                   
                   fromJsonObjectToAddCountryCode.isoCountryCode=config.Store.IsoCurrencyCode;
                   
                   localStorage.profile=angular.toJson(fromJsonObjectToAddCountryCode);
                   
                    
                    
                    

                    	return config;
                });
        }


        function searchKey(nameKey, myArray){
         for (var i=0; i < myArray.length; i++) {
       if (myArray[i].Name === nameKey) {
        return myArray[i];
          }
         }
       }

        function getTourist(identifier){
            return dataService.getData(apiLocation + 'tourists/' + identifier , {}, 'GET');
        }

        function getTouristFromPassport(passportNumber,countryCode){
            return dataService.getData(apiLocation + 'tourists/' + passportNumber + '/' + countryCode, {}, 'GET');
        }

        function createTourist(tourist) {
            return dataService.getData(apiLocation + 'tourists', tourist, 'POST');
        }

        function updateTourist(tourist) {
            return dataService.getData(apiLocation + 'tourists', tourist, 'PUT');
        }

        function getInvoice(barcodeNumber) {
            return dataService.getData(apiLocation + 'invoices/' + barcodeNumber, {}, 'GET');
        }

        function getVoucherList(){

            //report should currently just have the last 7 days receipts.

            var today = new Date();
            var tomorrow = new Date(today.setDate(today.getDate() + 1));
            var startDate = new Date(new Date().setDate(tomorrow.getDate() - 7));

            function dateAsIsoDateString(date){
                //don't forget, getMonth is zero indexed!
                return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
            }

            var url = apiLocation + 'vouchers/merchant/' +
                dateAsIsoDateString(startDate) + '/' + dateAsIsoDateString(tomorrow);
            //increased the time out for good latency raised by UK team
            return dataService.getData(url,{}, 'GET', false, 35000);

        }

        function getVoucherByVoucherNumber(voucherBarcode){

            return dataService.getData(apiLocation + 'vouchers/merchant/' + voucherBarcode, {}, 'GET').then(function(voucher){



                voucher.Tourist.Address.AddressLine1 = voucher.Tourist.Address.AddressLine1 || '';
                voucher.Tourist.Address.AddressLine2 = voucher.Tourist.Address.AddressLine2 || '';
                voucher.Tourist.Address.PostalCode = voucher.Tourist.Address.PostalCode || '';
                voucher.Tourist.Address.City = voucher.Tourist.Address.City || '';

                voucher.Tourist.AddressAdditional = voucher.Tourist.AddressAdditional || {};
                voucher.Tourist.AddressAdditional.AddressLine1 = voucher.Tourist.AddressAdditional.AddressLine1 || '';
                voucher.Tourist.AddressAdditional.AddressLine2 = voucher.Tourist.AddressAdditional.AddressLine2 || '';
                voucher.Tourist.AddressAdditional.PostalCode = voucher.Tourist.AddressAdditional.PostalCode || '';
                voucher.Tourist.AddressAdditional.City = voucher.Tourist.AddressAdditional.City || '';


                return voucher;
            });
        }

        function getVoucherRanges(){

            return dataService.getData(apiLocation + 'merchants/voucherranges/new', {}, 'GET');
        }

        function createVoucher(voucher) {
        	
        	
        	
            return dataService.getData(apiLocation + 'vouchers', voucher, 'POST', false, 15000);
        }

        function voidVoucher(voucherBarcode) {
            return dataService.getData(apiLocation + 'vouchers/' + voucherBarcode, {}, 'DELETE');
        }

        //getPhrasebook & getFaqs are from proctor's built CMS
        function getPhrasebook(twoLetterLanguageCode) {
            return dataService.getData(cmsApiLocation + 'phrasebook/' + twoLetterLanguageCode, {}, 'GET',  cacheConstants.PHRASE_BOOK);
        }

        function getFaqs(twoLetterLanguageCode, twoLetterCountryCode) {
            //return dataService.getData(cmsApiLocation + 'faq-b2b/fr/fr', {}, 'GET', cacheConstants.FAQS)
            return dataService.getData(cmsApiLocation + 'faq-b2b/' + twoLetterLanguageCode + '/' + twoLetterCountryCode, {}, 'GET', cacheConstants.FAQS)
                .then(function(data){

                    if(data && data.nodes){
                        data.nodes = data.nodes.map(function(node){
                            node.node.answer = contentParserService.parseEmailsToLinks(node.node.answer);
                            return node;
                        });
                    }

                    return data;

                });
        }

        function getCountriesForCurrentLocale(twoLetterLanguageCode){

            var languageCode = localeService.getCurrentDeviceLanguageCode();
            return dataService.getData(apiLocation + 'countries/locale/' + languageCode, {}, 'GET', cacheConstants.COUNTRIES);
        }
    }


})();
