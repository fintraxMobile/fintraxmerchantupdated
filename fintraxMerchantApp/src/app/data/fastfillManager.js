(function(){
    angular
        .module('merchantApp.data')
        .factory('fastfillManager', fastfillManagerService);

    /* @ngInject */
    function fastfillManagerService(merchantAppConfig, $http) {

        var service = {
            attemptPassportScan: attemptPassportScan
        };

        return service;

        ////////////////

        function attemptPassportScan(temp) {


            convertImgToBase64URL('images/passport.jpg', sendToJumio, 'img/jpg');

            function sendToJumio(base64PassportImage) {

                var content = {
                    frontsideImage: base64PassportImage,
                    metadata: {type: 'PASSPORT'}
                };

                var req = {
                    method: 'POST',
                    url: merchantAppConfig.jumioFastFill.url,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'User-Agent': 'Fintrax Merchant/1.0.0',
                        'Authorization': 'Basic ' + merchantAppConfig.jumioFastFill.basicAuth
                    },
                    data: content
                };

                return $http(req);
            }

        }

        function convertImgToBase64URL(url, callback, outputFormat){
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'), dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
            };
            img.src = url;
        }


    }

})();