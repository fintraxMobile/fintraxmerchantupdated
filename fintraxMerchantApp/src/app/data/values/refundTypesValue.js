﻿(function () {
    'use strict';

    angular.module('merchantApp.data').value('refundTypesValue',
    [
        {
            id: 'card',
            refundCodes: ['0101', '0102', '0103', '0104', '0105', '0106'],
            translationKey: 'REFUND.CARD',
            description: 'REFUND.CARD_DESCRIPTION',
            disableWhenOffline:true,
            visible: true,
            disabled: false,
            navLocation: 'navigation.refundCard'
        },
        {
            id: 'store-cash',
            refundCodes: ['1301'],
            translationKey: 'REFUND.IN_STORE_CASH',
            description: 'REFUND.IN_STORE_CASH_DESCRIPTION',
            visible: true,
            disabled: false,
            disableWhenOffline:false,
            navLocation: 'navigation.refundReview.purchaseDetails'
        },
        {
            id: 'bank-transfer', 
            refundCodes: ['0901'],
            translationKey: 'REFUND.BANK_TRANSFER',
            description: 'REFUND.BANK_TRANSFER_DESCRIPTION',
            visible: true,
            disabled: false, 
            disableWhenOffline:false,
            dialog: 'refundBankTransferDialog',
            navLocation: 'navigation.refundCard'
        },
        {
            id:'airport-cash', refundCodes: ['0802'], 
            translationKey: 'REFUND.AIRPORT_CASH',
            description: 'REFUND.AIRPORT_CASH_DESCRIPTION',
            visible: true,
            disabled: false,
            disableWhenOffline:false,
            navLocation: 'navigation.refundReview.purchaseDetails'
        },
        {
            id:'cheque',
            refundCodes: ['0201',
                '0202',
                '0203',
                '0204',
                '0205',
                '0206',
                '0207',
                '0208',
                '0209',
                '0210',
                '0211',
                '0212'], translationKey: 'REFUND.CHEQUE', description: 'REFUND.CHEQUE_DESCRIPTION',
            visible: true, disabled: false, dialog: 'refundConfirmAddressDialog'
        },
        {
            id:'alipay', refundCodes: ['1545'], translationKey: 'REFUND.ALIPAY', description: 'REFUND.ALIPAY_DESCRIPTION',
            visible: true, disabled: false, dialog: 'refundAliPayDialog'
        },
        {
            id:'print', refundCodes: ['0000'], translationKey: 'REFUND.PRINT', description: 'REFUND.PRINT_DESCRIPTION',
            visible: true, disabled: false, disableWhenOffline:false, navLocation: 'navigation.refundReview.purchaseDetails'
        },
        {
            id:'topos', refundCodes: ['0601', '0602', '0603', '0604', '0605'], translationKey: 'REFUND.TOPOS', description: 'REFUND.TOPOS_DESCRIPTION',
            visible: true, disabled: false, disableWhenOffline:true, dialog: 'refundTOPOSDisclaimerDialog'
        },
        
        {
        	id:'ctrip', refundCodes: ['1702'], 
            translationKey: 'REFUND.CTRIP',
            description: 'REFUND.CTRIP_DESCRIPTION',
            visible: true,
            disabled: false,
            disableWhenOffline:false,
            dialog: 'refundCtripTransferDialog',
            navLocation: 'navigation.refundCtrip'
        	
        	
        }
    ]);
})();