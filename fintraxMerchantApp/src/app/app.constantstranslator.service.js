(function () {
    'use strict';

    angular
        .module('merchantApp')
        .factory('constantsTranslator', constantsTranslator);

    function constantsTranslator($translate, 
                                 dateDropdownConstants,layoutTypeConstants,voucherStatusConstants, titleConstants, paymentMethodConstants) {
        var service = {
            translate: translate
        };

        return service;

        //this service automagically translates keys in constants

        function translate()
        {
            translateKeys(dateDropdownConstants.months);
            translateKeys(voucherStatusConstants);
            translateKeys(layoutTypeConstants);
            translateKeys(titleConstants);
            translateKeys(paymentMethodConstants);
        }

        function translateKeys(data)
        {
            var translationKeys = data.map(function (i) { return i.translationKey; });

            $translate(translationKeys).then(function (translatedKeyValuePairs) {

                //translatedKeyValuePairs is an Object: {KEY1:abc, KEY2:def , ...}
                var keysArray = Object.keys(translatedKeyValuePairs); //[KEY1, KEY2, ...]
                keysArray.forEach(function (key, i) {
                    data[i].name = translatedKeyValuePairs[key];
                });
            });
        }

    }
})();
