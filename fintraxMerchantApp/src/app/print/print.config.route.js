﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher.print')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.printvoucher', {
            url: '^/printvoucher',
            controller: 'printVoucherController as vm',
            templateUrl: 'app/print/templates/printVoucher.html',
            data: {
                isVoucherCreationRoute:true
            }
        });

    }

})();
