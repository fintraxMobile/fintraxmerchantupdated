(function () {
    'use strict';
    angular
        .module('merchantApp.createVoucher.print')
        .controller('printVoucherController', Controller);

    function Controller($scope, $state, navigationService, commandService, merchantConfig,
                        createVoucherService, printService, translatedToastr, $cordovaSocialSharing,
                        pdfWrangler, $cordovaDevice, $window, newVoucherValue, $timeout,
                        utilsService, confirmExitVoucherService,$rootScope, fintraxApi) {
        /* jshint validthis: true */
        var vm = this;

        vm.pdfDataUrl = '';
        vm.domElementFromPDF = undefined;
        vm.timers = 'nothing yet...';
        vm.generating = true;
        
        $rootScope.$on('GENERATE_PDF', generatePdf);

        activate();

        function activate() {
            navigationService.pageClass('print-voucher');
           // commandService.layoutSelectVisible(true);
            commandService.removeAllButtons();
            
            
            if(localStorage.profile)
        	{
        
        		if(angular.fromJson(localStorage.profile).isoCountryCode==826)
    	   
        			{
    	   
        			  commandService.layoutSelectVisible(true);
    	   
    	   
        			}
       
      
        	}
            
            
            
          
            commandService.addButton('COMMANDS.CLOSE', close, 'left', false,
                undefined, commandService.BUTTON_TYPE.tertiary);
            navigationService.setTitle('PRINT_VOUCHER.PRINT_TITLE', 4);

            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            var voucher = {};

            fintraxApi.getCountries().then(function (countries){

                if(createVoucherService.voucherCreationResult() &&
                    typeof createVoucherService.voucherCreationResult().MerchantData !== 'undefined'){
                    voucher = createVoucherService.voucherCreationResult();
                    voucher.MerchantSignature = newVoucherValue.MerchantSignature;
                    voucher.CustomerSignature = newVoucherValue.CustomerSignature;

                    localiseOnlineVoucherCountry(countries, voucher);
                } else {

                    voucher = newVoucherValue;
                }

                merchantConfig.getMerchantConfig().then(function(config){

                    //generate PDF causes the UI to lock up, wait until it's rendered before calling the function
                    $timeout(function(){
                        generateVoucherPdf(config, voucher);
                    }, 1000);

                });

            });
        }

        function localiseOnlineVoucherCountry(countries, voucher){

            if(voucher.VoucherData && voucher.VoucherData.CustomerData &&
                voucher.VoucherData.CustomerData.ISOCountryOfOrigin){
                var currentVoucherISOCode = voucher.VoucherData.CustomerData.ISOCountryOfOrigin;
                var matchingCountry = findMatchingCountry(countries, currentVoucherISOCode);

                if(matchingCountry.length === 1){

                    voucher.VoucherData.CustomerData.CountryOfOrigin = matchingCountry[0].Name;
                }
            }
        }
        
        function generatePdf(layoutType,args)
        {
        	
        	 fintraxApi.getCountries().then(function (countries){
        		 var voucher = {};
        		 if(args.name)
        		 localStorage.layoutType=args.name;
                 if(createVoucherService.voucherCreationResult() &&
                     typeof createVoucherService.voucherCreationResult().MerchantData !== 'undefined'){
                     voucher = createVoucherService.voucherCreationResult();
                     voucher.MerchantSignature = newVoucherValue.MerchantSignature;
                     voucher.CustomerSignature = newVoucherValue.CustomerSignature;

                     localiseOnlineVoucherCountry(countries, voucher);
                 } else {

                     voucher = newVoucherValue;
                 }

                 merchantConfig.getMerchantConfig().then(function(config){

                     //generate PDF causes the UI to lock up, wait until it's rendered before calling the function
                    if(args!=null)
                    	{
                    	config.VoucherLayout=args.layOut;
                    	
                    	}
                	 
                	 $timeout(function(){
                    	 generateDynamicVoucherPdf(config, voucher);
                     }, 1000);

                 });

             });
        	
        }
        

        function findMatchingCountry(countries, iso3Digit){

            return countries.filter(function(country){

                return country.Iso3Digit === iso3Digit;
            });
        }

        function close() {

            confirmExitVoucherService.confirmExit().then(function(result){

                //checking for index 1 for cordova, true for web dev
                if(result === 1 || result === true){
                    navigationService.endWorkflow();
                    $state.go(utilsService.homeNavigationDecider());
                }
            });

        }

        function generateVoucherPdf(currentMerchantConfig, voucher) {

            var wranglerStart = new Date();
            vm.voucher=voucher;
           // clearRandpmInvoiceNumberForUkMerchants(currentMerchantConfig,voucher);
            pdfWrangler.generateVoucherPdf(currentMerchantConfig, voucher).then(function (result) {
                vm.pdfDataUrl = result.dataUrl;

                var wranglerEnd = new Date();
                var wrangleDiff = wranglerEnd - wranglerStart;

                var htmlStart = new Date();

                printService.pdfToDomElements(result.dataUrl).then(function (domElement) {
                    vm.generating = false;

                    document.getElementById('pdfHolder').appendChild(domElement);
                    vm.domElementFromPDF = domElement;

                    var htmlEnd = new Date();

                    var htmlDiff = htmlEnd - htmlStart;

                    vm.timers ='voucher engine time = ' + wrangleDiff + ' PDF.js render time = ' + htmlDiff;

                    commandService.addButton('COMMANDS.EMAIL', email, 'right');
                    commandService.addButton('COMMANDS.PRINT', print, 'right');
                });

            }, function (err) {
                vm.generating = false;
                translatedToastr.error('TOAST.UNABLE_TO_GENERATE_PDF');
            });

        }
        
        
        function generateDynamicVoucherPdf(currentMerchantConfig, voucher) {

            var wranglerStart = new Date();
            
            pdfWrangler.generateVoucherPdf(currentMerchantConfig, voucher).then(function (result) {
                vm.pdfDataUrl = result.dataUrl;

                var wranglerEnd = new Date();
                var wrangleDiff = wranglerEnd - wranglerStart;

                var htmlStart = new Date();

                printService.pdfToDomElements(result.dataUrl).then(function (domElement) {
                    vm.generating = false;

                    document.getElementById('pdfHolder').innerHTML="";
                    document.getElementById('pdfHolder').appendChild(domElement);
                    vm.domElementFromPDF = domElement;

                    var htmlEnd = new Date();

                    var htmlDiff = htmlEnd - htmlStart;

                    vm.timers ='voucher engine time = ' + wrangleDiff + ' PDF.js render time = ' + htmlDiff;

                });

            }, function (err) {
                vm.generating = false;
                translatedToastr.error('TOAST.UNABLE_TO_GENERATE_PDF');
            });

        }
        

        function email() {
            if ($window.cordova) {
                if ($cordovaDevice.getPlatform() === 'windows') {
                    //Windows needs the file to be written to disk for sharing
                    savePDFToDiskOnWindowsAndSendEmail(vm.pdfDataUrl);
                } else {
                    console.log('Non windows sending voucher:');
                    console.log(vm.pdfDataUrl);
                    sendEmail(vm.pdfDataUrl);
                }
            } else {
                translatedToastr.warning('TOAST.NO_CORDOVA', 'TOAST.ERROR');
            }
        }


        function savePDFToDiskOnWindowsAndSendEmail(pdfData) {
            var localFolder = Windows.Storage.ApplicationData.current.localFolder;
            return localFolder.createFileAsync('voucher.pdf', Windows.Storage.CreationCollisionOption.replaceExisting).then(function (file) {

                var base64 = pdfData.substring(28); //remove leading: data:application/pdf;
                var iBuffer = Windows.Security.Cryptography.CryptographicBuffer.decodeFromBase64String(base64);
                return Windows.Storage.FileIO.writeBufferAsync(file, iBuffer).then(function () {
                    console.log('File is saved to:');
                    console.log(file.path);
                    sendEmail(file.path);
                }, function (error) {
                    console.log('Error writing file.');
                    console.log(error);
                });
            });
        }


        function sendEmail(fileAttachment) {
            $cordovaSocialSharing.shareViaEmail('Please find your voucher attached.', 'Tax refund voucher', null, null, null, fileAttachment,successCallbackforEmail,errorCallbackforEmail);
        }
        
        function successCallbackforEmail(callbackParams)
        {
        	
        	
        	//debugger;
      
          
        }
        
        function errorCallbackforEmail(callbackParams)
        {
        	
        	
        	//debugger;
        	
        }
        
        


        function print() {

            var printResult = false;

            if(utilsService.isOnWindowsPlatform()){
                var fragment = document.createDocumentFragment();
                var container = document.createElement('div');
                container.innerHTML = vm.domElementFromPDF.outerHTML;
                fragment.appendChild(container);

                printResult = printService.print(fragment);
            } else {

              printResult = printService.print(vm.pdfDataUrl, true,vm.voucher);
            }


            if (!printResult) {
                translatedToastr.error('TOAST.UNABLE_TO_PRINT');
            }
        }
        
        function clearRandpmInvoiceNumberForUkMerchants(merchantConfig,voucherdata)
        {
        	
        	if(utilsService.isUkmerchant())
        		{
        		
        		
        		angular.forEach(voucherdata.VoucherData.LineItems, function(value, key) {
        		  
        		    if(value.InvoiceNumber.indexOf("MERCHAPP")>=0)
        		    	{
        		    	//clearing all the invoice number values for UK merchants
        		    	 value.InvoiceNumber="none";
        		    	
        		    	}
        		    
        		    
        		});
        		
        		   
        		
        		}
        	//debugger;
        	
        }


    }
})();
