(function () {
    angular
        .module('merchantApp.core')
        .filter('voucherStatusLookup', voucherStatusLookupFilter);

    /* @ngInject */
    function voucherStatusLookupFilter(voucherStatusConstants) {

        //Find a translated voucher status name for a given status IDs from the voucher status constants
        return function (voucherStatusId) {

            var foundStatus = voucherStatusConstants.filter(function(item){
                return item.statusId === voucherStatusId;

            });

            if (foundStatus.length === 1) {

                //name is automatically translated by the app.constanttranslator.service
                return foundStatus[0].name;
            } else {
                return 'N/A';
            }

        };

    }

})();
