(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('blinkIdPassportScan', blinkIdPassportScan);

        /* @ngInject */
        function blinkIdPassportScan(merchantAppConfig, $window){

            var service = {
                scanPassport:scanPassport
            };

            function scanPassport(countries, success, failure){


                var types = merchantAppConfig.blinkId.types;
                var iOSLicenseKey = merchantAppConfig.blinkId.iOSLicenseKey;
                var androidLicenseKey = merchantAppConfig.blinkId.androidLicenseKey;

                if($window.cordova && $window.cordova.plugins.blinkIdScanner){

                    cordova.plugins.blinkIdScanner.scan(
                        function(result){
                            blinkIdSuccess(result, countries, success, failure);
                        },
                        failure, types, iOSLicenseKey, androidLicenseKey);

                } else {
                    failure('Blink ID plugin not found!');
                }

            }

            function blinkIdSuccess(scanningResult, countries, success, failure){

                if(scanningResult.cancelled){
                    failure('cancelled');
                } else {

                     var mrtdResult = scanningResult.resultList.filter(function(result){
                         return result.resultType === 'MRTD result';
                     });

                     if(mrtdResult.length === 1){
                         mrtdResult = mrtdResult[0];

                         var result = {
                             dateOfBirth: mrtdResult.fields.DateOfBirth,
                             dateOfExpiry: mrtdResult.fields.DateOfExpiry,
                             documentCode: mrtdResult.fields.DocumentCode,
                             documentNumber: mrtdResult.fields.DocumentNumber,
                             issuer: mrtdResult.fields.Issuer,
                             raw: mrtdResult.fields.Raw,
                             nationality: mrtdResult.fields.Nationality,
                             opt1: mrtdResult.fields.Opt1,
                             opt2: mrtdResult.fields.Opt2,
                             paymentDataType: mrtdResult.fields.PaymentDataType,
                             primaryId: mrtdResult.fields.PrimaryId,
                             secondaryId: mrtdResult.fields.SecondaryId,
                             sex: mrtdResult.fields.Sex,
                         };

                         var parsedResult = {};

                         parsedResult.dateOfBirth = parseTwoDigitYearToFour(result.dateOfBirth, true);
                         parsedResult.dateOfExpiry = parseTwoDigitYearToFour(result.dateOfExpiry, false);
                         parsedResult.lastName = result.primaryId;
                         parsedResult.firstName = result.secondaryId;
                         parsedResult.passportNumber = result.documentNumber;


                         var foundCountry = countries.filter(function(c){
                             return c.Iso3Letter === result.nationality;
                         });

                         if(foundCountry.length === 1){
                             parsedResult.countryOfIssue = foundCountry[0];
                         }

                         success(parsedResult);

                     } else {
                         failure('Unable to find MRTD result');
                     }

                }
            }

            function parseTwoDigitYearToFour(inputYear, dateIsInPast){
                var year = inputYear.slice(0, 2);
                var month = inputYear.slice(2, 4);
                var day = inputYear.slice(4, 6);

                var currentYear = new Date().getFullYear();
                var currentYearFirstTwoDigit = (currentYear + '').slice(0, 2);
                var yearTest = currentYearFirstTwoDigit + year;

                if(dateIsInPast){
                    if(parseInt(yearTest, 10) > currentYear){
                        //must be born before 2000

                        year = (parseInt(currentYearFirstTwoDigit, 10) - 1) + year;
                    }
                } else {
                    if(parseInt(yearTest, 10) < currentYear){
                        //must be expiring in 21 something

                        year = (parseInt(currentYearFirstTwoDigit, 10) + 1) + year;
                    }
                }

                return new Date(year + '-' + month + '-' + day);
            }

            return service;
        }


})();
