(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('refundCalculationService', refundCalculationService);

    /* @ngInject */
    function refundCalculationService() {

        //charge structure Fee type enum from Fintrax's API
        //only percentage and fixed are supported
        var FEE_TYPE = {
            UNKNOWN: 0,
            PERCENTAGE: 1,
            FIXED: 2,
            MAX: 3
        };

        //correction type enum from Fintrax's API
        //only percentage, amount, and alternative charge structure are supported
        var REFUND_CORRECTION_TYPE = {
            UNASSIGNED: 0,
            PERCENTAGE: 1,
            AMOUNT: 2,
            INCOMING_REFUND_AMOUNT: 3,
            ALTERNATIVE_CHARGE_STRUCTURE: 4
        };

        //resultant statuses for the calculation process
        var CALCULATION_STATUS = {
            BELOW_TOTAL_THRESHOLD: 'below total threshold',
            BELOW_DAY_THRESHOLD: 'below day threshold',
            INVALID_REFUND_CODE: 'invalid refund code',
            NO_MATCHING_CHARGE_STRUCTURE: 'no matching charge structure found',
            UNSUPPORTED_FEE_TYPE: 'Unsupported fee type found, when calculating refund',
            OK: 'OK'
        };

        var INVOICE_PRICING = {
            UNIT_PRICE: 'unit',
            TOTAL_PRICE: 'total'
        };

        var service = {
            calculateRefund: calculateRefund,
            checkReceiptDateValidity: checkReceiptDateValidity,
            CALCULATION_STATUS: CALCULATION_STATUS
        };

        return service;

        //this service assumes the voucher is in the format shown here
        //https://apidev.fintrax.com:4856/Help/Api/POST-api-Vouchers

        function calculateRefund(merchantConfig, voucher) {

            //what kinda refund is it?
            //this will be null or undefined for most of the app,
            //as the total needs to be calculated before a refund type is chosen
        	//added static until EAPI release for CTRIP

        	
        	
        
        	
               var refundCode = null;

            if(voucher.RefundOption){
                refundCode = voucher.RefundOption.RefundOptionCode;
                
               var optionCodelength= merchantConfig.RefundOptions.filter(function (option) {

                    return option.RefundCode === refundCode;

                }).length;
               
             if(optionCodelength===0)
            	 {
            		var ctripStatic={
                			IsCardRefund:false,
                			IsCashRefund:false,
                			RefundCode:"1702",
                			RefundCodeDescription:"CTRIP"
                			
                			
                	}
                	merchantConfig.RefundOptions.push(ctripStatic);
            	 
            	 
            	 }
               
            }

            //VIP refund rate (or not)
            var voucherFormula = voucher.VoucherFormula;

            var result = {
                totalRefund:0.0,
                status:CALCULATION_STATUS.OK,
                currencySymbol:merchantConfig.Store.CurrencySymbol
            };

            //if it's a report voucher, the gross refund amount will already be there
            //so we don't need to calculate it
            if(typeof voucher.GrossRefundAmount !== 'undefined'){

                result.totalRefund = voucher.GrossRefundAmount;
                return result;
            }

            var uniqueVatRates = findUniqueVatRates(merchantConfig.ChargeStructures);

            //check to see if the current voucher values are over a given threshold amount
            var thresholdStatus = checkVoucherAmountIsOverThreshold(merchantConfig.Store.VerifyThresholdByDay,
                merchantConfig.Store.MinimumAmount, voucher);

            if(thresholdStatus !== CALCULATION_STATUS.OK){
                result.status = thresholdStatus;
                result.totalRefund = 0;

                voucher.RefundAmount = 0;
                return result;
            }

            //check refund is a valid type
            var foundRefundOption = merchantConfig.RefundOptions.filter(function (option) {
                return option.RefundCode === refundCode;
            });

            if (refundCode && foundRefundOption.length !== 1) {
                result.status = CALCULATION_STATUS.INVALID_REFUND_CODE;
                result.totalRefund = 0;

                voucher.RefundAmount = 0;
                return result;
            }

            var filteredChargeStructures = findDefaultChargeStructures(merchantConfig.Store.ChargeStructureId,
                merchantConfig.ChargeStructures, foundRefundOption, merchantConfig.ChargeStructureCorrections);

            var vipChargeStructures = findVipChargeStructures(voucherFormula, merchantConfig.ChargeStructures);

            //bundle all totals by VAT code, so calculations can be made on the totals
            var totalsGroupedByVAT = groupTotalsByVatCode(voucher, uniqueVatRates);

            //calculate the default overall refund, for each of the line items in the voucher
            var calculatedResult = calculateRefundAmountForItems(totalsGroupedByVAT, filteredChargeStructures, vipChargeStructures);

            if(result.status !== CALCULATION_STATUS.OK){
                result.totalRefund = 0;

                voucher.RefundAmount = 0;
                return result;
            }

            result.status = calculatedResult.status;
            result.totalRefund = calculatedResult.totalRefund;

            //add any applicable corrections (EXCEPT alternate charge structures, that's handled above!)
            result.totalRefund += calculateCorrectionAmount(merchantConfig.ChargeStructureCorrections, refundCode, result.totalRefund);

            if(isNaN(result.totalRefund)){

                voucher.RefundAmount = 0;
                throw 'calculated refund is NaN! Something bad has happened in the refund calculation';
            } else {

                if(refundIsTopos(refundCode)){

                    result.totalRefund -= calculateToposFee(result.totalRefund, merchantConfig.ToposRefundFees);
                }

                appendCalculatedValuesToVoucher(voucher, result, merchantConfig);
            }

            if(isNaN(result.totalRefund)) {

                voucher.RefundAmount = 0;
                throw 'calculated refund is NaN! Something bad has happened in the refund calculation';
            }

            return result;
        }

        function findUniqueVatRates(chargeStructures) {

            var tempVatRates = {};
            chargeStructures.forEach(function (structure) {
                if (angular.isDefined(structure.VatPercentageappliesTo)) {
                    tempVatRates[structure.VatPercentageappliesTo] = true;
                }
            });

            return Object.keys(tempVatRates).map(function (key) {
                return parseFloat(key, 10);
            }).sort(function(a,b){return b - a;});
        }

        function findVipChargeStructures(voucherFormula, chargeStructures) {
            //voucherFormula is the from the VIP dropdown, or null if not set

            var filteredChargeStructures = [];

            //find the appropriate charge structures from which to calculate the refund
            if ((voucherFormula !== null && typeof voucherFormula !== 'undefined') && voucherFormula.length > 0) {

                //use matching, non-default charge structure instead of regular one
                filteredChargeStructures = chargeStructures.filter(function (structure) {

                    return !structure.DefaultStructure && structure.VoucherFormulaCode.toUpperCase() === voucherFormula.toUpperCase();
                });

            }

            return filteredChargeStructures;
        }

        function findDefaultChargeStructures(doubleDefaultChargeStructureId, chargeStructures, foundRefundOption, chargeStructureCorrections){

            //stupidly there might be multiple structures with DefaultStructure = true
            //so we have to look it up by Id too

            //if there's no voucher formula, it must be one of the default structures
            var filteredChargeStructures = chargeStructures.filter(function (structure) {
                return structure.DefaultStructure && structure.Id === doubleDefaultChargeStructureId;
            });

            //UNLESS that's an alternative charge structure for the refund's correction...
            if (foundRefundOption.length === 1) {

                var foundCorrection = chargeStructureCorrections.filter(function (option) {
                    return option.RefundCode === foundRefundOption[0].RefundCode;
                });

                if (foundCorrection.length === 1 &&
                    foundCorrection[0].CorrectionType === REFUND_CORRECTION_TYPE.ALTERNATIVE_CHARGE_STRUCTURE) {

                    filteredChargeStructures = chargeStructures.filter(function (structure) {
                        return structure.DefaultStructure && structure.Id === foundCorrection[0].ChargeStructureId;
                    });

                }
            }

            return filteredChargeStructures;
        }

        function appendCalculatedValuesToVoucher(voucher, result, merchantConfig){

            voucher.RefundAmount = result.totalRefund;
            voucher.AmountIncludingVat = 0;
            voucher.AmountExcludingVat = 0;
            voucher.EligibleAmount = 0;
            voucher.VatAmount = 0;

            voucher.Invoices.map(function(invoice){

                voucher.AmountIncludingVat += invoice.GrossAmount;
                voucher.AmountExcludingVat += invoice.NetAmount;
                voucher.VatAmount += invoice.VatAmount;
                voucher.EligibleAmount += invoice.GrossAmount;
            });

            result.totalGrossAmount= voucher.AmountIncludingVat;
            voucher.IsoCountryCode = merchantConfig.Store.Country.Iso.toString();
            voucher.VatNumber = merchantConfig.Store.VatNumber;
        }

        function checkVoucherAmountIsOverThreshold(verifyThresholdByDay, thresholdAmount, voucher){

            //check the total amount of the voucher is over the eligibility threshold
            if(verifyThresholdByDay){

                var isOverDayThreshold = verifyThresholdAmountByDayAndInvoice(thresholdAmount, voucher);

                if(!isOverDayThreshold){
                    return CALCULATION_STATUS.BELOW_DAY_THRESHOLD;
                }

            } else {
                var isOverThreshold = verifyThresholdAmount(thresholdAmount, voucher);

                if (!isOverThreshold) {
                    return CALCULATION_STATUS.BELOW_TOTAL_THRESHOLD;
                }
            }

            return CALCULATION_STATUS.OK;
        }

        function verifyThresholdAmount(threshold, voucher){

            //assume all receipts are valid at this point
            var currentTotal = 0;

            if(!voucher.Invoices){
                return false;
            }

            voucher.Invoices.forEach(function(invoice){

                if(typeof invoice.GrossAmount === 'undefined') {
                    throw 'When verifying threshold, GrossAmount was missing from this Invoice!';
                }

                currentTotal += invoice.GrossAmount;

            });

            return roundToTwoDecimalPoints(currentTotal) >= threshold;
        }

        function roundToTwoDecimalPoints(num){

            return +(Math.round(num + 'e+2')  + 'e-2');
        }

        function verifyThresholdAmountByDayAndInvoice(threshold, voucher){

            //assume all receipts are valid at this point
            var today = new Date().toDateString();
            var invoicesForToday = [];
            var invoicesForOtherDays = [];
            var somePreviousDaysAreOverThreshold = false;
            var sumForToday = 0;

            if(!voucher.Invoices){
                return false;
            }

            //look at all the invoices, sum the total invoice amounts by day
            voucher.Invoices.forEach(function(invoice){

                var invoiceDate = new Date(invoice.InvoiceDateTime);
                var invoiceDay = new Date(invoiceDate.getFullYear(), invoiceDate.getMonth(), invoiceDate.getDate()).toDateString();

                if(typeof invoice.NetAmount === 'undefined') {
                    throw 'When verifying threshold by day, NetAmount was missing from this Invoice!';
                }

                if(invoiceDay === today){

                    sumForToday += invoice.NetAmount;
                    invoicesForToday.push(invoice);
                } else {

                    invoicesForOtherDays.push(invoice);

                    if(roundToTwoDecimalPoints(invoice.NetAmount) < threshold) {
                        invoice._ignoreInRefundCalculation = true;
                    } else {
                        somePreviousDaysAreOverThreshold = true;
                    }
                }
            });

            invoicesForToday.forEach(function (invoice) {

                invoice.__ignoreInRefundCalculation = sumForToday < threshold && !somePreviousDaysAreOverThreshold;
            });


            // If there's only one date and that date is today, only need to check that
            if(invoicesForToday.length > 0 && invoicesForOtherDays.length === 0){

                return roundToTwoDecimalPoints(sumForToday) >= threshold;
            }

            //we got some vouchers for today, and previous days
            if(invoicesForToday.length > 0 && invoicesForOtherDays.length > 0){

                return somePreviousDaysAreOverThreshold || roundToTwoDecimalPoints(sumForToday) >= threshold;
            }

            //only vouchers for today
            if(invoicesForToday.length === 0 && invoicesForOtherDays.length > 0){

                return somePreviousDaysAreOverThreshold;
            }

        }



        //receipts are only valid for a certain amount of days
        function checkReceiptDateValidity(merchantConfig, receipt){

            var diff = Math.abs(new Date() - new Date(receipt.InvoiceDateTime));

            return (diff / 86400000) <= merchantConfig.InvoiceEligibleNumberOfDays;
        }

        function groupTotalsByVatCode(voucher, uniqueVatRates){

            //build an object to hold the totals
            var groupedByVat = uniqueVatRates.map(function(rate){

                return {
                    rate:rate,
                    total:0,
                    totalVat:0
                };

            });

            //for all the invoices, calculate the total sum per VAT rate
            voucher.Invoices.forEach(function(invoice){

                if(!invoice._ignoreInRefundCalculation){

                    //check each unique VAT code
                    groupedByVat.forEach(function(vatGroup){

                        //all receipts have line items
                        var matchingLineItems = invoice.LineItems.filter(function(lineItem){

                            if(typeof lineItem.VatRate === 'undefined') {
                                throw 'VatRate is missing from a LineItem in this Invoice!';
                            }

                            return parseFloat(lineItem.VatRate, 10) === parseFloat(vatGroup.rate, 10);
                        });

                        //sum all values for each VAT rate
                        matchingLineItems.forEach(function(vatItem){

                            //calculation of unitprice vs total for lineitems is done in advancedReceipt
                            //so no need to differentiate here
                            vatGroup.total += vatItem.GrossAmount;

                            //VatAmount for a line item is calculated the same for unit or total pricing.
                            //see advancedReceipt.js computeNetGrossVatAndArticleCode function
                            vatGroup.totalVat += vatItem.VatAmount;

                        });

                    });

                }

            });

            return groupedByVat;
        }


        function calculateRefundAmountForItems(totalsGroupedByVAT, chargeStructures, vipChargeStructures) {
            var result = {
                totalRefund:0.0,
                status:CALCULATION_STATUS.OK
            };


            totalsGroupedByVAT.forEach(function (item,i) {

                if(item.total > 0) {

                    var matchingChargeStructure = findRelevantChargeStructureForItem(
                        chargeStructures,
                        item.rate,
                        item.total
                    );

                    // Groups of of totals are sorted from highest to lowest already, so the first one should always be the highest
                    if(i === 0 && vipChargeStructures.length !== 0){

                        var vipStructures = findRelevantChargeStructureForItem(
                            vipChargeStructures,
                            item.rate,
                            item.total
                        );

                        if(vipStructures.length > 0){
                            matchingChargeStructure = vipStructures;
                        } else {


                            result.totalRefund = 0;
                            result.status = CALCULATION_STATUS.NO_MATCHING_CHARGE_STRUCTURE;

                            return result;
                        }
                    }

                    if (matchingChargeStructure.length === 1) {

                        var structureToUse = matchingChargeStructure[0];

                        switch (structureToUse.FeeType) {
                            case FEE_TYPE.PERCENTAGE:
                                result.totalRefund += (structureToUse.PercentageRefund / 100) * item.total;
                                break;
                            case FEE_TYPE.FIXED:
                                result.totalRefund += structureToUse.RefundFee;
                                break;
                            case FEE_TYPE.MAX:
                                result.totalRefund += (item.totalVat - structureToUse.RefundFee);
                                break;
                            default:
                                result.totalRefund = 0;
                                result.status = CALCULATION_STATUS.UNSUPPORTED_FEE_TYPE;
                                return result;
                        }
                    } else {

                        //more than one, or no matching charge structure
                        result.status = CALCULATION_STATUS.NO_MATCHING_CHARGE_STRUCTURE;
                    }

                }

            });

            return result;
        }

        function calculateCorrectionAmount(chargeStructureCorrections, refundCode, currentRefundAmount) {
            var correctionAmount = 0;

            //check to see if there are any corrections for this refund
            var foundCorrection = chargeStructureCorrections.filter(function (option) {
                return option.RefundCode === refundCode;
            });

            //there's probably only ever one correction item, but just in case...
            foundCorrection.forEach(function (correction) {

                switch (correction.CorrectionType) {
                    case REFUND_CORRECTION_TYPE.PERCENTAGE:
                        correctionAmount += (correction.Correctionvalue / 100) * currentRefundAmount;
                        break;
                    case REFUND_CORRECTION_TYPE.AMOUNT:
                        correctionAmount += correction.Correctionvalue;
                        break;
                }
            });

            return correctionAmount;
        }

        function findRelevantChargeStructureForItem(chargeStructures, vatPercentage, totalAmount, isDefaultStructure) {

            return chargeStructures.filter(function (structureItem) {
                return parseFloat(structureItem.VatPercentageappliesTo, 10) === parseFloat(vatPercentage, 10) &&
                    parseFloat(structureItem.FromAmount, 10) <= roundToTwoDecimalPoints(totalAmount) &&
                    parseFloat(structureItem.ToAmount, 10) >= roundToTwoDecimalPoints(totalAmount);
            });

        }

        function refundIsTopos(refundOptionCode){

            //matching on a string is lame, but all TOPOS refund option codes start with '06'.
            return typeof refundOptionCode === 'string' && refundOptionCode.indexOf('06') === 0;
        }

        function calculateToposFee(totalRefund, toposFees){

            var fee = 0;

            if(toposFees && toposFees.length){

                var matchingFee = toposFees.filter(function(fee){

                    return totalRefund >= fee.FromAmount && totalRefund <= fee.ToAmount;

                });

                if(matchingFee.length === 1){

                    return matchingFee[0].RefundFee;
                }
            }

            return fee;
        }

    }

})();