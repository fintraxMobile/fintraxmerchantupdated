(function () {
    angular
        .module('merchantApp.core')
        .factory('pdfWrangler', pdfWrangler);											

    /* @ngInject */
    function pdfWrangler($window, $http, $q,utilsService) {
        var service = {
            generateVoucherPdf: generateVoucherPdf
        };

        return service;

        //Fintrax voucher renderer returns a blob url
        //this will return that Url, and a more useful dataUrl

        function generateVoucherPdf(merchantConfig, voucher) {

            var deferred = $q.defer();
            
            //for ctrip and other refund method values
            
            modifyVoucherData(voucher);
            
           // var dummyConfig = {
             //       VoucherLayout:''
                    	
               //  };
            
            
         // $window.Ptf.voucherDocument.registerMerchantConfig(dummyConfig);
           
            $window.Ptf.voucherDocument.registerMerchantConfig(merchantConfig);


            $window.Ptf.voucherDocument.generatePDFReport(voucher).then(function(voucherData){

                var pdfBlobUrl = voucherData.toBlobURL('application/pdf');

                //TODO replace this with angularificated version below
                var xhr = new XMLHttpRequest();
                xhr.open('GET', pdfBlobUrl, true);
                xhr.responseType = 'blob';

                xhr.onload = function(e) {
                    if (this.status === 200) {
                        var blobResult = this.response;

                        //convert the blob into a base64 data url, so it can be shared etc
                        var reader = new $window.FileReader();

                        reader.onloadend = function() {
                            deferred.resolve({ dataUrl: reader.result, blobUrl:pdfBlobUrl });
                        };

                        reader.onerror = function(err){
                            deferred.reject(err);
                        };

                        reader.onabort = function(err){
                            deferred.reject(err);
                        };

                        reader.readAsDataURL(blobResult);

                    } else {
                        deferred.reject('fail');
                    }
                };

                xhr.send();

                /*
                $http.get(pdfBlobUrl).then(function(getPdfBlobResult){

                    //convert the blob into a base64 data url, so it can be shared etc
                    var reader = new $window.FileReader();

                    reader.onloadend = function() {
                        deferred.resolve({ dataUrl: reader.result, blobUrl:pdfBlobUrl });
                    };

                    reader.onerror = function(err){
                        deferred.reject(err);
                    };

                    reader.onabort = function(err){
                        deferred.reject(err);
                    };

                    var blob = new Blob([getPdfBlobResult.data]);
                    reader.readAsDataURL(blob);

                });
                */

            }, function(err){

                deferred.reject(err);

            });


            return deferred.promise;
        }
        
        
       

      function modifyVoucherData(voucher)
      
      {
    	  //voucher data 
    	  
    	  if(voucher.VoucherData)
      	{
                voucher.VoucherData.PriceInclVatWords= utilsService.inWords( voucher.VoucherData.PriceInclVat);
         
               if(voucher.VoucherData.CustomerData)
              	 {
              	 
              	 
              	    if(voucher.VoucherData.CustomerData.PremierPassIdSpecified)
              		 {
              		 
              		    if(voucher.VoucherData.CustomerData.PremierPassId)
              		    	{
              		    	
              		    		voucher.VoucherData.CustomerData.PremierPassItems = utilsService.convertStringToArray(voucher.VoucherData.CustomerData.PremierPassId)    	
              		    	
              		    	}
              		 
              		 }
              	 
              	 }
               
               
               
                
                
                
                
      	
      	}
    	  
    	  
      }
        
       
        
        


    }

})();
