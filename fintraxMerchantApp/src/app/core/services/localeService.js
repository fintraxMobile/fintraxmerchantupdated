(function(){

    angular
        .module('merchantApp.core')
        .factory('localeService', localeService);

    /* @ngInject */
    function localeService($window) {

        var service = {
            getCurrentDeviceLanguageCode:getCurrentDeviceLanguageCode
        };

        var allowedLanguages = ['en', 'fr', 'de', 'pt', 'es', 'it'];
        var defaultLanguage = 'en';

        function getCurrentDeviceLanguageCode() {

            var deviceLanguage = $window.navigator.language.split('-')[0];

            if(allowedLanguages.indexOf(deviceLanguage) > -1){
                return deviceLanguage;
            } else {
                return defaultLanguage;
            }
        }

        return service;


    }


})();
