(function(){
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('confirmExitVoucherService', confirmExitVoucherService);

        function confirmExitVoucherService($translate, $cordovaDialogs, $window){

            return {
                confirmExit:confirmExit
            };

            function confirmExit(){


                var thingsToTranslate = ['COMMANDS.CANCEL', 'COMMANDS.CONFIRM', 'REFUND.EXIT_CONFIRMATION'];

                return $translate(thingsToTranslate).then(function (translatedKeyValuePairs) {

                    //translatedKeyValuePairs is an Object: {KEY1:abc, KEY2:def , ...}

                    var message = translatedKeyValuePairs['REFUND.EXIT_CONFIRMATION'];
                    var title =  translatedKeyValuePairs['COMMANDS.CONFIRM'];
                    var cancel = translatedKeyValuePairs['COMMANDS.CANCEL'];
                    var confirm = translatedKeyValuePairs['COMMANDS.CONFIRM'];
                    var buttonArray = [confirm, cancel];

                    if($window.cordova){
                        return $cordovaDialogs.confirm(message, title, buttonArray);
                    } else {

                        //dev mode for web debugging
                        return $window.confirm(message);
                    }
                });

            }

        }


})();
