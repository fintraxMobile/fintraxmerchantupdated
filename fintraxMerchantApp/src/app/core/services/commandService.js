(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('commandService', Buttons);

    function Buttons(navigationService) {
        var BUTTON_TYPE = {
            primary: 'primary button',
            secondary: 'secondary button',
            tertiary: 'tertiary button',
            svgIcon: 'svg-icon'
        };

        var showVipSelector = false;
        var showLayoutSelector=false;
        var showShopperSelector=false;
        var changeCssForTotal=false;
        var showTotal=false;
        //Button class
        function Button(text, clickCallback, float, isDisabledCallback,
                        isNavigationButton, buttonType, svgIconName, isFooter) {
            this.text = text;
            this.clickCallback = clickCallback;
            this.float = float;
            this.isDisabledCallback = isDisabledCallback;
            this.isNavigationButton = isNavigationButton;
            this.buttonType = buttonType || BUTTON_TYPE.primary;
            this.svg = false;
            this.isFooter = isFooter;
            this.svgIconName = '';

            if (typeof svgIconName !== 'undefined') {

                this.svgPath = 'images/svg/' + svgIconName + '.svg';
                this.svg = true;
                this.buttonType = BUTTON_TYPE.svgIcon;
                this.svgIconName = svgIconName;
            }
        }

        var buttons = restoreDefaultButtons();

        var service = {
            addButton: addButton,
            addSvgButton: addSvgButton,
            addFooterButton: addFooterButton,
            restoreDefaultButtons: restoreDefaultButtons,
            removeAllButtons: removeAllButtons,
            getButtons: getButtons,
            vipSelectVisible: vipSelectVisible,
            layoutSelectVisible:layoutSelectVisible,
            shopperEnableMenu:shopperEnableMenu,
            removeButtonThatHasText:removeButtonThatHasText,
            BUTTON_TYPE: BUTTON_TYPE,
            changeTotalCssClass:changeTotalCssClass,
            showTotalAmount:showTotalAmount
        };

        return service;

        function vipSelectVisible(isVisible) {

            if (typeof isVisible !== 'undefined') {
                showVipSelector = isVisible;
            }

            return showVipSelector;
        }
        
        function changeTotalCssClass(isChange)
        {
        	 if (typeof isChange !== 'undefined') {
        		 changeCssForTotal = isChange;
             }

        	return changeCssForTotal;
        }
        
        function showTotalAmount(isHide)
        {
        	 if (typeof isHide !== 'undefined') {
        		 showTotal = isHide;
             }

        	return showTotal;
        }
        
        function layoutSelectVisible(isVisible) {

            if (typeof isVisible !== 'undefined') {
                showLayoutSelector = isVisible;
            }

            return false;
        }
        
        function shopperEnableMenu(isVisible)
        {
        	if (typeof isVisible !== 'undefined') {
        		showShopperSelector = isVisible;
            }
        	
        	return showShopperSelector
        }

        //text = writing that appears on the button
        //clickCallback = a function to invoke when the button is clicked
        //float = 'left' or 'right' (defaults to right if not specified)
        //replacesNavigationButton - replace the left or right navigation button (Back / Next). Defaults to false
        //isDisabledCallback - callback to test if the button should be enabled or disabled (optional)
        //REMEMBER TO CALL restoreDefaultButtons() ON $scope.$on('$destroy', commandService.restoreDefaultButtons)
        function addButton(text, clickCallback, float, replacesNavigationButton,
                           isDisabledCallback, buttonType, svgIconName, isFooter) {
            if (isFooter === undefined) {
                isFooter = false;
            }

            if (replacesNavigationButton) {
                var index = findNavigationButtonIndex(float);
                if (index !== -1) {
                    buttons[index] = new Button(text, clickCallback, float,
                        isDisabledCallback, false, buttonType, svgIconName, isFooter);
                }
            } else {
                buttons.push(new Button(text, clickCallback, float,
                    isDisabledCallback, false, buttonType, svgIconName, isFooter));
            }
        }

        function removeButtonThatHasText(text){

            var matchingButton = buttons.filter(function(button){
                return button.text === text;
            });

            if(matchingButton.length === 1){
                buttons.splice(buttons.indexOf(matchingButton), 1);
            }
        }

        function addSvgButton(svgIconName, clickCallback, float, replacesNavigationButton, isDisabledCallback) {
            addButton(undefined, clickCallback, float, replacesNavigationButton, isDisabledCallback, BUTTON_TYPE.svgIcon, svgIconName);
        }


        function addFooterButton(text, clickCallback, buttonType) {
            addButton(text, clickCallback, undefined, false, undefined, buttonType, undefined, true);
        }


        function getButtons() {
            return buttons;
        }


        //This restores the default navigation buttons
        function restoreDefaultButtons() {

            buttons = [
                new Button(undefined, navigationService.gotoPreviousState, 'left',
                    function () { return !navigationService.hasPreviousState(); }, true, BUTTON_TYPE.svgIcon, 'back', false),

                new Button('COMMANDS.NEXT', navigationService.gotoNextState, 'right',
                    function () { return !navigationService.hasNextState(); }, true, BUTTON_TYPE.primary, undefined, false)
            ];

            vipSelectVisible(false);
            layoutSelectVisible(false);
            shopperEnableMenu(false);

            return buttons;
        }

        function removeAllButtons() {
            buttons = [];
        }
        
        function removeButtonsAtIndex( i) {
            buttons = [];
        }


        function findNavigationButtonIndex(float) {
            var matchingIndex = -1;
            buttons.forEach(function (button, index) {
                if (button.isNavigationButton && button.float === float) {
                    matchingIndex = index;
                }
            });
            return matchingIndex;
        }
        
        function layoutType(layoutType)
        {
        	
        	
        	return layoutType;
        }
        function printController(controller)
        {
        	
        	this.dynamicController=controller;
        	
        }
        
        
        
    }
})();
