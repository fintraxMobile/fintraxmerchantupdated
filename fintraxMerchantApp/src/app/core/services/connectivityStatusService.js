﻿(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('connectivityStatusService', connectivityStatusService);

    /* @ngInject */
    function connectivityStatusService($http, $q, $state, merchantAppConfig, translatedToastr, navigationService, merchantConfig) {
        return {
            isOnline: isOnline,    //returns a promise
            checkOnlineIfOnlineSetNav:checkOnlineIfOnlineSetNav
        };

        //Attempts to hit a pre-defined API end point. If success then assume we're online. Any error = either we're offline or the
        //fintraxAPI is down. In either case we assume we're in offline mode.
        function isOnline() {

            var apiLocation = merchantAppConfig.fintraxApi.url;

            //dev only
            //translatedToastr.warning('TOAST.CHECKING_ONLINE');

            return $http({
                url: apiLocation + 'merchants/current',
                method: 'HEAD',
                timeout: 5000
            })
            .then(function () { return true; }, function () { return false; }); 
        }

        function checkOnlineIfOnlineSetNav(){

            if(!navigationService.offline()){

                return isOnline().then(function(online){

                    if(!online){
                        navigationService.offline(true);

                        return merchantConfig.getMerchantConfig().then(function (mc) {

                            if(!mc.Store.AllowOfflineVouchers){

                                //offline, but not allowed offline mode.
                                translatedToastr.error('TOAST.NOT_ALLOWED_OFFLINE');
                                navigationService.endWorkflow();
                                $state.go('navigation.home');

                                return $q.reject('merchant not allowed to create vouchers offline');
                            } else {

                                var rangeCount = mc.VoucherRanges ? mc.VoucherRanges.length : 0;

                                if(rangeCount === 0){
                                    translatedToastr.error('TOAST.NO_VOUCHER_RANGES');

                                    navigationService.endWorkflow();
                                    $state.go('navigation.home');

                                    return $q.reject('offline eligible, but no voucher ranges left!');
                                } else {
                                    return $q.resolve(false);
                                }

                            }

                        });
                    } else {

                        return $q.resolve(online);
                    }

                });

            } else {
                return $q.resolve(false);
            }
        }
       
    }
})();
