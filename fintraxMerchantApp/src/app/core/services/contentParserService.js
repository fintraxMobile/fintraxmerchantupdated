(function(){
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('contentParserService', contentParserService);

        function contentParserService(){

            return {
                parseEmailsToLinks:parseEmailsToLinks
            };

            function parseEmailsToLinks(text){

                var emailsInContent =  text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);

                if(emailsInContent){

                    emailsInContent.forEach(function(email){

                        var linkString = '<a href="mailto:' + email + '" class="faq-email">' + email + '</a>';

                        text = text.replace(email, linkString);

                    });
                }

                return text;

            }

        }


})();
