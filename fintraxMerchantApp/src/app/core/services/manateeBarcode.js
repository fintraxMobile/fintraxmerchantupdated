(function () {
    angular
        .module('merchantApp.core')
        .factory('manateeBarcode', manateeBarcodeConfig);

    /* @ngInject */
    function manateeBarcodeConfig($window) {
        var service = {
            startScanning: startScanning,
            scannerAvailable:scannerAvailable
        };

        return service;

        function startScanning(callback){

            if($window.cordova && $window.scanner){

                $window.scanner.startScanning(init, callback);

            } else {
                callback(false);
            }

        }

        function scannerAvailable(){
          return $window.cordova && $window.scanner;
        }

        //This registration / init object is as per the example at https://github.com/manateeworks/phonegap-mwbarcodescanner
        //TODO replace with real registration details
        function init(mwbs, constants, dvc){

            console.log('Manatee barcode init Invoked at: '+ (new Date()).getTime());
            //change these registration settings to match your licence keys
            /* BEGIN Registration settings */

            //if your app doesn't work after setting license keys, try to uncomment the try-catch, and see what the error is

            //try {

            /*jshint camelcase: false */
            var mwregister = {
                'Android' : {
                    'MWB_CODE_MASK_25' : {'username' : 'jonathan.martin@fintrax.com', 'key' : '5AA903880E884A61C07E15EFB1273D7B142E335C5CCE053DD2E500C25168BD00'},
                    'MWB_CODE_MASK_39' : {'username':'jonathan.martin@fintrax.com','key':'4AB7656373A5806DC358CE6A1B152329394B0B918A26463090952771097F9708'},
                    'MWB_CODE_MASK_93' : {'username':'jonathan.martin@fintrax.com','key':'A20EB5594042381271B420F35F32A1D44C78FBE9C15F22D7BF2E3A663880A6FF'},
                    'MWB_CODE_MASK_128' : {'username':'jonathan.martin@fintrax.com','key':'0772F26000EAC106029762F6C12D3993E3E82ECD4C1883D7192EAA97330BEFA3'},
                    'MWB_CODE_MASK_AZTEC' : {'username':'jonathan.martin@fintrax.com','key':''},
                    'MWB_CODE_MASK_DM' : {'username':'jonathan.martin@fintrax.com','key':''},
                    'MWB_CODE_MASK_EANUPC' : {'username':'jonathan.martin@fintrax.com','key':''},
                    'MWB_CODE_MASK_PDF' : {'username':'jonathan.martin@fintrax.com','key':'E7627DBE436A1A8B16B6B4D60A7A292020140BFBBB520E1AD789B73206BF5A94'},
                    'MWB_CODE_MASK_QR' : {'username':'jonathan.martin@fintrax.com','key':'9E3D25585CFD8657421C14AA05EFBDF989BE11253267AC8A271D8141347E3F23'},
                    'MWB_CODE_MASK_RSS' : {'username':'jonathan.martin@fintrax.com','key':'E26D13B5210520EBB4195C1EFB16D6200E15305E45F969E36848F38CB7CB2518'},
                    'MWB_CODE_MASK_CODABAR' : {'username':'jonathan.martin@fintrax.com','key':'70ED10F1E3C0AE831EA36F6C391E92C301B8E76C94D712F2392A4E0784BC5962'},
                    'MWB_CODE_MASK_11' : {'username':'jonathan.martin@fintrax.com','key':'925B34E408F390D4775F81643E8275CBB12F30C60277B34C5A6261A80023D1B7'},
                    'MWB_CODE_MASK_MSI' : {'username':'jonathan.martin@fintrax.com','key':'F4193D02707A67E3B1D5357427E9D133C7BEFEF66C2FF6817F8C7369A0AED4FB'},
                    'MWB_CODE_MASK_DOTCODE' : {'username':'jonathan.martin@fintrax.com','key':''}
                },
                'iOS' :{
                    'MWB_CODE_MASK_25' : {'username' : 'Fintrax.C25.iOS.UDL', 'key' : 'EA718896BBFEAE19A95A9752804DC5F7ED9A8340C45BEC2EFC1B2681469782B1'},
                    'MWB_CODE_MASK_39' : {'username':'Fintrax.C39.iOS.UDL','key':'FC5B6DC2719DD06E0C4CDF4AF66ED267EF35218543A52CAE17E0439BAFCD3E40'},
                    'MWB_CODE_MASK_93' : {'username':'Fintrax.C93.iOS.UDL','key':'19C80B2C25B27A23A7212134C0435107B999FEEC865F10689D0A7A40FBB96350'},
                    'MWB_CODE_MASK_128' : {'username':'Fintrax.C128.iOS.UDL','key':'995CC0E9945E3A4B259AD81AB8D266800B8DD82B210BFD5FC1BA613306085058'},
                    'MWB_CODE_MASK_AZTEC' : {'username':'','key':''},
                    'MWB_CODE_MASK_DM' : {'username':'','key':''},
                    'MWB_CODE_MASK_EANUPC' : {'username':'','key':''},
                    'MWB_CODE_MASK_PDF' : {'username':'Fintrax.PDF.iOS.UDL','key':'1A9B6BC20B0735197B926DF2922FA18BA844434D3A8D8DB73B88DCEF2103E07F'},
                    'MWB_CODE_MASK_QR' : {'username':'Fintrax.QR.iOS.UDL','key':'CDA227AE48334FC06582023C693AE3A220F94EF7D4150D16576952B03913D8C3'},
                    'MWB_CODE_MASK_RSS' : {'username':'Fintrax.DB.iOS.UDL','key':'761968C04079C57ED7635EFC3F66BE5FC37FFB429EE442066F6D0A888A629C00'},
                    'MWB_CODE_MASK_CODABAR' : {'username':'Fintrax.CB.iOS.UDL','key':'F0DE9AAD87D8784C2AA0B77D195ED4FC327296F486F417B9951B283741A3D7DC'},
                    'MWB_CODE_MASK_11' : {'username':'Fintrax.C11.iOS.UDL','key':'84A29F98D1A6A8B322440369B9CF595C80F2E815C7706F02373238EFB21074E0'},
                    'MWB_CODE_MASK_MSI' : {'username':'Fintrax.MSI.iOS.UDL','key':'4FCED183CE67AB1BFDDFA4BF6E294A29CC5CB597FFB1B2E543E68A7D45934208'},
                    'MWB_CODE_MASK_DOTCODE' : {'username':'','key':''}
                },
                'Win32NT' : {
                    'MWB_CODE_MASK_25' : {'username' : '', 'key' : ''},
                    'MWB_CODE_MASK_39' : {'username':'','key':''},
                    'MWB_CODE_MASK_93' : {'username':'','key':''},
                    'MWB_CODE_MASK_128' : {'username':'','key':''},
                    'MWB_CODE_MASK_AZTEC' : {'username':'','key':''},
                    'MWB_CODE_MASK_DM' : {'username':'','key':''},
                    'MWB_CODE_MASK_EANUPC' : {'username':'','key':''},
                    'MWB_CODE_MASK_PDF' : {'username':'','key':''},
                    'MWB_CODE_MASK_QR' : {'username':'','key':''},
                    'MWB_CODE_MASK_RSS' : {'username':'','key':''},
                    'MWB_CODE_MASK_CODABAR' : {'username':'','key':''},
                    'MWB_CODE_MASK_11' : {'username':'','key':''},
                    'MWB_CODE_MASK_MSI' : {'username':'','key':''},
                    'MWB_CODE_MASK_DOTCODE' : {'username':'','key':''}
                }
            };
            //    }
            //    catch(e){
            //        console.log(e);
            //    }
            /* END registration settings */
            var platform = mwregister[dvc.platform];
            Object.keys(platform).forEach(function(reg_codes){
                mwbs['MWBregisterCode'](constants[reg_codes],platform[reg_codes]['username'],platform[reg_codes]['key']);
            });

            //settings portion, disable those that are not needed
            /* BEGIN settings CALLS */
            //if your code doesn't work after changing a few para
            // meters, and there is no error output, uncomment the try-catch, the error will be output in your console
            //    try{
            /*UNCOMMENT the lines you wish to include in the settings */
            //  mwbs['MWBsetInterfaceOrientation'] (constants.OrientationPortrait);
            //  mwbs['MWBsetOverlayMode'](constants.OverlayModeImage);
            //  mwbs['MWBenableHiRes'](true);
            //  mwbs['MWBenableFlash'](true);
            //  mwbs['MWBsetActiveCodes'](constants.MWB_CODE_MASK_128 | constants.MWB_CODE_MASK_39);
            //  mwbs['MWBsetLevel'](2);
            //  mwbs['MWBsetFlags'](constants.MWB_CODE_MASK_39, constants.MWB_CFG_CODE39_EXTENDED_MODE);
            //  mwbs['MWBsetDirection'](constants.MWB_SCANDIRECTION_VERTICAL | constants.MWB_SCANDIRECTION_HORIZONTAL);
            //  mwbs['MWBsetScanningRect'](constants.MWB_CODE_MASK_39, 20,20,60,60);
            //  mwbs['MWBenableZoom'](true);
            //  mwbs['MWBsetZoomLevels'](200, 400, 0);
            //  mwbs['MWBsetMinLength'](constants.MWB_CODE_MASK_39, 4);
            //  mwbs['MWBsetMaxThreads'](1);
            //  mwbs['MWBcloseScannerOnDecode'](false);
            //  mwbs['MWBuse60fps'](true);
            //    }
            //    catch(e){
            //        console.log(e);
            //    }

        }


    }

})();
