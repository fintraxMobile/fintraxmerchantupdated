(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('translatedToastr', translatedToastr);

    /* @ngInject */
    function translatedToastr(toastr, $translate, toastrConfig) {

        var service = {
            success:success,
            error:error,
            warning:warning
        };

        toastrConfig.positionClass = 'toast-bottom-left';
        toastrConfig.preventOpenDuplicates = true;
        toastrConfig.containerId = 'toast-container';

        return service;

        function translateContent(bodyKey, titleKey, makeTheToast){

            if(typeof titleKey !== 'undefined'){

                $translate([bodyKey, titleKey]).then(function(translation){
                    makeTheToast(translation[bodyKey], translation[titleKey]);
                });

            } else {

                $translate(bodyKey).then(function(translation){
                    makeTheToast(translation);
                });

            }
        }

        function success(bodyKey, titleKey, config){
            translateContent(bodyKey, titleKey, toastr.success, config);
        }

        function error(bodyKey, titleKey, config){
            translateContent(bodyKey, titleKey, toastr.error, config);
        }

        function warning(bodyKey, titleKey, config){
            translateContent(bodyKey, titleKey, toastr.warning, config);
        }


    }

})();
