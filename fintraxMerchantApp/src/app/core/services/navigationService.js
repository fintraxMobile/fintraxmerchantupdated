﻿(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('navigationService', Navigation);

    function Navigation($state, $rootScope) {
        var states = [];
        var selectedStateIndex = 0;
        var title = '';
        var sectionNumber;
        var classOfPage = '';
        var infoContent = {};
        var isOffline = false;

        var service = {
            setTitle: setTitle,
            getTitle: getTitle,
            getSectionNumber: getSectionNumber,
            endWorkflow: endWorkflow,
            pageValidityChanged: pageValidityChanged,
            gotoNextState: gotoNextState,
            gotoPreviousState: gotoPreviousState,
            hasPreviousState: hasPreviousState,
            hasNextState: hasNextState,
            pageClass: pageClass,
            offline: offline,
            footerInfo: footerInfo
        };

        return service;

        function setTitle(pageTitle, areaSectionNumber) {
            title = pageTitle;
            sectionNumber = areaSectionNumber; //this highlights one of the numbers below the title
        }


        function getTitle() {
            return title;
        }

        function getSectionNumber() {
            return sectionNumber;
        }

        function endWorkflow() {
            states = [];
            selectedStateIndex = 0;
        }


        //designed to be called from a $watch expression
        function pageValidityChanged(valid, oldValid, nextState, futureIsUncertain) {
            if (valid !== oldValid) {
                setStateValidity(nextState, valid, futureIsUncertain);
            }
        }


        function setStateValidity(nextState, isValid, futureIsUncertain) {
            var currentState = $state.current.name;
            if (states.indexOf(currentState) === -1) {
                states.push(currentState);
            }

            if (futureIsUncertain) {
                removeAllAfterCurrentState(currentState);
            }

            if (isValid) {
                if (states.indexOf(nextState) === -1) {
                    states.push(nextState);
                }
            } else {
                removeAllAfterCurrentState(currentState);
            }
        }


        function removeAllAfterCurrentState(currentState) {
            var index = states.indexOf(currentState);
            states.splice(index + 1, states.length - index); //remove all elements after currentState
        }


        function gotoNextState() {
            if (hasNextState()) {
                $state.transitionTo(states[++selectedStateIndex]);
            }
        }

        function gotoPreviousState() {
            if (hasPreviousState()) {
                $state.transitionTo(states[--selectedStateIndex]);
            }
        }


        function hasPreviousState() {
            return selectedStateIndex > 0;
        }


        function hasNextState() {
            return selectedStateIndex < states.length - 1;
        }

        function pageClass(newClass) {
            if (typeof newClass !== 'undefined') {
                $rootScope.classOfPage = newClass;
            }

            return $rootScope.classOfPage;
        }

        function offline(appIsOffline) {
            if (typeof appIsOffline !== 'undefined') {
                isOffline = appIsOffline;
            }

            return isOffline;
        }

        function footerInfo(newFooterInfo) {
            if (typeof newFooterInfo !== 'undefined') {
                infoContent = newFooterInfo;
            }
            return infoContent;
        }
    }
})();
