﻿(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('notificationService', NotificationService);

    function NotificationService($window, $translate, $rootScope, merchantAppConfig) {
        var interval;
        return {
            scheduleNotification: scheduleNotification,
            clearTriggeredNotification: clearTriggeredNotification
        };


        //Under iOS we can't schedule a notification in the onPause event (something about it running objective C code).
        //Instead, we'll schedule a notification for one tick's time. On every tick of the scheduler, we'll nudge it forward
        //in time so that it only fires if the app is suspended.
        function createNotification(queueLength) {
            createOrUpdateNotification(queueLength, true);
        }

        function updateNotification(queueLength) {
            createOrUpdateNotification(queueLength, false);
        }



        function scheduleNotification(queueLength) {
            if ($window.cordova && $rootScope.isPaused === false &&
              $window.cordova.plugins && $window.cordova.plugins.notification) {
                cordova.plugins.notification.local.getIds(function (ids) {
                    var notificationExists = ids.indexOf(1) !== -1;

                    if (queueLength === 0 && notificationExists) {
                        cordova.plugins.notification.local.cancel(1);
                    } else if (queueLength > 0) {
                        if (notificationExists) {
                            updateNotification(queueLength);
                        } else {
                            createNotification(queueLength);
                        }
                    }
                });
            }
        }


        //To be called when the application resumes. If the notification has triggered, then it will never show again.
        //We need to clear it and re-schedule it.
        function clearTriggeredNotification() {
            if ($window.cordova && $window.cordova.plugins && $window.cordova.plugins.notification) {
                cordova.plugins.notification.local.getTriggeredIds(function (ids) {
                    if (ids.indexOf(1) !== -1) {
                        cordova.plugins.notification.local.clear(1);
                    }
                });
            }
        }

        function createOrUpdateNotification(queueLength, create) {

          if($window.cordova && $window.cordova.plugins && $window.cordova.plugins.notification){
            $translate(['NOTIFICATIONS.UNPROCESSED_ITEMS', 'NOTIFICATIONS.UNPROCESSED_ITEMS_MESSAGE']).then(function (translatedKeyValuePairs) {
                var now = new Date().getTime();
                var nextTick = new Date(now + merchantAppConfig.schedulerInterval + 2000);
                if (create) {
                    cordova.plugins.notification.local.schedule({
                        id: 1,
                        title: translatedKeyValuePairs['NOTIFICATIONS.UNPROCESSED_ITEMS'],
                        text: translatedKeyValuePairs['NOTIFICATIONS.UNPROCESSED_ITEMS_MESSAGE'] + ': ' + queueLength,
                        at: nextTick
                    });
                } else {
                    cordova.plugins.notification.local.update({
                        id: 1,
                        text: translatedKeyValuePairs['NOTIFICATIONS.UNPROCESSED_ITEMS_MESSAGE'] + ': ' + queueLength,
                        at: nextTick
                    });
                }
            });

          }

        }

    }
})();
