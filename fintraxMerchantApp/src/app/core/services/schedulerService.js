﻿(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('schedulerService', Scheduler);

    function Scheduler($interval, offlineQueue) {
        var interval;
        return {
            start: start,
            stop: stop //probably never need this, but just for completeness
        };


        function start(updateInterval) {            
            interval = $interval(offlineQueue.process, updateInterval);
        }


        function stop() {
            if (angular.isDefined(interval)) {
                $interval.cancel(interval);
                interval = undefined;
            }
        }

    }
})();
