﻿(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .factory('offlineQueue', offlineQueue);

    /* @ngInject */
    function offlineQueue(connectivityStatusService, fintraxApi, cacheService, createVoucherService,
                          cacheConstants, $q, notificationService, merchantConfig) {
        var queue;

        return {
            init: init,
            enqueueCreateVoucher: enqueueCreateVoucher,
            enqueueVoidVoucher: enqueueVoidVoucher,
            process: processQueue, //this gets called by the scheduler each tick
            queueLength: queueLength
        };

        function init() {
            //Load the queue from storage, or create a new blank one if it's not there.
            return cacheService.get(cacheConstants.OFFLINE_QUEUE).then(function (queueFromCache) { queue = queueFromCache; }, function () { queue = []; });
        }

        function enqueueCreateVoucher(voucher) {
            return cacheService.bulkSet(cacheConstants.VOUCHER_LIST_QUEUED, [voucher]).then(function () {
                enQueue({ command: 'CreateVoucher', voucher: voucher });
            });
        }

        function enqueueVoidVoucher(voucherNumber) {
            return enQueue({ command: 'VoidVoucher', voucherNumber: voucherNumber });
        }

        function enQueue(instruction) {
            queue.push(instruction);
            return cacheService.set(cacheConstants.OFFLINE_QUEUE, queue);
        }

        function refreshVoucherRanges() {
            return merchantConfig.getMerchantConfig().then(function(mc) {
                console.log('refreshVoucherRanges from offline queue');

                if (!mc.Store.AllowOfflineVouchers) {
                    return;
                }

                var rangeCount = mc.VoucherRanges ? mc.VoucherRanges.length : 0;

                if (rangeCount >= 2) {
                    return;
                }

                return fintraxApi.getVoucherRanges(mc.Store.pftShopNumber)
                    .then(function(voucherRanges) {
                        console.log('Got voucher ranges, replacing...');

                        if (angular.isDefined(voucherRanges) && voucherRanges.length) {
                            return merchantConfig.replaceVoucherRanges(voucherRanges)
                    } 
                });
            });
        }

        function processQueue() {
            notificationService.scheduleNotification(queueLength());

            if (queue.length > 0) {

                connectivityStatusService.isOnline().then(function (online) {
                    console.log('Processing queue! Online: ' + online);
                    console.log(queue);
                    if (online) {
                        processQueueOnline(queue).then(function () {
                            console.log('queue processed OK');
                            refreshVoucherRanges();
                        },
                            function () {
                                console.log('Queue could not be processed');
                            }
                        );
                    }
                });

            }
        }

        //Recursive function shouldn't kill the stack because the stack starts fresh for async operations. In theory.
        function processQueueOnline(queue) {
            if (queue.length === 0) {
                return $q.defer().resolve();
            }
            var instruction = queue[0];
            switch (instruction.command) {
                case 'CreateVoucher':
                    console.log('Processing CreateVoucher command');
                    return processCreateVoucherCommand(instruction.voucher, queue).then(processQueueOnline);
                case 'VoidVoucher':
                    console.log('Processing VoidVoucher command');
                    return processVoidVoucherCommand(instruction.voucherNumber, queue).then(processQueueOnline);
            }
        }


        function processCreateVoucherCommand(voucher) {

            if(!voucher.RefundAmount){
                console.log('Something terrible has happened - the voucher has no refund amount! Removing it from the queue.');
                return deQueue();
            }

            return createVoucherService.createVoucherOnline(angular.copy(voucher)).then(function(){

                console.log('deQueueing successful voucher creation');
                return deQueue();
            }, function(error){

                console.log('deQueueing failed voucher creation');
                console.log(error);

                //failed to update tourist, or create voucher, throw it out
                return deQueue();
            });

            //See https://fintraxitqatest.atlassian.net/browse/PEA-453

            //if tourist exists in auth0, attempt to find tourist

            //if can't find tourist, throw voucher out (?)

            //if tourist found, update voucher with found tourist (replace address etc with new data)

            //handle connection error, reject promise

            //attempt voucher creation

            //if can't create voucher, throw voucher out (?)

            //no error handler, so item stays in the queue and processing stops.
        }


        function processVoidVoucherCommand(voucherNumber) {
            return fintraxApi.voidVoucher(voucherNumber).then(function (response) {

                return deQueue();
            }, function (error) {

                //error voiding voucher, throw it out of the queue
                return deQueue();
            });
        }

        function deQueue() {
            queue.splice(0, 1); //remove head of the queue
            return cacheService.set(cacheConstants.OFFLINE_QUEUE, queue);
        }


        function queueLength() {
            if (queue) {
                return queue.length;
            }
            return -1;
        }

        //Just for testing!
        //function processTestCommand(customer, queue) {
        //    console.log('Processing:');
        //    console.log(customer);

        //    var waitForInterval = $q.defer();

        //    $interval(function () {
        //        if (customer === 'huw') {
        //            waitForInterval.reject();
        //        } else {
        //            queue.splice(0, 1); //remove head of the queue
        //            waitForInterval.resolve();
        //        }
        //    }, 500, 1);

        //    return waitForInterval.promise.then(function () {
        //        return queue;
        //    });
        //}

    }
})();
