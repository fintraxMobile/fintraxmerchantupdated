(function () {

    angular
        .module('merchantApp.core')
        .factory('printService', printService);

    /* @ngInject */
    function printService($window, $cordovaPrinter, $cordovaSocialSharing, $cordovaDevice, $q,utilsService) {
        var service = {
            print: myprint,
            pdfToDomElements: pdfToDomElements
        };

        var PRINT_SUPPORT = {
            pluginUri: 1,
            pluginHtml: 2,
            socialShare: 3,
            windows: 4
        };
        return service;

        //the print function expects a full HTML document
        //assume this service is only used by android 4.4+, windows, and iOS 8
        function myprint(contentToPrint, isPDF,voucher) {
            if ($window.cordova) {
                var printType = findPrintTypeForDevice(isPDF);
                
                switch (printType) {
                    case PRINT_SUPPORT.pluginUri:
                        $cordovaPrinter.print(contentToPrint);
                        
                        
                        
                        
                        return true;
                    case PRINT_SUPPORT.pluginHtml:

                        if(isPDF){
                            pdfToDomElements(contentToPrint).then(function (domElement) {
                            	 if(device.platform==='Android')
                             	{
                             	
                            		if(localStorage.layoutType)
                            			{
                            			//for thermal in future
                            			  if(localStorage.layoutType=="Thermal")
                            		        domElement=angular.toJson(voucher.VoucherData);
                             	
                            			}
                            		
                            		else
                            			{
                            			
                            			$cordovaPrinter.print(contentToPrint.split(',')[1]);
                            			return true;
                            			
                            			}
                            		
                             	}	
                            	
                                $cordovaPrinter.print(domElement);
                            });
                        } else {
                            $cordovaPrinter.print(contentToPrint);
                        }

                        return true;

                    case PRINT_SUPPORT.socialShare:
                        //use pdf data uri to share to some external service
                        $cordovaSocialSharing.share(null, 'voucher', contentToPrint, null);
                        return true;

                    case PRINT_SUPPORT.windows:
                        //assumes contentToPrint is a html document
                        printForWindows(contentToPrint);
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }

        function printForWindows(contentToPrint){

          var printDocumentSource = null;
          var printManager = Windows.Graphics.Printing.PrintManager.getForCurrentView();
          printManager.onprinttaskrequested = onPrintTaskRequested;

          //This is what gets called when the user hits the print button (on windows)
          function onPrintTaskRequested(printEvent) {

              printEvent.request.createPrintTask('Fintrax Print', function (args) {
                  args.setSource(printDocumentSource);
              });
          }

          printDocumentSource = MSApp.getHtmlPrintDocumentSource(contentToPrint);

          // If the print contract is registered, the print experience is invoked.
          Windows.Graphics.Printing.PrintManager.showPrintUIAsync();

        }

        //This returns a promise, which resolves to a DOM element with a bunch of canvases attached
        function pdfToDomElements(voucherPdfDataUri) {
            PDFJS.disableWorker = true;
            var pdfAsArray = convertDataURIToBinary(voucherPdfDataUri);

            return PDFJS.getDocument(pdfAsArray).then(function renderPages(pdfDoc) {

                var promiseArray = [];
                for (var num = 1; num <= pdfDoc.numPages; num++) {
                    promiseArray.push(pdfDoc.getPage(num).then(renderPage));
                }

                return $q.all(promiseArray).then(function (returnCanvasArray) {

                    var parentDiv = document.createElement('div');

                    returnCanvasArray.forEach(function (canvas) {

                        var image = document.createElement('img');
                        image.src = canvas.toDataURL('image/png');
                        
                        image.style.width = canvas.style.width;
                        image.style.height = canvas.style.height;

                        parentDiv.appendChild(image);

                    });

                    return parentDiv;
                });
            });
        }


        function renderPage(page) {
            var viewport = page.getViewport(2); //2 is the scale factor
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };

            canvas.height = viewport.height;
            canvas.width = viewport.width;
            canvas.style.height = viewport.height / 2 + 'px';
            canvas.style.width = viewport.width / 2 + 'px';

            return page.render(renderContext).promise.then(function(){
                return canvas;
            });
        }


        function findPrintTypeForDevice(isPDF) {
            var platform = $cordovaDevice.getPlatform();
            if (platform === 'iOS') {
            	 
                return isPDF ? PRINT_SUPPORT.pluginUri : PRINT_SUPPORT.pluginHtml; //We only prefer uri for ios if it's a pdf. Otherwise we send html
            } else if (platform === 'Android') {

                //older versions of android don't support printing, we'll social share it instead
                var version = $cordovaDevice.getVersion();
                if (parseFloat(version, 10) >= 4.4 && $cordovaPrinter.isAvailable()) {
                    return PRINT_SUPPORT.pluginHtml;
                }
                return PRINT_SUPPORT.socialShare;
            } else if (platform === 'windows') {
                return PRINT_SUPPORT.windows;
            }
        }


        function convertDataURIToBinary(dataURI) {
            var BASE64_MARKER = ';base64,';
            var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
            var base64 = dataURI.substring(base64Index);
            var raw = window.atob(base64);
            var rawLength = raw.length;
            var array = new Uint8Array(new ArrayBuffer(rawLength));

            for (var i = 0; i < rawLength; i++) {
                array[i] = raw.charCodeAt(i);
            }
            return array;
        }
        
       



    }
})();
