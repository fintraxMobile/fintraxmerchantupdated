(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .constant('merchantAppConfig', config());

        function config() {

            return {
                auth0Merchant: {
                    domain: window.config.auth0Merchant.domain,
                    clientId: window.config.auth0Merchant.clientId,
                    connection:window.config.auth0Merchant.connection,
                    audience:window.config.auth0Merchant.audience
                },
                auth0Tourist: {
                    domain: window.config.auth0Tourist.domain,
                    clientId: window.config.auth0Tourist.clientId,
                    connection:window.config.auth0Tourist.connection
                },
                passworks: {
                    premierPassCampaignId: '0cd08d01-dab6-4a04-8526-ca66a3752dcd'
                },
                jumioCardScan: {
                    encryptionKey: 'UgQPjvZLkdwuBMnrkgFkeONPos7WX95YBoNMf9/UF9U=',
                    checksumKey: 'HhSBzd4y3rSvx4oFP8IlF3Xp+dtbli79RC0WKyojYHw=',
                    reference: 'MubalooRef1',
                    publicIdentifier: 'a6b8e45c-62e5-486e-8f50-e7c4a29a9568'
                },
                jumioFastFill:{
                    url:'https://netverify.com/api/netverify/v2/fastfill',
                    apiToken:'5a394622-5a5b-499b-8749-4eb948d4a369',
                    apiSecret:'s2d7TSkuq3R1Vxl4AGNEdJ02sZuD1ju2',
                    basicAuth:'NWEzOTQ2MjItNWE1Yi00OTliLTg3NDktNGViOTQ4ZDRhMzY5OnMyZDdUU2t1cTNSMVZ4bDRBR05FZEowMnNadUQxanUy'

                },
                fintraxApi: {
                    url: window.config.fintraxApi.url
                },
                cmsApi: {
                    url: 'http://premiertaxfree.com/json/'
                },
                addReceiptScenarios: {
                    //receipt number, no line items
                    simple1: 'SIMPLE1',

                    //sales receipt attached, no line items
                    simple2: 'SIMPLE2',

                    //line items, no invoice lookup
                    simple3: 'SIMPLE3',

                    //line items, invoice lookup, no editing of line items
                    advanced1: 'ADVANCED1',

                    //line items, invoice lookup, full edit
                    advanced2: 'ADVANCED2'

                    /*
                    receiptNumber: 'SIMPLE1',
                    salesReceipt: 'SIMPLE2',
                    description: 'SIMPLE3',
                    receiptNumberLookup: 'N/A', //we're not currently using this option...
                    fullEdit: 'ADVANCED1'
                    */
                },
                schedulerInterval: 5000,
                blinkId:{
                    types: ['MRTD'],
                    iOSLicenseKey: 'EIDTPIPZ-WRLWO6Z7-G776BZH3-K5ZVFPZV-Q24OR473-K5ZVFPZV-Q24OR473-K5ZSLKDD',
                    androidLicenseKey: 'EACMWSCD-6NO4DWC2-3P5J7CUI-2TEXL3YP-I6VSMAXR-CMQDUHNM-3FN2655P-O6XQB7N7'
                },
                //This enables the PUT request to update a tourist, in createVoucherService
                //currently Fintrax want it disabled, but I can see if being turned on again at some point in the future...
                enableTouristUpdate:false
            };

        }

})();
