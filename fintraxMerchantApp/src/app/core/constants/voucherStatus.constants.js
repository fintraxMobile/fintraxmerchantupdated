(function () {
    'use strict';

    angular.module('merchantApp.core').constant('voucherStatusConstants',
        [
            //This first status is not from Fintrax's API, we'll use this to
            //indicate an offline voucher is queued for sending to Fintrax.
            { translationKey: 'VOUCHER_STATUS.QUEUED', statusId:-1 },

            { translationKey: 'VOUCHER_STATUS.UNKNOWN', statusId:0 },
            { translationKey: 'VOUCHER_STATUS.PENDING_VOUCHER_RECEIPT', statusId:1 },
            { translationKey: 'VOUCHER_STATUS.REFUNDED_CASH', statusId:2 },
            { translationKey: 'VOUCHER_STATUS.REFUNDED_NON_CASH', statusId:3 },
            { translationKey: 'VOUCHER_STATUS.PENDING_FURTHER_INFORMATION', statusId:4 },
            { translationKey: 'VOUCHER_STATUS.VOIDED', statusId:5 },
            { translationKey: 'VOUCHER_STATUS.ISSUED', statusId:6 },
            { translationKey: 'VOUCHER_STATUS.PROCESSING', statusId:7 },
            { translationKey: 'VOUCHER_STATUS.REFUND_REJECTED', statusId:8 },
            { translationKey: 'VOUCHER_STATUS.STAMPED', statusId:9 }

        ]);
})();