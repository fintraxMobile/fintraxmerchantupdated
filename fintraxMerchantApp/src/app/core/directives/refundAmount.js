(function(){
    angular
        .module('merchantApp.core')
        .directive('refundAmount', refundAmount);



    /* @ngInject */
    function refundAmount() {
        /* jshint validthis: true */
       var directive = {
           scope: {
               refundInfo: '=',
               isEstimated: '='
           },
           link:link,
           templateUrl: 'app/core/templates/refundAmount.html',
           restrict: 'E'
        };

        function link(scope){
            scope.currentDate = function(){
                return new Date();
            };
        }

        return directive;

    }
})();