(function () {
    'use strict';

    angular.module('merchantApp.core', [
        //'ngAnimate', don't need this yet and it causes a random crash.
        'ui.router',
        'ui.select',
        'ngSanitize',
        'pascalprecht.translate',
        'ngCordova',
        'toastr',
        'ngAnimate',
        'angulartics',
        'angulartics.google.analytics'
    ]);
})();
