(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .controller('navigationCtrl', Navigation);

    function Navigation(navigationService, commandService, $scope, $state, refundCalculationService,
                        $translate,layoutTypeConstants, $rootScope, merchantConfig, newVoucherValue, confirmExitVoucherService,utilsService) {
        /*jshint validthis: true */
        var vm = this;
        vm.layoutTypes=layoutTypeConstants;
        vm.buttons = function () { return commandService.getButtons(); };
        vm.title = function () { return navigationService.getTitle(); };
        vm.sectionNumber = function () { return navigationService.getSectionNumber(); };
        vm.hasNext = function () { return navigationService.hasNextState(); };
        vm.hasPrevious = function () { return navigationService.hasPreviousState(); };
        vm.next = function () { navigationService.gotoNextState(); };
        vm.previous = function () { navigationService.gotoPreviousState(); };
        vm.pageClass = function () {
            return navigationService.pageClass();
        };
        vm.offline = function () {
            return navigationService.offline();
        };
        vm.footerInfo = function () {
            return navigationService.footerInfo();
        };

        vm.showVip = function () {
            return commandService.vipSelectVisible();
        };
        vm.changeTotalCssClass = function () {
            return commandService.changeTotalCssClass();
        };
        
        
        vm.showLayoutType = function () {
            return commandService.layoutSelectVisible();
        //	return true;
        };
        
        vm.shopperEnable =function ()
        {
        	return commandService.shopperEnableMenu();
        	//return true;
        	
        }
        vm.showTotalAmount=function()
        {
        	
        	return commandService.showTotalAmount();
        	
        }

        vm.goHome = function () {

            if($state.current && $state.current.data && $state.current.data.isVoucherCreationRoute){

                confirmExitVoucherService.confirmExit().then(function(result){

                    //checking for index 1 for cordova, true for web dev
                    if(result === 1 || result === true){
                        navigationService.endWorkflow();
                        $state.go('navigation.home');
                    }
                });
            } else {
                navigationService.endWorkflow();
                $state.go('navigation.home');
            }

        };
        
        vm.goToShopperDetails = function () {
        	
        	// commandService.addSvgButton('back', back, 'left', true);

        	navigationService.pageValidityChanged(true, undefined, 'navigation.passportdetails');
            navigationService.gotoNextState();
        	
//        	 $state.go('navigation.passportdetails');

        };

        function back() {

            confirmExitVoucherService.confirmExit().then(function(result){

                //checking for index 1 for cordova, true for web dev
                if(result === 1 || result === true){
                    navigationService.endWorkflow();
                    $state.go('navigation.voucherdashboard');
                }
            });

        }
        
        
        vm.calculateRefund = recalculateRefundAmount;
        vm.suggestedLayoutType=selectedLayoutType;
        vm.currentVoucher = newVoucherValue;
        vm.voucherFormulas = [];
        vm.vipRefundAllowed = false;

        $rootScope.$on('RECALCULATE_REFUND', recalculateRefundAmount);

        activate();

        function activate() {

            merchantConfig.getSetting('AddReceiptScenario').then(function(value){
                $rootScope.addReceiptScenario = value;
            });

            merchantConfig.getMerchantConfig().then(function (config) {

                vm.voucherFormulas = config.VoucherFormulas;
                utilsService.activateFlow(config);
                merchantConfig.getSetting('AddReceiptScenario').then(function(value){
                    vm.vipRefundAllowed  = value;

                    //no point in allowing VIP if there are no VIP codes
                    if (vm.voucherFormulas === null || vm.voucherFormulas.length === 0) {
                        vm.vipRefundAllowed = false;
                    }
                });

            });

        }

        function selectedLayoutType(event, args) {
        	
        	 $rootScope.$emit('GENERATE_PDF', event);
        
        }
        function recalculateRefundAmount(event, args) {

            var voucherToCalculate = newVoucherValue;

            //because sometimes we're acting on a temporary voucher (editing voucher) rather than the real thing
            if(args && args.voucher){
                voucherToCalculate = args.voucher;
            }

            merchantConfig.getMerchantConfig().then(function (config) {

                var totalRefund = refundCalculationService.calculateRefund(config, voucherToCalculate);

                console.log(totalRefund);
                navigationService.footerInfo({ refund: totalRefund });
            });
        }
    }
})();
