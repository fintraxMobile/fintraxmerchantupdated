﻿(function () {
    'use strict';

    angular
        .module('merchantApp.core')
        .config(configureRoutes);

    /* @ngInject */
    function configureRoutes($stateProvider, $windowProvider, $analyticsProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation', {
            abstract: true,
            templateUrl: 'app/core/templates/navigation.html',
            controller: 'navigationCtrl as vm'
        });

        //turn off google analytics if none has been configured
        if(!$windowProvider.$get().config.googleAnalytics){
            
            $analyticsProvider.virtualPageviews(false);
        }
    }


})();