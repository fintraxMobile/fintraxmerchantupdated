﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher.signature')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.customersignature', {
            url: '^/customersignature',
            controller: 'customerSignatureController as vm',
            templateUrl: 'app/signature/templates/customerSignature.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.merchantsignature', {
            url: '^/merchantsignature',
            controller: 'customerSignatureController as vm',
            params: { isMerchant: true },
            templateUrl: 'app/signature/templates/customerSignature.html',
            data: {
                isVoucherCreationRoute:true
            }
        });

    }

})();
