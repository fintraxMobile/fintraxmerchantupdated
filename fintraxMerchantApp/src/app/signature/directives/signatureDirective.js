﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher')
        .directive('muSignature', function () {

            //Useful tutorial that includes windows devices:
            //https://software.intel.com/en-us/html5/articles/touch-drawing-app-using-html5-canvas

            var context, colour = '#000', x = 0, y = 0, theScope;
            var absX = 0, absY = 180; //compare with CSS file

            // prototype to	start drawing on touch using canvas moveTo and lineTo
            $.fn.drawTouch = function () {
                var start = function (e) {
                    e = e.originalEvent;
                    context.beginPath();
                    x = e.changedTouches[0].pageX - absX;
                    y = e.changedTouches[0].pageY - absY;
                    context.moveTo(x, y);
                    context.lineTo(x, y + 1);
                    context.stroke();
                    setValid();
                };

                var move = function (e) {
                    e.preventDefault();
                    e = e.originalEvent;
                    x = e.changedTouches[0].pageX - absX;
                    y = e.changedTouches[0].pageY - absY;
                    context.lineTo(x, y);
                    context.stroke();
                };

                $(this).on('touchstart', start);
                $(this).on('touchmove', move);
            };


            $.fn.drawPointer = function () {
                var start = function (e) {
                    e = e.originalEvent;
                    context.beginPath();
                    x = e.pageX - absX;
                    y = e.pageY - absY;
                    context.moveTo(x, y);
                    context.lineTo(x, y + 1);
                    context.stroke();
                    setValid();
                };

                var move = function (e) {
                    e.preventDefault();
                    e = e.originalEvent;
                    x = e.pageX - absX;
                    y = e.pageY - absY;
                    context.lineTo(x, y);
                    context.stroke();
                };

                $(this).on('MSPointerDown', start);
                $(this).on('MSPointerMove', move);
            };


            $.fn.drawMouse = function () {
                var clicked = 0;
                var start = function (e) {
                    clicked = 1;
                    context.beginPath();
                    x = e.pageX - absX;
                    y = e.pageY - absY;
                    context.moveTo(x, y);
                    context.lineTo(x, y+1);
                    setValid();
                };
                var move = function (e) {
                    if (clicked) {
                        x = e.pageX - absX;
                        y = e.pageY - absY;
                        context.lineTo(x, y);
                        context.stroke();
                    }
                };
                var stop = function () {
                    clicked = 0;
                };
                $(this).on('mousedown', start);
                $(this).on('mousemove', move);
                $(window).on('mouseup', stop);
            };

            $.fn.clearCanvas = function(){
                context.clearRect(0, 0, this.width(), this.height());
            };

            //Use this to set the isValid attribute for this directive, so we know to enable the confirm button.
            function setValid() {
                theScope.isValid = true;
                if (theScope.$root.$$phase !== '$apply' && theScope.$root.$$phase !== '$digest') {
                    theScope.$apply();
                }
            }

            function resizeImage(url, width, height, callback) {
                var sourceImage = new Image();

                sourceImage.onload = function() {
                    // Create a canvas with the desired dimensions
                    var canvas = document.createElement('canvas');
                    canvas.width = width;
                    canvas.height = height;

                    // Scale and draw the source image to the canvas
                    canvas.getContext('2d').drawImage(sourceImage, 0, 0, width, height);

                    // Convert the canvas to a data URL in PNG format
                    callback(canvas.toDataURL());
                };

                sourceImage.src = url;
            }

            //used to redraw old signature onto canvas
            function drawImage(imageDataUrl){
                var img = new Image();
                img.src = imageDataUrl;
                context.drawImage(img, 0, 0);
            }


            return {
                restrict: 'E',
                scope: { toDataUrl: '=', isValid: '=', existingSignature: '=' }, //toDataUrl exposes a function
                link: function (scope, element) {
                    theScope = scope;
                    scope.isValid = false;

                    //Set canvas to use appropriate width and height (5/6ths of the screen width).
                    var width = window.innerWidth * 5 / 6;
                    var height = window.innerHeight - 320;
                    absX = (window.innerWidth - width) / 2;
                    var canvas = $('<canvas width="' + width + '" height="' + height + '" style="margin-left:' + absX + 'px;"></canvas>');
                    element.empty().append(canvas);

                    //add a spacer after the absolutely positioned canvas, so the page flow still works as expected.
                    element.after($('<div style="height:' + (height + 10) + 'px"></div>'));

                    //prevent scrolling on windows devices (swallows touch events).
                    $('body').addClass('noscroll');
                    scope.$on('$destroy', function () {
                        $('body').removeClass('noscroll');
                    });

                    if (canvas[0].getContext) {
                        context = canvas[0].getContext('2d');
                        context.rect(0, 0, width, height);
                        context.fillStyle = '#FFF';
                        context.fill();

                        context.lineWidth = 4;
                        context.lineJoin = 'round';
                        context.lineCap = 'round';
                        context.strokeStyle = colour;
                        // setup to trigger drawing on mouse or touch
                        canvas.drawTouch();
                        canvas.drawPointer();
                        canvas.drawMouse();
                    }

                    if(scope.existingSignature){
                        drawImage(scope.existingSignature);
                        scope.isValid = true;
                    }

                    //expose a method so the parent controller can extract the base 64 encoded date.
                    scope.toDataUrl = function (callback) {
                        console.log('toDataUrl!');

                        var currentImage = canvas[0].toDataURL('image/png');

                        //currentImage is of an unknown size, due to it being a percentage width of the browser
                        var resized = resizeImage(currentImage, 360, 120, function(resized){

                            callback({
                                original: currentImage,
                                resized:resized
                            });

                        });

                        //We could also have used ('image/jpeg', 0.7), but PNG with it's run length encoding wins over JPG
                        //for a signature. (It's approx 10K smaller on a high res laptop).
                    };

                }
            };
        });
})();
