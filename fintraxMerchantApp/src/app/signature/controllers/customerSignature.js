﻿(function () {
    'use strict';
    angular
        .module('merchantApp.createVoucher.signature')
        .controller('customerSignatureController', Controller);

    function Controller(navigationService, commandService, $scope, newVoucherValue, translatedToastr,
                        merchantConfig, $state, $stateParams, createVoucherService, createVoucherOfflineService,
                        $log, connectivityStatusService, voucherErrorHandler) {

        /* jshint validthis: true */
        var vm = this;
        vm.isMerchant = $stateParams.isMerchant;
        vm.existingSignature = '';

        activate();

        function activate() {

            var title = '';

            if($stateParams.isMerchant){
                title = 'SIGNATURE.MERCHANT_CONFIRMATION';

                if(newVoucherValue._originalMerchantSignature){
                    vm.existingSignature = newVoucherValue._originalMerchantSignature;
                }

            } else {
                title = 'SIGNATURE.SHOPPER_CONFIRMATION';

                if(newVoucherValue._originalCustomerSignature){
                    vm.existingSignature = newVoucherValue._originalCustomerSignature;
                }
            }

            navigationService.setTitle(title, 4);
            navigationService.pageClass('signature');

            vm.voucher = newVoucherValue;


            commandService.addSvgButton('back', back, 'left', true);

            commandService.addButton('COMMANDS.CONFIRM', confirm, 'right', true, function () {
                return !vm.isValid;
            });

            commandService.addButton('SIGNATURE.CLEAR_SIGNATURE', function(){
                $('mu-signature').clearCanvas();
            },'right');


            $scope.$on('$destroy', commandService.restoreDefaultButtons);
        }

        function back(){

            vm.getDataUrlFromDirective(function(base64ImageData){

                if ($stateParams.isMerchant) {

                    newVoucherValue.MerchantSignature = base64ImageData.resized;
                    newVoucherValue._originalMerchantSignature = base64ImageData.original;
                } else {

                    newVoucherValue.CustomerSignature = base64ImageData.resized;
                    newVoucherValue._originalCustomerSignature = base64ImageData.original;
                }

                navigationService.gotoPreviousState();

            });

        }

        function confirm() {

            vm.getDataUrlFromDirective(function(base64ImageData){

                if ($stateParams.isMerchant) {

                    newVoucherValue.MerchantSignature = base64ImageData.resized;
                    newVoucherValue._originalMerchantSignature = base64ImageData.original;

                    attemptVoucherCreation().then(createVoucherSuccess).catch(createVoucherFailure);

                } else {

                    newVoucherValue.CustomerSignature = base64ImageData.resized;
                    newVoucherValue._originalCustomerSignature = base64ImageData.original;

                    merchantConfig.getSetting('EnableMerchantSignature').then(function (merchantSignatureEnabled) {

                        if (merchantSignatureEnabled) {

                            navigationService.pageValidityChanged(true, undefined, 'navigation.merchantsignature');
                            navigationService.gotoNextState();
                        } else {

                            attemptVoucherCreation().then(createVoucherSuccess).catch(createVoucherFailure);
                        }
                    });
                }
            });

        }

        function createVoucherSuccess(result) {
            $log.debug('Create voucher success!');

            if(result.online){

                translatedToastr.success('TOAST.CREATE_VOUCHER_SUCCESS');
            } else {

                //if we're not online, it must have been queued
                translatedToastr.warning('TOAST.VOUCHER_QUEUED', null, { preventDuplicates:false } );
            }

            //ending the workflow here prevents weird things happening after you change tabs
            navigationService.endWorkflow();
            $state.go('navigation.printvoucher');
        }

        function createVoucherFailure(result) {

            voucherErrorHandler.attemptToParseError(result);
        }

        function attemptVoucherCreation(){

            //means we can manipulate the voucher value in the createVoucher service without effecting the original
            var voucherClone = angular.copy(newVoucherValue);

            voucherClone.CustomerSignature = '';
            voucherClone.MerchantSignature = '';

            return connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(online){

                if(online){
                    return createVoucherService.createVoucherOnline(voucherClone).then(function(result){

                        result.online = true;
                        return result;
                    });
                } else {

                    //otherwise the previous successful online voucher creation result will be printed
                    createVoucherService.clearVoucherCreationResult();

                    //actually want to act on the newVoucherValue object for offline vouchers
                    return createVoucherOfflineService.createVoucherOffline(newVoucherValue);
                }

            });

        }

    }
})();
