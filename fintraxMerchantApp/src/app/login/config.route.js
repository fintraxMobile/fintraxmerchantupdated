(function () {
    'use strict';

    angular
        .module('merchantApp.login')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('login', {
            url: '^/login',
            controller: 'LoginController as login',
            templateUrl: 'app/login/login.html'
        });
    }

})();
