(function () {

    angular
        .module('merchantApp.login')
        .controller('LoginController', Login);


    /* @ngInject */
    function Login(authentication, merchantConfig, $window, navigationService,
        translatedToastr, $log, $state, fintraxApi,$scope,merchantConfig,merchantAppConfig,$window,store) {
        /* jshint validthis: true */
        var vm = this;

        vm.login = login;
        vm.reset = reset;
        
        vm.isAuthenticated = false;
        
        $scope.settings={};

        activate();

        function login(user) {
          //clear it for ever login.
           localStorage.ageDateOfBirth="";
            authentication.login(user, loginSuccess, loginFail);
        }
        
        function reset() {
            //clear it for ever login.
        	// $state.go('navigation.reset');
        	 $state.transitionTo('reset');
        	//authentication.reset();
        	// window.location=location.origin+"/#/reset";
          }

        function loginSuccess(profile) {
            vm.profile = profile;
            vm.isAuthenticated = authentication.isAuthenticated();

            $log.log(profile);

            //load the merchant config for the first time
            merchantConfig.refreshMerchantConfig(true).then(function (config) {

                //download the countries list to ensure it's cached for offline use
                
                
                   $scope.merchantConfig = config;

            
                  config.Settings.forEach(function(setting){

                    switch(setting.Name){

                        case 'VipRefundAllowed':
                            $scope.settings.vipRefundAllowed =  setting.Value;
                            break;

                        case 'AddReceiptScenario':
                            $scope.settings.addReceiptScenario =  setting.Value;
                            break;

                        case 'EnableTouristSignature':
                            $scope.settings.enableTouristSignature =  setting.Value;
                            break;

                        case 'EnableMerchantSignature':
                            $scope.settings.enableMerchantSignature =  setting.Value;
                            break;
                        case 'EnableOrientation':
                            $scope.settings.EnableOrientation =  setting.Value;
                            store.set('EnableOrientation',setting.Value);
                            break;
                        case 'EnableQuickPrint':
                        	
                            $scope.settings.EnableQuickPrint =  setting.Value;
                            $window.localStorage.setItem('EnableQuickPrint',  setting.Value=='True'?true:false);
                            store.set('EnableQuickPrint',setting.Value);
                            break;
                            
                        case 'EnableCtrip':
                        	
                            $scope.settings.EnableCtrip =  setting.Value;
                            $window.localStorage.setItem('EnableCtrip',setting.Value=='True'?true:false);
                            store.set('EnableCtrip',setting.Value);
                            break;
                            
                            
                            
                            
                            
                            
                    }
                });
                
                activateFlow();                
                
                

            }, function (err) {
                translatedToastr.error('LOGIN.UNABLE_TO_DOWNLOAD_MERCHANT_CONFIG', null, { target: 'body' });
                console.log(err);
            });

            translatedToastr.success('LOGIN.SUCCESS', null, { target: 'body' });

        }

        function loginFail(Error) {

            var message = 'LOGIN.UNABLE_TO_LOG_IN';

            if (Error.code === 'invalid_user_password') {
                message = 'LOGIN.INVALID_USER_PASSWORD';
            }

            translatedToastr.error(message, null, { target: 'body' });
        }

        function activate() {
            navigationService.pageClass('login');

            vm.profile = authentication.getProfile();
            vm.isAuthenticated = authentication.isAuthenticated();
        }
        
        function activateFlow() {
        	
        	var $window=angular.isUndefined($window)?window:$window; 		    	
        	if($scope.settings.EnableOrientation=='True')
        		{
              
                if($window.screen)
                {

                	 $window.localStorage.setItem('FlexibleOrientation', true);
                	 
                   	 $window.screen.orientation.unlock();
             
                	           
                }
                
              	
        		}
        	
        	else
        		{
        		  if($window.screen)
                  {
        			$window.localStorage.setItem('FlexibleOrientation', false);
        			$window.screen.orientation.lock('landscape');
                  
                  }
        		}
        	
        	
        	fintraxApi.getCountries().then(function(){

                if($window.localStorage.getItem('hideWalkthrough') !== 'true'){
                          
                  //  $scope.settings.EnableOrientation=='True'?$state.go('navigation.voucherdashboardflexible'):$state.go('navigation.walkthrough')
                
                	$state.go('navigation.walkthrough');  		
                    		
                    		
                } else {
               
                	
                    $scope.settings.EnableOrientation=='True'?$state.go('navigation.voucherdashboardflexible'):$state.go('navigation.home')
                    
                    
                	
                }
            });
        	
        	
        }

    }

})();
