(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher.receiptList')
        .config(configureRoutes);

    function configureRoutes($stateProvider) {

        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.receiptList', {
            url: '^/receiptlist',
            controller: 'ReceiptListController as vm',
            params: { useAdvancedReceiptValue: null },
            templateUrl: 'app/receiptlist/templates/receiptlist.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.advancedReceipt', {
            url: '^/advancedReceipt',
            controller: 'advancedReceiptController as vm',
            params: { previousState: null, sectionNumber: null, simple: null },
            templateUrl: 'app/receiptlist/templates/advancedReceipt.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.simpleReceipt', {
            url: '^/simpleReceipt',
            controller: 'advancedReceiptController as vm',
            params: { previousState: null, sectionNumber: null, simple: null },
            templateUrl: 'app/receiptlist/templates/simpleReceipt.html',
            data: {
                isVoucherCreationRoute:true
            }
        });

    }

})();
