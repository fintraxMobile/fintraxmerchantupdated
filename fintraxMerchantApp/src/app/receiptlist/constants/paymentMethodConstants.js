﻿(function () {
    'use strict';

    angular.module('merchantApp.createVoucher.receiptList').constant('paymentMethodConstants',
    [
        { key: 'cash', translationKey: 'DROPDOWNS.PAYMENT_METHOD.CASH', id: 1 },
        { key: 'cheque', translationKey: 'DROPDOWNS.PAYMENT_METHOD.CHEQUE', id: 3 },
        { key: 'card', translationKey: 'DROPDOWNS.PAYMENT_METHOD.CREDIT_CARD', id: 4 },
        { key: 'other', translationKey: 'DROPDOWNS.PAYMENT_METHOD.OTHERS', id: 13 }
    ]);
})();