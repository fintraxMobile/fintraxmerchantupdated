﻿(function () {
    'use strict';

    angular.module('merchantApp.createVoucher.receiptList').value('advancedReceiptValue', { receipt: {} });
})();
