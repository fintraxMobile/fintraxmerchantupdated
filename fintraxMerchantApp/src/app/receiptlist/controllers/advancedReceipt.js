(function () {
    angular
        .module('merchantApp.createVoucher.receiptList')
        .controller('advancedReceiptController', advancedReceiptController);

    /* @ngInject */
    function advancedReceiptController(
        $scope,
        navigationService,
        commandService,
        advancedReceiptValue,
        $state,
        $stateParams,
        utilsService,
        merchantConfig,
        merchantAppConfig,
        manateeBarcode,
        translatedToastr,
        $log,
        fintraxApi,
        paymentMethodConstants,
        connectivityStatusService,
        $window,
        $timeout
    ) {
        /* jshint validthis: true */
        var vm = this;
        vm.receipt = angular.copy(advancedReceiptValue.receipt);
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.products = [];
        vm.paymentMethods = paymentMethodConstants;
        vm.addReceiptScenario = $scope.addReceiptScenario;
        vm.paymentMethodKeys = {};

        //note this is changed in the activate function
        vm.isPartiallyReadOnly = false;
        vm.descriptionRequired = vm.addReceiptScenario === merchantAppConfig.addReceiptScenarios.simple3;

        vm.addReceiptScenarios = merchantAppConfig.addReceiptScenarios;
        vm.scanReceipt = scanReceipt;
        vm.scanAndLookupReceipt = scanAndLookupReceipt;
        vm.lookupReceipt = lookupReceipt;
        vm.receiptHasSomeValues = receiptHasSomeValues;
        vm.isUkmerchant=utilsService.isUkmerchant();
        vm.acceptOnlyNumbers=accepOnlyNumbers;
        vm.today = function(){
            var today = new Date();
            return new Date(today.getFullYear(), today.getMonth(), today.getDate());
        };

        var maxDaysInThePastForAReceipt = 0;
        
        
      

        activate();

        function activate() {

            connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(){

                setScreenConfig();
            });

            setScreenConfig();

            //use this to do nice if statements in the template
            paymentMethodConstants.forEach(function(method){
                vm.paymentMethodKeys[method.key] = method.id;
            });

            setupBasicReceiptFieldsIfNeeded();

            navigationService.setTitle('RECEIPTS.ADD_RECEIPT', $stateParams.sectionNumber);
            navigationService.pageClass('add-receipt');
            commandService.removeAllButtons();
            commandService.addButton('COMMANDS.CANCEL', back, 'left', false, undefined, commandService.BUTTON_TYPE.tertiary);
            commandService.addButton('COMMANDS.CONFIRM', confirmAndClose, 'right', false, isDataInvalid);

            $scope.$on('$destroy', function(){

                commandService.restoreDefaultButtons();
            });

            //Get relevant data out of merchantConfig

            merchantConfig.getMerchantConfig().then(function (config) {

                maxDaysInThePastForAReceipt = config.Store.InvoiceEligibleNumberOfDays;
                //Fix bug where config is slow to load and we see a false validation error. We should ideally move away from a
                //promise-heavy API for accessing config data. EG, keep config in local storage, which can be accessed synchronously.
                evaluateDateTooFarInPast(vm.receipt.InvoiceDateTime);

                vm.vatRates = utilsService.getUniqueVATCodes(config);
                vm.products = config.Products;
                vm.verifyThresholdByDay = config.Store.VerifyThresholdByDay;
                vm.minimumAmount = config.Store.MinimumAmount;

                if ($stateParams.simple) {
                    vm.vatRates.forEach(function (vatRate) {

                        var exists = vm.receipt.LineItems.some(function (e) {
                            return e.VatRate === vatRate;
                        });

                        if (!exists) {
                            vm.receipt.LineItems.push({ VatRate: vatRate });
                        }
                    });
                }

                utilsService.hideOrRequireFields(config, $scope);
            });

            //Add a check to ensure the receipt date is within InvoiceEligibleNumberOfDays
            $scope.$watch('vm.receipt.InvoiceDateTime', function(){

                evaluateDateTooFarInPast(vm.receipt.InvoiceDateTime);
            });

            commandService.vipSelectVisible(true);

        }

        function setScreenConfig(){

            //if you're an advanced1 user, but offline, you should have full edit facilities.
            if(navigationService.offline() && vm.addReceiptScenario === merchantAppConfig.addReceiptScenarios.advanced1){
                vm.addReceiptScenario = merchantAppConfig.addReceiptScenarios.advanced2;
                vm.isPartiallyReadOnly = false;
            } else {
                vm.isPartiallyReadOnly = vm.addReceiptScenario === merchantAppConfig.addReceiptScenarios.advanced1;
            }
        }
        
        function accepOnlyNumbers(value,changingObject)
        {
        	
        	utilsService.acceptOnlyNumbers(value,changingObject,"UnitPrice");
                 
        	
        }

        function receiptHasSomeValues(){
            return vm.receipt.LineItems.some(function(item){ return item.UnitPrice > 0; });
        }

        function evaluateDateTooFarInPast(newval) {
            if (angular.isDefined(newval)) {
                var daysDifference = Math.floor(((new Date()) - newval) / (24 * 60 * 60 * 1000));
                vm.dateTooFarInPast = daysDifference > maxDaysInThePastForAReceipt;
            }
        }

        function evaluateOutOfRangeForDate(){
            if(vm.receipt.InvoiceDateTime < vm.today()){

                if(vm.receipt.NetAmount <  vm.minimumAmount){
                    return true;
                }
            }
            return false;
        }

        function setupBasicReceiptFieldsIfNeeded() {
            if (!vm.receipt.pricing) {

                merchantConfig.getSetting('UseUnitPriceAndQuantity').then(function (useUnitPriceAndQuantity) {

                    if($stateParams.simple) {

                        vm.receipt.pricing = 'unit';
                    } else {

                        vm.receipt.pricing = useUnitPriceAndQuantity ? 'unit' : 'total';
                    }
                });
            }

            if (!vm.receipt.LineItems) {
                vm.receipt.LineItems = [];
            }
            if (!vm.receipt.InvoiceDateTime) {
                vm.receipt.InvoiceDateTime = new Date();
            }

            if (!vm.receipt.PaymentMethodDetails) {
                vm.receipt.PaymentMethodDetails = [];
                vm.receipt.PaymentMethodDetails[0] = {};
            }

            if (!vm.receipt.PaymentMethodDetails[0].PaymentMethodOtherName) {
                vm.receipt.PaymentMethodDetails[0].PaymentMethodOtherName = '';
            }

            if (!vm.receipt.PaymentMethodDetails[0].Amount) {
                vm.receipt.PaymentMethodDetails[0].Amount = 0;
            }
        }

        function back() {

            $state.go($stateParams.previousState);
        }

        function addItem() {
            vm.receipt.LineItems.push({});

            var mainElement = $('main');

            //scroll the item into view
            $timeout(function(){
                mainElement.scrollTop(mainElement[0].scrollHeight);

            }, 10);

        }

        function removeItem(index) {
            vm.receipt.LineItems.splice(index, 1);
        }

        function confirmAndClose() {

            if(addReceiptToVoucher()) {
                $state.go($stateParams.previousState, { useAdvancedReceiptValue: true });
            }
        }

        function addReceiptToVoucher() {

            computeNetGrossVatAndArticleCode();
            removeZeroQuantityLineItems();

            if(vm.verifyThresholdByDay && evaluateOutOfRangeForDate()){

                translatedToastr.error('TOAST.BELOW_DAY_THRESHOLD');
                return false;
            } else {
                var index = advancedReceiptValue.receipts.indexOf(advancedReceiptValue.receipt);

                if (index === -1) {

                    var matchingReceipts = advancedReceiptValue.receipts.filter(function(invoice){

                        return invoice.Number === vm.receipt.Number;
                    });

                    if(matchingReceipts.length === 0 || vm.addReceiptScenario === merchantAppConfig.addReceiptScenarios.simple2){

                        advancedReceiptValue.receipts.push(vm.receipt);

                    } else {

                    	//accepting multi empty barcodes/receipt number especially for UK merchants
                    	
                    	if(utilsService.isUkmerchant() && !vm.receipt.Barcode)
                    		{
                    		
                    		 advancedReceiptValue.receipts.push(vm.receipt);
                    		 return true;
                    		
                    		}
                    	
                        translatedToastr.error('RECEIPTS.INVOICE_ALREADY_ATTACHED');
                        return false;
                    }


                } else {
                    advancedReceiptValue.receipts[index] = vm.receipt;
                }

                return true;
            }

        }


        function computeNetGrossVatAndArticleCode() {

            //Set the article code according to the selected item type. Also build up NetAmount totals.
            vm.receipt.GrossAmount = 0;
            vm.receipt.NetAmount = 0;
            vm.receipt.VatAmount = 0;

            vm.receipt.LineItems.forEach(function (li) {

                if(li.UnitPrice > 0 || li.GrossAmount > 0){

                    //only true for advanced2 & simple3 receipts
                    if (li._itemtype && vm.addReceiptScenario !== merchantAppConfig.addReceiptScenarios.advanced1) {
                        li.ProductCode = li._itemtype.ProductCode;
                        li.ProductDescription = li._itemtype.ProductDescription;

                        li.VatRate = li._itemtype.VatRate;
                    }

                    var vatDecimal = li.VatRate / 100;

                    //not setting quantity for simple receipt, so need to default to one
                    if(!li.Quantity){
                        li.Quantity = 1;
                    }

                    if (vm.receipt.pricing !== 'total') {
                        li.GrossAmount = li.UnitPrice * li.Quantity;
                    }

                    li.NetAmount = li.GrossAmount / (1 + vatDecimal);
                    li.VatAmount = li.GrossAmount - li.NetAmount;
                    vm.receipt.GrossAmount += li.GrossAmount; //total gross amount
                    vm.receipt.NetAmount += li.NetAmount; //Total net amount
                    vm.receipt.VatAmount += li.VatAmount;

                } else {
                    li.Quantity = 0;
                }

            });

            vm.receipt.PaymentMethodDetails[0].Amount = vm.receipt.GrossAmount;
            vm.receipt.Barcode = vm.receipt.Number;
        }

        function removeZeroQuantityLineItems(){
            //remove all items with zero quantity
            while(vm.receipt.LineItems.some(function(item){ return item.Quantity === 0; })){

                var firstZeroQuantityItem = vm.receipt.LineItems.map(function(li){
                    return li.Quantity;
                }).indexOf(0);

                vm.receipt.LineItems.splice(firstZeroQuantityItem, 1);
            }
        }


        function isDataInvalid() {
            if ($stateParams.simple) {
                return $scope.myForm.$invalid || vm.dateTooFarInPast || vm.outOfRangeForDate;
            }

            //Returns true if there are no items, the receipt number / date is missing or if an item is incomplete.
            return (vm.receipt.LineItems.length === 0 || $scope.myForm.$invalid || $scope.itemsForm.$invalid || vm.dateTooFarInPast || vm.outOfRangeForDate);
        }

        //////////////////////////////////////////////////////////////////////
        // SCANNING METHODS
        //////////////////////////////////////////////////////////////////////
        function scanReceipt() {

        	if(utilsService.isOnWindowsPlatform() || device.platform=="Android" || device.platform=="amazon-fireos")
        		{
              cordova.plugins.barcodeScanner.scan(stoppedScanning,
                   function(){
                     stoppedScanning(false);
                   }
                );
            } else {
                manateeBarcode.startScanning(stoppedScanning);
            }
        }

        function scanAndLookupReceipt() {

        	if(utilsService.isOnWindowsPlatform() || device.platform=="Android" || device.platform=="amazon-fireos")
        		{
              cordova.plugins.barcodeScanner.scan(stoppedScanningAndLookup,
                   function(){
                     stoppedScanning(false);
                   }
                );
            } else {
                manateeBarcode.startScanning(stoppedScanningAndLookup);
            }
        }

        function stoppedScanning(result) {

            if(result === false){

                translatedToastr.warning('TOAST.UNABLE_TO_OPEN_SCANNER', 'TOAST.ERROR');

            } else if(result && result.type !== 'Cancel' && !result.cancelled){
                if(result.code || result.text){

                    vm.receipt.Number = result.code || result.text;

                } else {
                    $log.error('Unable to open barcode scanner');
                    translatedToastr.warning('TOAST.RECEIPT_SCAN_ERROR', 'TOAST.ERROR');
                }
            }

        }

        function stoppedScanningAndLookup(result) {

            if(result === false){

                translatedToastr.warning('TOAST.UNABLE_TO_OPEN_SCANNER', 'TOAST.ERROR');

            } else if(result && result.type !== 'Cancel' && !result.cancelled){
              if(result.code || result.text){

                    vm.receipt.Number = result.code || result.text;
                    lookupReceipt();

                } else {
                    $log.error('Unable to open barcode scanner');
                    translatedToastr.warning('TOAST.RECEIPT_SCAN_ERROR', 'TOAST.ERROR');
                }
            }
        }

        function lookupReceipt() {

            if (angular.isDefined(vm.receipt.Number) && vm.receipt.Number.length > 0) {

                fintraxApi.getInvoice(vm.receipt.Number).then(invoiceApiLookupSuccess, invoiceApiLookupFail);
            }
        }

        function invoiceApiLookupSuccess(response) {
            //convert the date from a string to an actual date
            response.InvoiceDateTime = new Date(response.InvoiceDateTime);

            if(response.IsAttachedToVoucher){

                translatedToastr.error('RECEIPTS.INVOICE_ALREADY_ATTACHED');

            } else {

                vm.receipt = response;
                vm.receipt._wasFetchedFromAPI = true;
                vm.receipt.pricing = 'total';
                translatedToastr.success('TOAST.RECEIPT_SCAN_OK');
                confirmAndClose();
            }

        }

        function invoiceApiLookupFail(response) {
            if (response.status === 404) {
                translatedToastr.warning('TOAST.RECEIPT_NUMBER_NOT_FOUND', 'TOAST.ERROR');
            } else {
                translatedToastr.warning('TOAST.UNABLE_TO_LOOKUP_RECEIPT', 'TOAST.ERROR');
            }
        }
    }
})();
