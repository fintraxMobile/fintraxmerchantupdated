(function () {

    angular
        .module('merchantApp.createVoucher.receiptList')
        .controller('ReceiptListController', ReceiptList);

    /* @ngInject */
    function ReceiptList(navigationService, commandService, newVoucherValue, advancedReceiptValue,
                         $stateParams, $state, $scope, $rootScope, connectivityStatusService, merchantAppConfig, translatedToastr,printService,utilsService,createVoucherService,voucherErrorHandler) {
        /* jshint validthis: true */
        var vm = this;
        vm.recalculateRefund = recalculateRefund;
        vm.invoices = [];
       
        activate();

        function activate(){

            connectivityStatusService.checkOnlineIfOnlineSetNav();

            //When coming back into this screen from the advanced receipts screen, we should prefer the advancedReceiptValue collection.
            if (!$stateParams.useAdvancedReceiptValue && angular.isDefined(newVoucherValue.Invoices) === false) {
                newVoucherValue.Invoices = [];
            }

            vm.invoices = $stateParams.useAdvancedReceiptValue ? advancedReceiptValue.receipts : newVoucherValue.Invoices;

            navigationService.setTitle('RECEIPTS.PURCHASE_DETAILS', 2);
            navigationService.pageClass('receiptList');

            commandService.addButton('COMMANDS.NEXT', goToNextScreen, 'right', true, isDataInvalid);
            //for german stuffs
            if(window.localStorage.getItem('EnableQuickPrint'))
           { 		
            	commandService.addButton('COMMANDS.PRINT', print, 'right');
           }
            commandService.changeTotalCssClass(false);
            commandService.vipSelectVisible(true);
            
            commandService.showTotalAmount(true);

            if( !angular.isDefined( newVoucherValue._alreadyEnteredList ) ){

                newVoucherValue._alreadyEnteredList = true;
                createEditReceipt();

            }
        }

        

        function print() {
        	
        	
        	attemptVoucherCreation().then(createVoucherSuccess).catch(createVoucherFailure);
        	
        	

           
        }
        
       
        
        
        
        function createVoucherSuccess(result) {
          //  $log.debug('Create voucher success!');

            if(result.online){

                translatedToastr.success('TOAST.CREATE_VOUCHER_SUCCESS');
            } else {

                //if we're not online, it must have been queued
                translatedToastr.warning('TOAST.VOUCHER_QUEUED', null, { preventDuplicates:false } );
            }

            //ending the workflow here prevents weird things happening after you change tabs
            navigationService.endWorkflow();
            $state.go('navigation.printvoucher');
        }

        function createVoucherFailure(result) {

            voucherErrorHandler.attemptToParseError(result);
        }
        function updateRefundOption(newVoucherValue)
        {
        	//Default unknown refund option 
        	newVoucherValue.RefundOption={};
        	newVoucherValue.RefundOption.RefundOptionCode="0000";
        	
        	return newVoucherValue;
        }
        
        
        function attemptVoucherCreation(){

            //means we can manipulate the voucher value in the createVoucher service without effecting the original
            
        	newVoucherValue=updateRefundOption(newVoucherValue)
        	
        	var voucherClone = angular.copy(newVoucherValue);

            voucherClone.CustomerSignature = '';
            voucherClone.MerchantSignature = '';

            return connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(online){

                if(online){
                    return createVoucherService.createVoucherOnline(voucherClone).then(function(result){

                        result.online = true;
                        return result;
                    });
                } else {

                    //otherwise the previous successful online voucher creation result will be printed
                    createVoucherService.clearVoucherCreationResult();

                    //actually want to act on the newVoucherValue object for offline vouchers
                    return createVoucherOfflineService.createVoucherOffline(newVoucherValue);
                }

            });

        }
        
        function createEditReceipt() {
            if ($scope.addReceiptScenario === merchantAppConfig.addReceiptScenarios.advanced1 ||
                $scope.addReceiptScenario === merchantAppConfig.addReceiptScenarios.advanced2 ||
                $scope.addReceiptScenario === merchantAppConfig.addReceiptScenarios.simple3) {
                $state.transitionTo('navigation.advancedReceipt', {
                    previousState: $state.current.name,
                    sectionNumber: 2, simple: false
                });
            } else {
                $state.transitionTo('navigation.simpleReceipt', {
                    previousState: $state.current.name,
                    sectionNumber: 2, simple: true
                });
            }
        }

        function isDataInvalid(){

            return !vm.isValid || newVoucherValue.RefundAmount <= 0;
        }

        function goToNextScreen(){

            //change page validity, and got to next state, to ensure back state works correctly
            navigationService.pageValidityChanged(true, undefined, 'navigation.refundtypes');
            navigationService.gotoNextState();
        }

        function recalculateRefund(){

            $rootScope.$broadcast('RECALCULATE_REFUND', {});
        }
    }
})();
