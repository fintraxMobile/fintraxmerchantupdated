(function () {
    'use strict';

    angular.module('merchantApp', [

        'merchantApp.core',
        'merchantApp.data',
        'merchantApp.createVoucher',
        'merchantApp.tourist',
        'merchantApp.createVoucher.receiptList',
        'merchantApp.createVoucher.refund',
        'merchantApp.createVoucher.signature',
        'merchantApp.createVoucher.print',
        'merchantApp.login',
        'merchantApp.reset',
        'merchantApp.dashboards',
        'merchantApp.reports',
        'merchantApp.calculator',
        'merchantApp.education'

    ]).config(appConfig).run(appRun);

    function appConfig($compileProvider){

      //makes ng-src work with windows
      $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|local|data|ms-appx|file):/);
    }

    function appRun($timeout,$state, $rootScope, constantsTranslator, offlineQueue, schedulerService, connectivityStatusService, translatedToastr,
        merchantAppConfig, notificationService,navigationService, $window, $cordovaDevice, $cordovaAppVersion) {

        $rootScope.locale = (window.navigator.userLanguage || window.navigator.language).toLowerCase();

        $state.transitionTo('navigation.home');

        //Remove 300ms delay between touch and click events
        FastClick.attach(document.body);
        var connectionPoll;
        //generic connectivity checking for all screens fixed by kavi as per FRANCE issue
        connectivityCheck();

        $rootScope.$on('$translateChangeSuccess', constantsTranslator.translate);
        
        $rootScope.$on('$routeChangeStart', onNavigate);
        $rootScope.$on('$locationChangeStart', onNavigate);
        $rootScope.$on('$routeChangeSuccess', onNavigate);
        
        

        //$translateChangeSuccess doesn't seem to be firing on first load now we're using a default language
        //see https://github.com/angular-translate/angular-translate/issues/734


        constantsTranslator.translate();


        offlineQueue.init().then(function () {
            $rootScope.isPaused = false;
            localStorage.currentLocation=undefined;
            notificationService.scheduleNotification(offlineQueue.queueLength());
            schedulerService.start(merchantAppConfig.schedulerInterval);

            //Listen for the app going to sleep and wakeing up. (NB - we ensure the offlineQueue.init() has been called).
            document.addEventListener('deviceready', function () {
                document.addEventListener('pause', onPause, false);
                document.addEventListener('resume', onResume, false);

                //Disable the back button on Android (or use it to fully exit the app if you're on the home screen)
                //This event listener must be hooked up  within the deviceready event.
                document.addEventListener('backbutton', function (e) {
                    if ($state.current.name === 'navigation.home') {
                        navigator.app.exitApp();
                    } else {
                        e.preventDefault();
                    }
                }, false);



                if($window.cordova){

                    if($window.hockeyapp && window.config.hockeyApp){

                        var hockeyAppId = '';
                        var platform = $cordovaDevice.getPlatform();

                        if(platform === 'iOS') {
                            hockeyAppId = window.config.hockeyApp.iOS;
                        } else if (platform === 'Android'){
                            hockeyAppId = window.config.hockeyApp.android;
                        }

                        if(hockeyAppId && hockeyAppId.length > 0) {

                            $window.hockeyapp.start(function () {
                                console.log('hockey started...');
                            }, function () {
                                console.log('hockey failed to start');
                            }, hockeyAppId);
                        }
                    }

                    
                    
                    
                    if($window.screen){

                        $window.screen.orientation.lock('landscape');
                    }

                    $cordovaAppVersion.getVersionNumber().then(function (version) {
                        $window.config.version = version;
                    });

                    $cordovaAppVersion.getVersionCode().then(function (build) {
                       $window.config.build = build;
                     });

                }

            }, false);

        });


        function onPause() {
            $rootScope.isPaused = true;
        }
        //generic connectivity checking for all screens fixed by kavi
        function connectivityCheck()
        {
        	//increased 4000 as per UK
          return connectivityStatusService.isOnline().then(function(online) {
            connectionPoll = $timeout(function() {
                return connectivityCheck();
            }, 4000);
              if (online) {
                  connectionPoll = undefined;
                  if(navigationService)
                  return navigationService.offline(false);
              }
              if(navigationService)
              navigationService.offline(true);


          });


        }


        function onResume() {
            $rootScope.isPaused = false;
            //translatedToastr.warning('TOAST.SUCCESS', 'TOAST.WARNING');
            //Technical task #23721 says that we need to notify the user if there are unprocessed items in the queue when the app is launched offline.
            connectivityStatusService.isOnline().then(function (online) {
                if (online === false && offlineQueue.queueLength() > 0) {
                    translatedToastr.warning('TOAST.UNPROCESSED_ITEMS_IN_QUEUE', 'TOAST.WARNING');
                }
            });

            notificationService.clearTriggeredNotification();
        }
        
function onNavigate(event, next, current)
{
if(next.split("#")[1])
	{
       localStorage.currentLocation=next.split("#")[1].replace("/","");
	}
}
        
    }



})();
