﻿(function () {
    'use strict';

    angular.module('merchantApp.createVoucher').constant('layoutConstants',
    [
        { translationKey: 'DROPDOWNS.LAYOUT_TYPE.A4', id: 1 },
        { translationKey: 'DROPDOWNS.LAYOUT_TYPE.THERMAL', id: 2 }
    ]);
})();
