﻿(function () {
    'use strict';

    angular.module('merchantApp.createVoucher').constant('fieldConstants',
    {
        HIDDEN: 0,
        OPTIONAL: 1,
        REQUIRED: 2
    });
})();