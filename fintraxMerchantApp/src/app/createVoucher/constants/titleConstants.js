﻿(function () {
    'use strict';

    angular.module('merchantApp.createVoucher').constant('titleConstants',
    [
        { translationKey: 'DROPDOWNS.TITLE.MR', id: 1 },
        { translationKey: 'DROPDOWNS.TITLE.MRS', id: 2 },
        { translationKey: 'DROPDOWNS.TITLE.MS', id: 3 }
    ]);
})();