﻿(function () {
    'use strict';
    angular
        .module('merchantApp.createVoucher')
        .controller('receiptNumberLookupDialogController', Controller);

    function Controller($scope, translatedToastr, $log, manateeBarcode) {
        $scope.scanReceipt = scanReceipt;

        function scanReceipt() {

            manateeBarcode.startScanning(stoppedScanning);
        }

        function stoppedScanning(result) {

            if(result === false){
                translatedToastr.warning('TOAST.NO_CORDOVA', 'TOAST.ERROR');
            } else {
                if(result.type !== 'Cancel'){
                    if(result && result.code){

                        $scope.closeThisDialog(result.code);
                        translatedToastr.success('TOAST.RECEIPT_SCAN_OK');

                    } else {
                        $log.error('Unable to open barcode scanner');
                        translatedToastr.warning('TOAST.RECEIPT_SCAN_ERROR', 'TOAST.ERROR');
                    }
                }
            }
        }

    }
})();

