﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher')
        .directive('muEditCustomerDetails', function (titleConstants, utilsService) {

            return {
                restrict: 'E',
                templateUrl: 'app/createVoucher/templates/editCustomerDetails.html',
                scope: { Tourist: '=tourist', isValid: '=', showEmail: '=', countries: '=' },
                link: function (scope, elem, attrs, ctrl) {                    

                    scope.titles = titleConstants;

                    scope.$watch('myForm.$valid', function (newval, oldval) {
                        scope.isValid = newval;                        
                    });

                    utilsService.setRequiredAndHiddenFieldsOnScope(scope);

                }
            };
        });

})();