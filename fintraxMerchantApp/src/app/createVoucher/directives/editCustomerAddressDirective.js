﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher')
        .directive('muEditCustomerAddress', function (fintraxApi, utilsService) {

            return {
                restrict: 'E',
                templateUrl: 'app/createVoucher/templates/editCustomerAddress.html',
                scope: { Address: '=address', isValid: '=', countries: '=' },
                link: function (scope, ele, attrs, ctrl) {

                    scope.$watch('myForm.$valid', function (newval, oldval) {
                        scope.isValid = newval;
                    });

                    utilsService.setRequiredAndHiddenFieldsOnScope(scope);
                }
            };
        });

})();