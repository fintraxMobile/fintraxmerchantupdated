﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher')
        .directive('muShopperDetails', function ($state, $stateParams) {


            return {
                restrict: 'E',
                templateUrl: 'app/createVoucher/templates/shopperDetails.html',
                scope: { Tourist: '=tourist', countries: '=' },
                link: function (scope, elem, attrs, ctrl) {

                    scope.editShopper = !$stateParams.reportsVoucher;                    

                    scope.editCustomerDetails = editCustomerDetails;
                    scope.editCustomerAddress = editCustomerAddress;

                    /*
                    //Translate IDs back into their actual selected value / icon.
                    if (scope.Tourist && scope.Tourist.Nationality && scope.Tourist.Nationality.Iso3Digit) {
                        for (var i = 0; i < countryConstants.length; i++) {
                            if (countryConstants[i].iso3Letter === scope.Tourist.Nationality.Iso3Digit) {
                                scope.flag = countryConstants[i].flag;
                                break;
                            }
                        }
                    }
                    */

                    function editCustomerDetails() {
                        $state.go('navigation.editCustomerDetails', {
                            title: 'NEWCUSTOMER.PASSPORTDETAILS_TITLE',
                            sectionNumber: parseInt(attrs.sectionNumber),
                            returnToState: $state.current.name
                        });
                    }


                    function editCustomerAddress(primaryAddress) {
                        if (primaryAddress) {
                            $state.go('navigation.editCustomerAddress', {
                                title: 'NEWCUSTOMER.CUSTOMER_DETAILS_TITLE',
                                sectionNumber: parseInt(attrs.sectionNumber),
                                returnToState: $state.current.name
                            });
                        } else {
                            if (angular.isDefined(scope.Tourist.AddressAdditional) === false) {
                                scope.Tourist.AddressAdditional = {};
                            }
                            $state.go('navigation.editCustomerAlternativeAddress', {
                                title: 'NEWCUSTOMER.CUSTOMER_DETAILS_TITLE',
                                sectionNumber: parseInt(attrs.sectionNumber),
                                returnToState: $state.current.name
                            });
                        }
                    }
                }
            };
        });

})();