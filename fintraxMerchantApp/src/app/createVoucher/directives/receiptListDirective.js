﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher')
        .directive('muReceiptList', function ($state, ngDialog, advancedReceiptValue, merchantAppConfig, commandService,
            translatedToastr, fintraxApi, $log, $rootScope, merchantConfig, utilsService) {

            return {
                restrict: 'E',
                templateUrl: 'app/createVoucher/templates/receiptlist.html',
                scope: { receipts: '=', addReceiptScenario: '@', isReadonly: '=',
                    isValid: '=', isReport:'@', recalculateRefund: '&' },
                link: function (scope, elem, attr) {
                    scope.editReceipt = editReceipt;
                    scope.deleteReceipt = deleteReceipt;
                    scope.showfullTextDescription=showfullTextDescription;
                    merchantConfig.getMerchantConfig().then(function (mc) {
                        scope.currencySymbol = mc.Store.CurrencySymbol;
                    });

                    if (!angular.isDefined(scope.receipts)) {
                        scope.receipts = [];
                    }

                    advancedReceiptValue.receipts = scope.receipts; //do this so we can pass receipts collection to advancedReceipt screen

                    if (!scope.isReadonly) {
                        commandService.addFooterButton('RECEIPTS.ADD_RECEIPT', addReceipt, commandService.BUTTON_TYPE.secondary);
                        scope.$on('$destroy', commandService.restoreDefaultButtons);
                    }

                    if(!scope.isReport) {

                        //refund value comes from the API if this is displaying a report voucher
                        scope.recalculateRefund();
                    }

                    scope.isValid = scope.receipts.length > 0;

                    function addReceipt() {
                        advancedReceiptValue.receipt = {};

                        createEditReceipt();
                    }


                    function editReceipt(receipt) {
                        advancedReceiptValue.receipt = receipt;
                        createEditReceipt();
                    }
                    
                    function showfullTextDescription(title,text)
                    {
                    	text=angular.isDefined(text)?text:" ";
                    	if(text.length>10 || title.length>10)
                    	utilsService.showGenericOkDialog(title,text);
                    	
                    	
                    }


                    function deleteReceipt(receipt) {
                        utilsService.showGenericOkCancelDialog('RECEIPTS.DELETE_RECEIPT_TITLE', 'RECEIPTS.DELETE_RECEIPT_MESSAGE')
                            .then(function (ok) {
                                if (ok) {
                                    scope.receipts.splice(scope.receipts.indexOf(receipt), 1);
                                    scope.recalculateRefund();
                                }
                            });
                    }


                    function createEditReceipt() {
                        if (scope.addReceiptScenario === merchantAppConfig.addReceiptScenarios.advanced1 ||
                            scope.addReceiptScenario === merchantAppConfig.addReceiptScenarios.advanced2 ||
                            scope.addReceiptScenario === merchantAppConfig.addReceiptScenarios.simple3) {
                            $state.transitionTo('navigation.advancedReceipt', {
                                previousState: $state.current.name,
                                sectionNumber: parseInt(attr.sectionNumber), simple: false
                            });
                        } else {
                            $state.transitionTo('navigation.simpleReceipt', {
                                previousState: $state.current.name,
                                sectionNumber: parseInt(attr.sectionNumber), simple: true
                            });
                        }
                    }
                }
            };
        });

})();