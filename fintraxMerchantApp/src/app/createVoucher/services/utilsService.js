﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher')
        .factory('utilsService', utilsService);

    function utilsService(merchantConfig, fieldConstants, $parse, ngDialog, $cordovaDevice, $window,translatedToastr) {
        return {
            setRequiredAndHiddenFieldsOnScope: setRequiredAndHiddenFieldsOnScope,
            hideOrRequireFields: hideOrRequireFields,
            requiredFieldsArePresent: requiredFieldsArePresent,
            getUniqueVATCodes: getUniqueVATCodes,
            showGenericOkCancelDialog: showGenericOkCancelDialog,
            showGenericOkDialog:showGenericOkDialog,
            isUkmerchant:isUkmerchant,
            isOnWindowsPlatform: isOnWindowsPlatform,
            inWords:inWords,
            convertStringToArray:convertStringToArray,
            acceptOnlyNumbers:acceptOnlyNumbers,
	        updateWalkthrough:updateWalkthroughFlexible,
            homeNavigationDecider:homeNavigationDecider,
            EnableCtrip:EnableCtrip,
            activateFlow:activateFlow,
            clearStorages:clearStorages,
            removeSpecialCharacters:removeSpecialCharacters,
            dateValidateMessenger:dateValidateMessenger,
            validDate:validDate,
            validExpiryDateforPast:validExpiryDateforPast
        };

        //Show or hide certain fields based on merchant config. Some fields may be required, while others are optional.
        //We'll assume that by default fields are visible and optional unless specified by Merchant Config.
        function setRequiredAndHiddenFieldsOnScope(scope) {
            return merchantConfig.getMerchantConfig().then(function (mc) { hideOrRequireFields(mc, scope); });
        }
        
        function dateValidateMessenger(attributes)
        {
			        	if (attributes) {
				var scope = attributes.scope;
				if (attributes.validatorName === "DateOfBirth") {
					if (scope.myForm.$error.DateOfBirth_incompleteDate
							&& (scope.myForm.day.$touched || scope.myForm.day.$modelValue)
							&& (scope.myForm.month.$touched || scope.myForm.month.$modelValue)
							&& (scope.myForm.year.$touched || scope.myForm.year.$modelValue))
						translatedToastr.error('VALIDATION.DATE_ERROR_INCOMPLETE');

					if (scope.myForm.$error.DateOfBirth_invalidDate
							&& (scope.myForm.day.$touched || scope.myForm.day.$modelValue)
							&& (scope.myForm.month.$touched || scope.myForm.month.$modelValue)
							&& (scope.myForm.year.$touched || scope.myForm.year.$modelValue))
						translatedToastr.error('VALIDATION.DATE_ERROR_INVALID');
					
					if (scope.myForm.$error.underAgeDateOfBirth && attributes.validationname === "underAgeDateOfBirth")
						{
						
						translatedToastr.error('VALIDATION.DATE_ERROR_UNDER_AGE');
						}

				}

				if (attributes.validatorName === "ExpirationDate") {
					if (scope.myForm.$error.ExpirationDate_incompleteDate
							&& (scope.myForm.dayExp.$touched || scope.myForm.dayExp.modelValue)
							&& (scope.myForm.monthExp.$touched || scope.myForm.monthExp.modelValue) 
							&& (scope.myForm.yearExp.$touched || scope.myForm.yearExp.modelValue))
						translatedToastr.error('VALIDATION.DATE_ERROR_INCOMPLETE');

					if (scope.myForm.$error.ExpirationDate_invalidDate
							&& (scope.myForm.dayExp.$touched || scope.myForm.dayExp.modelValue)
							&& (scope.myForm.monthExp.$touched || scope.myForm.monthExp.modelValue) 
							&& (scope.myForm.yearExp.$touched || scope.myForm.yearExp.modelValue))
						translatedToastr.error('VALIDATION.DATE_ERROR_INVALID');

				}






        	}
        	
        }

      

        function validDate(dateObjectToCheck) {
        	if(typeof dateObjectToCheck==="object")
        		{
        		if(  new Date('01/01/2000')>=dateObjectToCheck)
    	   
        			{   
        			dateObjectToCheck.setFullYear('20'+dateObjectToCheck.getFullYear());
    			   
        			}
        		}
    			   
    		return dateObjectToCheck;
    	   
        	
        	
        }
        function validExpiryDateforPast(dateObjectToCheck) {
        	if(typeof dateObjectToCheck==="string")
        		{
            if( new Date()>=new Date(dateObjectToCheck))
         	   
         	{   
         	   return null;
         			   
         	}
            
        		}
        	
        	
        	if(typeof dateObjectToCheck==="object")
        		{
        		
        		 if( new Date()>=dateObjectToCheck)
               	   
              	{   
              	   return null;
              			   
              	}
        		
        		}
         
         			   
         		return dateObjectToCheck;
         	   
             	
             	
             }
        
        function hideOrRequireFields(mc, scope) {
            scope.hiddenFields = {};
            scope.requiredFields = {};

            //later we'll use 'ng-if="hiddenFields.fieldName"' in the template

            mc.MandatoryFields.forEach(function (field) {

                if (field.Status === fieldConstants.HIDDEN) {

                    //Use parse as we have dotty fields
                    $parse('hiddenFields.' + field.Field).assign(scope, true);
                } else if (field.Status === fieldConstants.REQUIRED) {

                    //Use parse as we have dotty fields
                    $parse('requiredFields.' + field.Field).assign(scope, true);
                }
            });

        }


        function requiredFieldsArePresent(voucher) {
            return merchantConfig.getMerchantConfig().then(function (mc) {
                var allPresent = true;
                mc.MandatoryFields.forEach(function (field) {
                    if (field.Field.indexOf('Tourist') === 0) {
                        var result = $parse(field.Field)(voucher);
                        if (!result) {
                            console.log('Required field is missing in voucher: ' + field.Field);
                            allPresent = false;
                        }
                    }
                });
                return allPresent;
            });
        }


        function getUniqueVATCodes(merchantConf) {
            var vatRates = {};
            merchantConf.ChargeStructures.forEach(function (cs) {
                if (angular.isDefined(cs.VatPercentageappliesTo)) {
                	
                	if(!(isUkmerchant() && cs.VatPercentageappliesTo==21))
                		{
                		
                		 vatRates[cs.VatPercentageappliesTo] = true;
                		
                		}
                	
                	
                   
                }
            });
            //return ["20", "5.5", "10"];
            return Object.keys(vatRates);
        }

        function isOnWindowsPlatform(){

          return $window.cordova && $cordovaDevice.getPlatform() === 'windows';
        }
        
        function isUkmerchant()
        {
        	
        	 if(localStorage.profile)
         	{
         
         		if(angular.fromJson(localStorage.profile).isoCountryCode==826)
     	   
         			{
         			
         			return true;
         			
         			}
         		
         		else
         			{
         			
         			return false;
         			
         			
         			}
         			}
        	
        }
        
        function inWords (num) {
       	 var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
            var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
           if ((num = num.toString()).length > 9) return 'overflow';
        var  n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
           if (!n) return; var str = '';
           str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
           str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
           str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
           str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
           str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'pounds only ' : '';
           return str;
       }
        

        function showGenericOkCancelDialog(title, message) {
            return ngDialog.open({
                template: 'app/core/templates/genericOkCancelDialog.html',
                className: 'ngdialog-theme-plain',
                showClose: false,
                preserveFocus: false,
                disableAnimations:isOnWindowsPlatform(),
                data: {
                    title: title,
                    message: message
                }
            }).closePromise.then(function (data) {
                return data.value && data.value !== '$document';
            });
        }
        
        function showGenericOkDialog(title, message) {
            return ngDialog.open({
                template: 'app/core/templates/genericOkDialog.html',
                className: 'ngdialog-theme-plain',
                showClose: false,
                preserveFocus: false,
                disableAnimations:isOnWindowsPlatform(),
                data: {
                    title: title,
                    message: message
                }
            }).closePromise.then(function (data) {
                return data.value && data.value !== '$document';
            });
        }
        
        function convertStringToArray(stringValue)
        {
        	var arrayValue=[];
        	var exactString=""+stringValue;
        	for(var i=0;i<exactString.length;i++)
        		{
        		
        			arrayValue.push(exactString.charAt(i));
        		
        		}
        	
        	
        	return arrayValue;
        }
        
        
        function acceptOnlyNumbers(value,changingObject,paramName) {
        	value=""+value; 
            var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;

                  if (!value.match(reg)) {
                	  
                	  changingObject[paramName] = value.substring(0, value.length-1);
        

                  }
                 
          }
        
        function removeSpecialCharacters(param)
        {
        	
        	  if(!angular.isUndefined(param))
              	
          		{
          	
        		  return (""+param).replace(/[^\w\s]/gi, "");
          	
          	
          		}
        	
        }
	  
	  
	  function updateWalkthroughFlexible(arrayvalues)
        {
        	
        	if($window.localStorage.getItem('FlexibleOrientation')=='true')
        	{
        	
        		arrayvalues[0].imageUrl="images/walkthrough/potrait.png";
            	arrayvalues[1].imageUrl="images/walkthrough/potrait.png";
        		
        	}
        	
        	else
        	{
        		arrayvalues[0].imageUrl="images/walkthrough/1_1460x1050.png";
            	arrayvalues[1].imageUrl="images/walkthrough/2_1460x1050.png";
        		
        		
        		
        	}
        	
        	
        	return arrayvalues;
       	
        	
        } 
	 
	 
	 function homeNavigationDecider()
        
        {
        	//replace with swtich case
		 
		 var navigationScreen='navigation.home';
		 
		 switch(true){

         case $window.localStorage.getItem('FlexibleOrientation')=='true':
        	 return "navigation.voucherdashboardflexible";
             break;
        	 
         case $window.localStorage.getItem('NoMandateFields')=='true':
        	 return "navigation.voucherdashboardflexible";
             break;      
             
             
             }
		return navigationScreen; 
        
		//return $window.localStorage.getItem('FlexibleOrientation')=='true'?"navigation.voucherdashboardflexible":"navigation.home";
        	 
        	
        }
	 
	 function EnableCtrip(config)
	 {
		if(config && $window.localStorage.getItem('EnableCtrip')=='true')
			{
			
			
		 var ctripStatic={
     			IsCardRefund:false,
     			IsCashRefund:false,
     			RefundCode:"1702",
     			RefundCodeDescription:"CTRIP"
     			
     			
     	}
     	
     	config.RefundOptions.push(ctripStatic);
			}
		
		return config;		
		
		 
	 }
	 //for future usage
	 function activateFlow(config)
	 {    
		 //for windows fixes
		 var $window=angular.isUndefined($window)?window:$window; 
		 
		 //will implement on next release on all empty places
		  config.Settings.forEach(function(setting){

              switch(setting.Name){

                  case 'VipRefundAllowed':
                   //replace with local storage
                      break;

                  case 'AddReceiptScenario':
                	//replace with local storage 
                      break;

                  case 'EnableTouristSignature':
                	//replace with local storage
                      break;

                  case 'EnableMerchantSignature':
                	//replace with local storage 
                      break;
                  case 'EnableOrientation':
                	 if($window.screen)
                		 {
                		 setting.Value=='True'?$window.screen.orientation.unlock() : $window.screen.orientation.lock('landscape');
                		 setting.Value=='True'?$window.localStorage.setItem('FlexibleOrientation', true)  :$window.localStorage.setItem('FlexibleOrientation', false); 
                		 }
                      break;
                  case 'EnableQuickPrint':
                  	  $window.localStorage.setItem('EnableQuickPrint',  setting.Value=='True'?true:false);
                      break;
                      
                  case 'EnableCtrip':
                  	  $window.localStorage.setItem('EnableCtrip',setting.Value=='True'?true:false);
                      break;      
                      
                      
                      
                      
              }
          });
		 
		 
		 
	 }
	 
	 function clearStorages()
	 
	 {
		  //by default landscape while initializing an application
	
		 localStorage.clear();
       
    	var  $window = angular.isUndefined($window)?window:$window;
		
		 if($window.screen){

             $window.screen.orientation.lock('landscape');
         }
		 
		 
	 }
	 
	 
	 
	 
	 
	 
        
         
        
    }
})();
