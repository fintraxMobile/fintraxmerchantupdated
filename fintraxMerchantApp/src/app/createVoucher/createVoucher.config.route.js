﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher')
        .config(configureRoutes);


    function configureRoutes() {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
    }

})();
