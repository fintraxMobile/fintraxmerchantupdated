(function () {
    'use strict';

    angular
        .module('merchantApp.education')
        .constant('walkthroughConstants', walkthoughConstants());

    function walkthoughConstants() {

        return {
            walkthroughItems: [
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_1_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_1_BODY',
                    imageUrl:'images/walkthrough/1_1460x1050.png',
                    selected:true
                },
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_2_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_2_BODY',
                    imageUrl:'images/walkthrough/2_1460x1050.png',
                    selected:false
                },
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_3_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_3_BODY',
                    imageUrl:'images/walkthrough/3_1460x1050.png',
                    selected:false
                },
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_4_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_4_BODY',
                    imageUrl:'images/walkthrough/4_1460x1050.png',
                    selected:false
                },
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_5_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_5_BODY',
                    imageUrl:'images/walkthrough/5_1460x1050.png',
                    selected:false
                },
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_6_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_6_BODY',
                    imageUrl:'images/walkthrough/6_1460x1050.png',
                    selected:false
                },
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_7_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_7_BODY',
                    imageUrl:'images/walkthrough/7_1460x1050.png',
                    selected:false
                },
                {
                    titleTranslationKey:'WALKTHROUGH.STEP_8_TITLE',
                    bodyTranslationKey:'WALKTHROUGH.STEP_8_BODY',
                    imageUrl:'images/walkthrough/8_1460x1050.png',
                    selected:false
                }

            ]
        };

    }

})();