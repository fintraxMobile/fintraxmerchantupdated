(function () {
    'use strict';
    angular
        .module('merchantApp.education')
        .controller('phrasebookController', Controller);

    function Controller(navigationService, commandService, $state,
                        fintraxApi, $q, $translate, localeService) {

        var vm = this;
        vm.phrases = [];
        vm.languages = [];
        vm.fromLan = '';
        vm.toLan = '';
        vm.currentPhrase = null;
        vm.switchLanguages = switchLanguages;
        var defaultLanguage = '';
        vm.languageCode = '';

        activate();

        function activate(){
            vm.languageCode = localeService.getCurrentDeviceLanguageCode();

            navigationService.pageClass('education phrasebook');
            navigationService.setTitle('EDUCATION.PHRASEBOOK_TITLE');
            commandService.removeAllButtons();
            commandService.addSvgButton('back', back, 'left');
            fintraxApi.getPhrasebook(vm.languageCode).then(loadPhrasebookData);
        }

        function loadPhrasebookData(phrasebookData){
            vm.phrases = phrasebookData.phrasebook;

            var firstPhrase = vm.phrases[0].phrase;
            var promiseArray = [];

            for(var languageCode in firstPhrase){

                if(firstPhrase.hasOwnProperty(languageCode) && languageCode !== 'Reference'){

                    promiseArray.push(translateLanguageCode(languageCode));
                }
            }

            //wait till all the language names are translated
            $q.all(promiseArray).then(function(){

                //set the default from language
                var matchingLanguage = vm.languages.filter(function(item){

                    return item.code === vm.languageCode.toUpperCase();
                });

                if(matchingLanguage.length === 1){

                    defaultLanguage = matchingLanguage[0];
                    vm.fromLan = defaultLanguage;
                }

            });

            //select the first translation
            vm.currentPhrase = vm.phrases[0];

        }

        function switchLanguages(){
            var temp = vm.fromLan;

            if(vm.toLan !== '') {
                vm.fromLan = vm.toLan;
            } else {

                //we don't ever want the from language to be 'please select' as it screws up the UI
                vm.fromLan = defaultLanguage;
            }

            vm.toLan = temp;
        }

        function translateLanguageCode(languageCode){

            //we need the name of the language in the current user's language
            return $translate('LANGUAGES.' + languageCode).then(function(translated){

                vm.languages.push({ code: languageCode, title: translated });
            }, function(){

                //translation was missing, just show the code
                vm.languages.push({ code: languageCode, title: languageCode });
            });
        }

        function back() {
            $state.go('navigation.education');
        }

    }
})();

