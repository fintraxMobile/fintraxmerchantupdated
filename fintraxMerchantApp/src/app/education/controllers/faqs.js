(function () {
    'use strict';
    angular
        .module('merchantApp.education')
        .controller('faqsController', Controller);

    function Controller(navigationService, commandService, fintraxApi,
                        $state, localeService, merchantConfig) {

        var vm = this;
        vm.faqs = [];
        vm.categories = [];
        vm.chosenCategory = '';
        vm.noFaqs = false;

        activate();

        function activate(){
            navigationService.pageClass('education faqs');
            navigationService.setTitle('EDUCATION.FAQS_TITLE');
            commandService.removeAllButtons();
            commandService.addSvgButton('back', back, 'left');

            merchantConfig.getMerchantConfig().then(function(config){

                var merchantCountryThreeDigitIso = config.Store.Country.Iso;
                lookupCountryLoadFaqs(merchantCountryThreeDigitIso);
            });


        }

        function lookupCountryLoadFaqs(merchantCountryThreeDigitIso){

            fintraxApi.getCountries().then(function(countries){

                var matchingCountry = countries.filter(function(country){
                    return country.Iso3Digit === merchantCountryThreeDigitIso;
                });

                if(matchingCountry.length === 1){
                    var languageTwoLetter = localeService.getCurrentDeviceLanguageCode();
                    fintraxApi.getFaqs(languageTwoLetter, matchingCountry[0].Iso2Letter.toLowerCase())
                    .then(loadFaqs)
                    .catch(function(){
                        vm.noFaqs = true;
                    });
                } else {

                    vm.noFaqs = true;
                    console.log('unable to find matching country when looking up FAQs!');
                }

            });

        }


        function loadFaqs(faqData){
            vm.faqs = faqData.nodes;

            findUniqueCategories();

            if(vm.faqs.length > 0) {
               vm.chosenCategory = vm.faqs[0].node.Category;
           } else {
               vm.noFaqs = true;
           }
        }

        function findUniqueCategories(){

            vm.faqs.map(function(item){

                if(vm.categories.indexOf(item.node.Category) === -1){

                    vm.categories.push(item.node.Category);
                }

            });
        }

        function back() {
            $state.go('navigation.education');
        }


    }
})();
