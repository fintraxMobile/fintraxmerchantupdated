(function () {
    'use strict';
    angular
        .module('merchantApp.education')
        .controller('walkthroughController', Controller);

    function Controller(navigationService, commandService, walkthroughConstants, $state, $window,utilsService) {

        var vm = this;
        vm.index = 0;
        vm.walkthroughItems = walkthroughConstants.walkthroughItems; 	 
        vm.walkthroughItems=utilsService.updateWalkthrough(walkthroughConstants.walkthroughItems);  
        vm.backwards = false;
        vm.next = next;
        vm.previous = previous;

        activate();

        function activate(){

            resetWalkthrough();

            navigationService.pageClass('walkthrough');
            navigationService.setTitle('SETTINGS.WALKTHROUGH');
            
           

            commandService.removeAllButtons();
            commandService.addButton('COMMANDS.DONT_SHOW_AGAIN', dontShowAgain, 'left', false, undefined, commandService.BUTTON_TYPE.tertiary);
            commandService.addButton('COMMANDS.MAIN_MENU', skip, 'right', false);
          
        }

        function resetWalkthrough(){

            vm.index = 0;
            vm.walkthroughItems.map(function(item){
                item.selected = false;
            });

            vm.walkthroughItems[0].selected = true;
        }

        function skip(){
        	 
        	//$window.localStorage.getItem('FlexibleOrientation')=='true'?$state.go('navigation.voucherdashboardflexible'):$state.go('navigation.home')
          //  $state.go('navigation.home');
        	
        	$state.go(utilsService.homeNavigationDecider());
        }

        function dontShowAgain(){
        	
        	//$window.localStorage.getItem('FlexibleOrientation')=='true'?$state.go('navigation.voucherdashboardflexible'):$state.go('navigation.home')
           
        	// $state.go('navigation.home');
        	
        	$state.go(utilsService.homeNavigationDecider());
        }

        function next(){

            vm.backwards = false;

            vm.walkthroughItems.map(function(item){
                item.selected = false;
            });

            addOrResetIndex(true);

            vm.walkthroughItems[vm.index].selected = true;
        }

        function previous(){

            vm.backwards = true;

            vm.walkthroughItems.map(function(item){
                item.selected = false;
            });

            addOrResetIndex(false);

            vm.walkthroughItems[vm.index].selected = true;
        }

        function addOrResetIndex(positive){

            if(positive){

                vm.index++;

                if(vm.index > vm.walkthroughItems.length -1){
                    vm.index = 0;
                }

            } else {

                vm.index--;

                if(vm.index < 0){
                    vm.index = vm.walkthroughItems.length -1;
                }
            }
        }


    }
})();

