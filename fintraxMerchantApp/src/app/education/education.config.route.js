(function () {
    'use strict';

    angular
        .module('merchantApp.education')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.phrasebook', {
            url: '^/phrasebook',
            controller: 'phrasebookController as vm',
            templateUrl: 'app/education/templates/phrasebook.html'
        }).state('navigation.faqs', {
            url: '^/faqs',
            controller: 'faqsController as vm',
            templateUrl: 'app/education/templates/faqs.html'
        }).state('navigation.walkthrough', {
            url:'^/walkthrough',
            controller: 'walkthroughController as vm',
            templateUrl: 'app/education/templates/walkthrough.html'
        });

    }

})();
