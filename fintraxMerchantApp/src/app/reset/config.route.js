(function () {
    'use strict';

    angular
        .module('merchantApp.reset')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('reset', {
            url: '^/reset',
            controller: 'ResetController as reset',
            templateUrl: 'app/reset/reset.html'
        });
    }

})();
