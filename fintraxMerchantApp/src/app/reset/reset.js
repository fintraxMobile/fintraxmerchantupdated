(function () {

    angular
        .module('merchantApp.reset')
        .controller('ResetController', Reset);


    /* @ngInject */
    function Reset(authentication, merchantConfig, $window, navigationService,
        translatedToastr, $log, $state, fintraxApi) {
        /* jshint validthis: true */
        var vm = this;

        vm.reset = reset;
        vm.cancel=cancel;
        vm.isAuthenticated = false;

        activate();

        function reset(user) {
          //clear it for ever login.
        	
        	//debugger;
           authentication.reset(user,cancel);
           
        }
      
        
        function cancel() {
        	
        	 $state.transitionTo('login');	
        	
        }
        

        function loginSuccess(profile) {
            vm.profile = profile;
            vm.isAuthenticated = authentication.isAuthenticated();

            $log.log(profile);

            //load the merchant config for the first time
            merchantConfig.refreshMerchantConfig(true).then(function (config) {

                //download the countries list to ensure it's cached for offline use
                fintraxApi.getCountries().then(function(){b 

                    if($window.localStorage.getItem('hideWalkthrough') !== 'true'){

                        $state.go('navigation.walkthrough');
                    } else {

                        $state.go('navigation.home');
                    }
                });

            }, function (err) {
                translatedToastr.error('LOGIN.UNABLE_TO_DOWNLOAD_MERCHANT_CONFIG', null, { target: 'body' });
                console.log(err);
            });

            translatedToastr.success('LOGIN.SUCCESS', null, { target: 'body' });

        }

        function loginFail(Error) {

            var message = 'LOGIN.UNABLE_TO_LOG_IN';

            if (Error.code === 'invalid_user_password') {
                message = 'LOGIN.INVALID_USER_PASSWORD';
            }

            translatedToastr.error(message, null, { target: 'body' });
        }

        function activate() {
            navigationService.pageClass('login');

            vm.profile = authentication.getProfile();
            vm.isAuthenticated = authentication.isAuthenticated();
        }
        
        

    }

})();
