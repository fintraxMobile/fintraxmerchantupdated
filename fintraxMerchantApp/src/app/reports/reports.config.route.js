(function () {
    'use strict';

    angular
        .module('merchantApp.reports')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.reportcustom', {
            url: '^/reportcustom',
            controller: 'customReportController as vm',
            templateUrl: 'app/reports/templates/customReport.html'
        }).state('navigation.voucherReview', {
            abstract: true,
            templateUrl: 'app/refund/templates/refundReviewTabs.html',
            params: { reportsVoucher: null },
            controller: 'refundReviewTabsController as vm'
        }).state('navigation.voucherReview.shopperDetails', {
            url: '^/voucherreviewshopper',
            controller: 'refundReviewShopperDetailsController as vm',

            templateUrl: 'app/refund/templates/refundReviewShopperDetails.html'
        }).state('navigation.voucherReview.purchaseDetails', {
            url: '^/voucherreviewpurchase',
            controller: 'refundReviewPurchaseController as vm',
            templateUrl: 'app/refund/templates/refundReviewPurchase.html'
        });

    }

})();
