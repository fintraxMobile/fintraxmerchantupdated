(function(){
    angular
        .module('merchantApp.reports')
        .controller('customReportController', CustomReport);

    /* @ngInject */
    function CustomReport($scope, $state, newVoucherValue, navigationService, commandService,
                          voucherStatusConstants, voucherListService, utilsService,
                          merchantConfig, printService, translatedToastr, fintraxApi) {
        /* jshint validthis: true */
        var vm = this;
        vm.searchTerm = '';
        vm.countryFilter = null;
        vm.statusFilter = null;
        vm.searchFilter = searchFilter;
        vm.countryList = [];
        vm.statusList = voucherStatusConstants;
        vm.dateDescending = true;
        vm.reportItems = [];
        vm.showFilter = false;
        vm.showSearch = false;
        vm.viewVoucher = viewVoucher;
        vm.currencySymbol = '';
        vm.reportToPrint = '';

        var currentMerchantConfig = {};

        activate();

        function activate() {

            navigationService.setTitle('REPORTS.CUSTOM_REPORT_TITLE');

            navigationService.pageClass('reports');

            commandService.removeAllButtons();

            commandService.addSvgButton('back', back, 'left');
            commandService.addSvgButton('refresh', refresh, 'left');
            commandService.addButton('COMMANDS.PRINT', print, 'right', false, null, commandService.BUTTON_TYPE.secondary);
            commandService.addSvgButton('filter', modeFilter, 'right');
            commandService.addSvgButton('search', modeSearch, 'right');

            $scope.$on('$destroy', commandService.restoreDefaultButtons);


            fintraxApi.getCountries().then(function(countries){
                vm.countryList = countries;

                merchantConfig.getMerchantConfig().then(function(config){

                    currentMerchantConfig = config;

                    if(navigationService.offline()){

                        voucherListService.getVoucherList(config).then(function(response){

                            response.forEach(function(item){

                                var matchingCountry = countries.filter(function(c){

                                    return item.Tourist.Address && item.Tourist.Address.Country &&
                                        c.Iso3Digit === item.Tourist.Address.Country.Iso3Digit;
                                });

                                if(matchingCountry.length === 0){

                                    item.Address.Country = matchingCountry[0];
                                }

                            });

                            vm.reportItems = response;
                        });
                    } else {

                        //if online, always get an up-to-date
                        voucherListService.refreshVoucherList(currentMerchantConfig).then(function(result){
                            vm.reportItems = result;
                        });
                    }

                    vm.currencySymbol = config.Store.CurrencySymbol;

                });
            });



        }

        //we pass voucherStatus in here because it's not returned by getVoucherByVoucherNumber
        function viewVoucher(voucherBarcodeNumber, voucherStatus){

            fintraxApi.getVoucherByVoucherNumber(voucherBarcodeNumber).then(function(data){

                //why doesn't this work?
                //newVoucherValue = data;

                //clear the voucher value
                for(var voucherKey in newVoucherValue){
                    if(newVoucherValue.hasOwnProperty(voucherKey)){
                        delete newVoucherValue[voucherKey];
                    }
                }

                //set it to new value
                //because there might be some properties in newVoucherValue but not the data response.
                for(var key in data){
                    if(data.hasOwnProperty(key)){
                        newVoucherValue[key] = data[key];
                    }
                }

                newVoucherValue.VoucherStatus = voucherStatus;

                $state.go('navigation.voucherReview.purchaseDetails', { reportsVoucher: true });
            }, function(err){

                translatedToastr.error('REPORTS.UNABLE_TO_FIND_VOUCHER');

            });

        }

        function back() {
            $state.go('navigation.home');
        }

        function searchFilter(voucher){
            if (angular.isDefined(voucher.Tourist) && angular.isDefined(voucher.Tourist.Nationality)) {
                //match the Customer name OR Voucher number
            	//UK reports issue
            	voucher.Tourist.FirstName==null?voucher.Tourist.FirstName="":"";
            	voucher.Tourist.LastName==null?voucher.Tourist.LastName="":"";
            	
                var searchMatch = voucher.Tourist.FirstName.toUpperCase().indexOf(vm.searchTerm.toUpperCase()) > -1 ||
                    voucher.Tourist.LastName.toUpperCase().indexOf(vm.searchTerm.toUpperCase()) > -1 ||
                    voucher.Number.toUpperCase().indexOf(vm.searchTerm.toUpperCase()) > -1;

                //AND the country of residence
                var touristCountry = voucher.Tourist.Address && voucher.Tourist.Address.Country ?
                    voucher.Tourist.Address.Country.Iso3Digit : null;

                var countryMatch = vm.countryFilter === null ||
                    touristCountry === vm.countryFilter;

                //AND the status
                var statusMatch = vm.statusFilter === null ||
                        voucher.VoucherStatus === vm.statusFilter;

                return searchMatch && countryMatch && statusMatch;
            }
            return true; //TODO - we currently include "dodgy" vouchers!
        }

        function modeFilter() {
            vm.showFilter = !vm.showFilter;

            //also clear the filters
            if(!vm.showFilter){

                vm.countryFilter = null;
                vm.statusFilter = null;
            }

            setPageClass();
        }

        function modeSearch() {
            vm.showSearch = !vm.showSearch;

            //also clear the search term
            if(!vm.showFilter){

                vm.searchTerm = '';
            }

            setPageClass();
        }

        //so the filter/search buttons can be a different colour when enabled
        function setPageClass(){

            var pageClass = 'reports';

            if(vm.showSearch){
                pageClass += ' search-on';
            }

            if(vm.showFilter){
                pageClass += ' filter-on';
            }

            navigationService.pageClass(pageClass);
        }

        function refresh(){
            console.log('refresh');

            voucherListService.refreshVoucherList(currentMerchantConfig).then(function(result){
                vm.reportItems = result;
            });
        }

        function print(){

            //there's only one html table on the page
            var reportTable = angular.element(document.getElementById('reports-container')).html();

            if(utilsService.isOnWindowsPlatform()){
              var fragment = document.createDocumentFragment();
              var container = document.createElement('div');
              container.innerHTML = reportTable;
              fragment.appendChild(container);

              reportTable = fragment;
            }

            var printed = printService.print(reportTable);

            if(!printed){
                translatedToastr.error('TOAST.UNABLE_TO_PRINT');
            }
        }

    }
})();
