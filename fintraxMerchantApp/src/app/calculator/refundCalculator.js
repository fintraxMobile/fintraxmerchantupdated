(function () {
    'use strict';

    angular
        .module('merchantApp.calculator')
        .controller('refundCalculatorController', refundCalculator);

    /* @ngInject */
    function refundCalculator($scope, $state, navigationService, merchantConfig,
                              commandService, refundCalculationService,utilsService) {
        /* jshint validthis: true */
        var vm = this;
        vm.purchaseAmount = 0;
        vm.refundAmount = {
            status:'N/A',
            totalRefund:0
        };

        vm.minimumAmount = 0;

        var currentMerchantConfig = {};
        var vatRate = 0;

        //we'll use this to send to the refund calculation service later
        var dummyVoucher = {

            RefundOption: {
                RefundOptionCode:''
            },
            Invoices:[
                {
                    pricing:'total',
                    InvoiceDateTime: new Date(),
                    NetAmount:0,
                    GrossAmount:0,
                    LineItems: [
                        {
                            Description:'',
                            VatRate:null,
                            NetAmount:0,
                            GrossAmount:0

                        }
                    ]
                }
            ]
        };

        activate();

        function activate() {
            navigationService.setTitle('REFUND_CALCULATOR.REFUND_CALCULATOR_TITLE');
            navigationService.pageClass('refund-calculator');

            commandService.removeAllButtons();
            commandService.addSvgButton('back', back, 'left');
            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            //we don't want to keep fetching the config every time we calculate a refund
            merchantConfig.getMerchantConfig().then(function(config){

                currentMerchantConfig = config;
                
                $scope.fields=config.Settings;
                
                vm.currencySymbol = config.Store.CurrencySymbol;

                //the default VAT percentage is the highest rate available to the merchant
                vatRate = 0;
                
              

                config.ChargeStructures.forEach(function(structure){

               if(!utilsService.isUkmerchant())
            	   {
                    	if(structure.VatPercentageappliesTo > vatRate){
                        vatRate = structure.VatPercentageappliesTo;
                                                                                    
                        
                    	}
                    
            	   }
               
               else
            	   {
            	   //for Uk the story is different that minimum vat ranges applied for each stores as per the 
            	   
            	   if(structure.VatPercentageappliesTo > vatRate && structure.VatPercentageappliesTo<21){
                       
            		   
            		   vatRate = structure.VatPercentageappliesTo;
                                                                                   
                       
            	   }
            	   
            	   
            	   }
                    
                                     
                    

                });

                vm.minimumAmount = config.Store.MinimumAmount;

                updateRefundAmounts();

            });

            $scope.$watch('vm.purchaseAmount', updateRefundAmounts);
        }

        function back(){
            return $state.go('navigation.home');
        }

        function updateRefundAmounts(){

            if(currentMerchantConfig.RefundOptions) {

                var netAmount = vm.purchaseAmount / (1 + (vatRate / 100));

                dummyVoucher.Invoices[0].GrossAmount = vm.purchaseAmount;
                dummyVoucher.Invoices[0].NetAmount = netAmount;
                dummyVoucher.Invoices[0].LineItems[0].GrossAmount = vm.purchaseAmount;
                dummyVoucher.Invoices[0].LineItems[0].NetAmount = netAmount;
                dummyVoucher.Invoices[0].LineItems[0].VatRate = vatRate;
                
               //We need VatAmount as well for the perfect refund calculation
                
                if(utilsService.isUkmerchant())
                	{
                       dummyVoucher.Invoices[0].LineItems[0].VatAmount= vm.purchaseAmount- netAmount;
                	
                	}
                
                

                var calculated = refundCalculationService.calculateRefund(currentMerchantConfig, dummyVoucher);
                vm.refundAmount = calculated;

            }

        }
        
        function getFieldByName(prop){
            var field = undefined;
            //Or just use a for loop and break once you find a match
            $scope.fields.some(function(itm){
                if(itm.fieldName === prop){
                   field = itm;
                   return true;
                }
            });
            //Or you could inject $filter as well an do as below
            //return $filter('filter')($scope.fields,{fieldName:"street"})[0] || {}
            return field;
       }

        

    }
})();
