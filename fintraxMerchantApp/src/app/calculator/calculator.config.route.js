(function () {
    'use strict';

    angular
        .module('merchantApp.calculator')
        .config(configureRoutes);


    function configureRoutes($stateProvider) {
        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.refundcalculator', {
            url: '^/refundcalculator',
            controller: 'refundCalculatorController as vm',
            templateUrl: 'app/calculator/templates/refundCalculator.html'
        });
    }

})();
