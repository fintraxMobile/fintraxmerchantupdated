(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher.refund')
        .config(configureRoutes);

    function configureRoutes($stateProvider) {

        // The ^ character means we want to match the exact url rather than parent/setup it's just /setup
        $stateProvider.state('navigation.refundtypes', {
            url: '^/refundtypes',
            controller: 'refundTypesController as vm',
            templateUrl: 'app/refund/templates/refundTypes.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundCard', {
            url: '^/refundcard',
            controller: 'refundCardController as vm',
            params: { isTopos: false },
            templateUrl: 'app/refund/templates/refundCard.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundToposCard', {
            url: '^/refundtoposcard',
            controller: 'refundCardController as vm',
            params: { isTopos: true },
            templateUrl: 'app/refund/templates/refundCard.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundAlternativeAddress', {
            url: '^/refundalternativeddress',
            controller: 'refundAlternativeAddressController as vm',
            templateUrl: 'app/refund/templates/editCustomerAlternativeAddress.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundAliPay', {
            url: '^/refundalipay',
            controller: 'refundAliPayController as vm',
            templateUrl: 'app/refund/templates/refundAliPay.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundPrint', {
            url: '^/refundprint',
            controller: 'refundPrintController as vm',
            templateUrl: 'app/refund/templates/refundPrint.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundReview', {
            abstract: true,
            templateUrl: 'app/refund/templates/refundReviewTabs.html',
            controller: 'refundReviewTabsController as vm',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundReview.shopperDetails', {
            url: '^/refundreviewshopper',
            controller: 'refundReviewShopperDetailsController as vm',
            templateUrl: 'app/refund/templates/refundReviewShopperDetails.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.refundReview.purchaseDetails', {
            url: '^/refundreviewpurchase',
            controller: 'refundReviewPurchaseController as vm',
            templateUrl: 'app/refund/templates/refundReviewPurchase.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.editCustomerDetails', {
            url: '^/editcustomerdetails',
            controller: 'editCustomerController as vm',
            params: { title: null, sectionNumber: null, returnToState: null },
            templateUrl: 'app/refund/templates/editCustomerDetails.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.editCustomerAddress', {
            url: '^/editcustomeraddress',
            controller: 'editCustomerController as vm',
            params: { title: null, sectionNumber: null, returnToState: null },
            templateUrl: 'app/refund/templates/editCustomerAddress.html'
        }).state('navigation.editCustomerAlternativeAddress', {
            url: '^/editcustomeralternativeaddress',
            controller: 'editCustomerController as vm',
            params: { title: null, sectionNumber: null, returnToState: null },
            templateUrl: 'app/refund/templates/editCustomerAlternativeAddress.html',
            data: {
                isVoucherCreationRoute:true
            }
        }).state('navigation.editReceiptList', {
            url: '^/editreceiptlist',
            controller: 'editReceiptListController as vm',
            params: { useAdvancedReceiptValue: null },
            templateUrl: 'app/refund/templates/editReceiptList.html',
            data: {
                isVoucherCreationRoute:true
            }
        });

    }

})();
