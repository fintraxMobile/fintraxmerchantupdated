﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher.refund')
        .factory('creditCardValidationService', service);

    //We currently validate card type and length. If further card number validation is required (checksums, etc) then we could look at adapting:
    //https://github.com/PawelDecowski/jQuery-CreditCardValidator/
    function service() {

        var cardTypes = [
        {
            name: 'amex',
            pattern: /^3[47]/,
            validLength: 15,
            refundCode: '0101'
        }, {
            name: 'diners',
            pattern: /^(30[0-5]|309|36|3[8-9])/,
            validLength: 14,
            refundCode: '0102'
        }, {
            name: 'jcb',
            pattern: /^35[2-8]/,
            validLength: 16,
            refundCode: '0105'
        }, {
            name: 'visa',
            pattern: /^4/,
            validLength: 16,
            refundCode: '0103'
        }, {
            name: 'mastercard',
            pattern: /^5[1-5]/,
            validLength: 16,
            refundCode: '0104'
        }, {
            name: 'cups',
            pattern: /^(6([0-2]|858))|(95559[0-5])|(98430[23])|(99880[12])/,
            validLength: -1, //special case- 15 to 19 characters
            refundCode: '0106'
        }
        ];

        return {
            validateCard: validateCard
        };


        //Returns negative for too short, 0 for just right, positive for too long.
        function validateCard(cardNumber) {
            var cardType = getCardType(cardNumber);
            //cups is its own special case!
            if (cardType && cardType.name === 'cups') {
                return {
                    validLength: cardNumber.length >= 15 && cardNumber.length <= 19,
                    cardType: cardType,
                    isTooLong: cardNumber.length > 19,
                    isValidLuhn:true, //luhn check not valid for CUPS
                    lengthDesc: '15 - 19'
                };
            }

            //all other cards work in the expected way
            return {
                validLength: cardType ? cardNumber.length === cardType.validLength : true,
                cardType: cardType,
                isTooLong: cardType ? cardNumber.length > cardType.validLength : false,
                isValidLuhn:isValidLuhn(cardNumber),
                lengthDesc: cardType ? cardType.validLength : ''
            };
        }

        function getCardType(cardNumber) {
            return cardTypes.filter(function (ct) {
                return cardNumber.match(ct.pattern);
            })[0];
        }

        function  isValidLuhn(number) {
            var digit, n, sum, _j, _len1, _ref1;
            sum = 0;
            _ref1 = number.split('').reverse();

            for (n = _j = 0, _len1 = _ref1.length; _j < _len1; n = ++_j) {
                digit = _ref1[n];
                digit = +digit;
                if (n % 2) {
                  digit *= 2;
                  if (digit < 10) {
                    sum += digit;
                  } else {
                    sum += digit - 9;
                  }
                } else {
                  sum += digit;
                }
            }
            return sum % 10 === 0;
        }

    }
})();
