﻿(function () {
    'use strict';

    angular
        .module('merchantApp.createVoucher.refund')
        .factory('jumioAuthorisationTokenService', jumioAuthorisationTokenService);

    function jumioAuthorisationTokenService(merchantAppConfig) {        
        return {
            getToken: getToken
        };

        //This requires CryptoJS library (https://code.google.com/p/crypto-js/)
        function getToken() {            
            var encryptionKey = CryptoJS.enc.Base64.parse(merchantAppConfig.jumioCardScan.encryptionKey);
            var checksumKey = CryptoJS.enc.Base64.parse(merchantAppConfig.jumioCardScan.checksumKey);
            // Step 1. a
            var message = new Date().getTime() + ';' + merchantAppConfig.jumioCardScan.reference;
            // Step 1.b
            var iv = CryptoJS.lib.WordArray.random(128 / 8);
            // Step 1.c
            var encryptedMessage = CryptoJS.AES.encrypt(message, encryptionKey, { iv: iv });
            // Step 2
            /* Note:
            * Instead of concatenating encryptedMessage and iv into one byte array (step 2.a),
            * this sample uses hmacSha1.update() separately.
            */
            var hmacSha1 = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA1, checksumKey);
            hmacSha1.update(encryptedMessage.ciphertext);
            hmacSha1.update(iv);
            var hmac = hmacSha1.finalize();
            // Steps 3 and 4
            var authorizationToken = CryptoJS.enc.Base64.stringify(encryptedMessage.ciphertext.concat(iv).concat(hmac));            return authorizationToken;
        }


    }
})();
