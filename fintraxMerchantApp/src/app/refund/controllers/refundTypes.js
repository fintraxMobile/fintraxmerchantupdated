(function () {
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('refundTypesController', refundTypesController);

    /* @ngInject */
    function refundTypesController(
        $scope,
        navigationService,
        commandService,
        newVoucherValue,
        refundTypesValue,
        ngDialog,
        merchantConfig,
        connectivityStatusService,
        manateeBarcode,
        utilsService,
        fintraxApi
    ) {
        /* jshint validthis: true */
        var vm = this;
        vm.refundTypes = [];
        vm.selectedRefundTypeId = '';
        vm.premierPassScan=premierPassScan;
        connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(){
            activate();
        });

        function activate(){
            navigationService.pageValidityChanged(false, null, null, true);
            vm.voucher = newVoucherValue;

            navigationService.setTitle('REFUND.REFUND_TYPE', 3);
            navigationService.pageClass('refundTypes');

            commandService.addButton('COMMANDS.NEXT', next, 'right', true, function () { return $scope.myForm.$invalid; });
            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            showHideValidRefundTypes();

        }

        function showHideValidRefundTypes(){

            merchantConfig.getMerchantConfig().then(function(config){

                refundTypesValue.forEach(function(refundType){
                	
                	
                	
                	config	= utilsService.EnableCtrip(config)

                    refundType.visible = refundType.refundCodes.some(function(code){

                        return config.RefundOptions.some(function(refundOption){

                            return refundOption.RefundCode === code;
                        });

                    });

                    if(navigationService.offline()){

                        refundType.disabled = refundType.disableWhenOffline;
                    } else {

                        refundType.disabled  = false;
                    }

                });

                vm.refundTypes = refundTypesValue;

            });
        }

        function next() {

            //always wipe refund option, in case it's changed (don't want old values)
            vm.voucher.RefundOption = {};


            var selectedRefundType = refundTypesValue.filter(function(refundType){

                return refundType.id === vm.selectedRefundTypeId;
            })[0];

            //there are multiple possible refund codes per type for some refunds
            if(selectedRefundType.refundCodes.length === 1){

                vm.voucher.RefundOption.RefundOptionCode = selectedRefundType.refundCodes[0];
                //vm.voucher.RefundOption.RefundTypeDesc=selectedRefundType.translationKey;
                //add refund options as well in receipts 
                vm.voucher.Invoices.forEach(function(invoice){

                	invoice.RefundTypeDesc= selectedRefundType.translationKey; 

                });
                
                
                
            }
            //if there are multiple possible refundOptionCodes, the next screen should allow the user to pick

            if (selectedRefundType.dialog === 'refundConfirmAddressDialog') {
                openConfirmAddressDialog();
            } else if (selectedRefundType.dialog) {
                //generic dialog
                openDialog(selectedRefundType.dialog);
            } else {
                navigationService.pageValidityChanged(true, undefined, selectedRefundType.navLocation, true);
                navigationService.gotoNextState();
            }
        }


        function openDialog(name) {

            openRefundDialog(name).closePromise.then(function (data) {
                if (angular.isDefined(data.value) && data.value !== '$document') {

                    navigationService.pageValidityChanged(true, undefined, data.value);
                    navigationService.gotoNextState();
                }
            });
        }

        function openConfirmAddressDialog() {
            //18/05/2015 - Just checked with Hem and he'd like us to go to a full screen address editor if we chose to pay to alternative address.

            merchantConfig.getMerchantConfig().then(function (mc) {

                openRefundDialog('refundConfirmAddressDialog', mc).closePromise.then(function (data) {

                    if (angular.isDefined(data.value) && data.value !== '$document') {
                        vm.voucher.RefundOption.RefundOptionCode = vm.voucher.RefundOption._RefundOptionCode;
                        delete vm.voucher.RefundOption._RefundOptionCode;
                        navigationService.pageValidityChanged(true, undefined, data.value, true);
                        navigationService.gotoNextState();
                    }
                });

            });
        }


        function openRefundDialog(name, mc) {

            return ngDialog.open({
                template: 'app/refund/templates/' + name + '.html',
                className: 'ngdialog-theme-plain',
                showClose: false,
                //because the animations are screwy on windows
                disableAnimation: utilsService.isOnWindowsPlatform(),
                data: {
                    'voucher': vm.voucher,
                    'model':vm,
                    'merchantConfigData': mc
                }
            });
        }
        
        
        
        
        
        
        function premierPassScan(){

            if(utilsService.isOnWindowsPlatform()){
              cordova.plugins.barcodeScanner.scan(stoppedScanning,
                   function(){
                     stoppedScanning(false);
                   }
                );
            } else {
      if(device)
    	  {
if (device.platform=="Android") {
  cordova.plugins.barcodeScanner.scan(stoppedScanning,
       function(){
         stoppedScanning(false);
       }
    );
	}
else {
	manateeBarcode.startScanning(stoppedScanning);
	}
    	  }

            }

        }

        function stoppedScanning(result){

            if(result === false){
                translatedToastr.warning('TOAST.NO_CORDOVA', 'TOAST.ERROR', {preventDuplicates:false});
            } else if(result && result.type !== 'Cancel' && !result.cancelled){
                if(result.code || result.text){

                    var code = result.code || result.text;
                    var premierPassId = code.split('|')[0];
                    vm.premierPassNumber = premierPassId;
                    searchForTourist(premierPassId);

                } else {
                    $log.error('Unable to open barcode scanner');
                    translatedToastr.warning('PREMIER_PASS.SCAN_FAILED', 'TOAST.ERROR', { preventDuplicates:false });
                }
            }

        }

        function searchForTourist(premierPassId){

            connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(online){

                if(online){

                    fintraxApi.getTourist(premierPassId).then(getTouristResponse, getTouristError);
                } else {

                    translatedToastr.error('TOAST.CANT_SCAN_OFFLINE');
                    navigationService.endWorkflow();
                    $state.go('navigation.home');
                }
            });

        }

        function getTouristResponse(response){
            //newVoucher Response appending
        	newVoucherValue.Tourist = response;
           	
        	
                 if (newVoucherValue.Tourist.PremierPassId) {

                     navigationService.pageValidityChanged(true, undefined, 'navigation.refundReview.purchaseDetails');
                     translatedToastr.success('PREMIER_PASS.TOURIST_FOUND');
                 } 

                 navigationService.gotoNextState();
             
        	
            
        }

        function getTouristError(error) {
            console.error('TOURIST_ERROR',error);
            translatedToastr.error('PREMIER_PASS.TOURIST_NOT_FOUND', 'TOAST.WARNING', { preventDuplicates:false });
        }


        

    }
})();
