﻿(function () {
    'use strict';
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('editReceiptListController', editReceiptListController);

    function editReceiptListController(navigationService, commandService, newVoucherValue,
                                       advancedReceiptValue, $scope, $state, $stateParams, $rootScope) {
        /* jshint validthis: true */
        var vm = this;
        //When coming back into this from the advanced receipts screen, we should prefer the advancedReceiptValue collection. 
        vm.receipts = $stateParams.useAdvancedReceiptValue ? advancedReceiptValue.receipts : angular.copy(newVoucherValue.Invoices);
        vm.isReadonly = false;
        vm.recalculateRefund = recalculateRefund;

        //this is so we can pass a voucher to the refund calculator, without effecting the original voucher
        var tempVoucher = angular.copy(newVoucherValue);

        activate();

        function activate() {

            navigationService.setTitle('RECEIPTS.PURCHASE_DETAILS', 3);
            navigationService.pageClass('receiptList');

            commandService.addButton('COMMANDS.CANCEL', cancel, 'left', true);
            commandService.addButton('COMMANDS.CONFIRM', confirm, 'right', true, function () {
                return !$scope.vm.isValid || tempVoucher.RefundAmount <= 0;
            });

            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            commandService.vipSelectVisible(true);

            recalculateRefund();

        }

        function cancel() {
            $state.go('navigation.refundReview.purchaseDetails');
        }

        function confirm() {
            newVoucherValue.Invoices = vm.receipts;
            $rootScope.$broadcast('RECALCULATE_REFUND', {});
            $state.go('navigation.refundReview.purchaseDetails');
        }

        function recalculateRefund(){
            tempVoucher.Invoices = vm.receipts;
            $rootScope.$broadcast('RECALCULATE_REFUND', { voucher: tempVoucher });
        }

    }
})();

