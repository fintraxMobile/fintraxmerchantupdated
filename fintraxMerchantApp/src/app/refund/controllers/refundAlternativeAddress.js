(function () {
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('refundAlternativeAddressController', refundAlternativeAddressController);

    function refundAlternativeAddressController($scope, navigationService, newVoucherValue, fintraxApi) {
        /* jshint validthis: true */
        var vm = this;

        newVoucherValue.Tourist = newVoucherValue.Tourist || {};
        newVoucherValue.Tourist.AddressAdditional = newVoucherValue.Tourist.AddressAdditional || {};

        vm.Tourist = newVoucherValue.Tourist;
        vm.countries = [];

        navigationService.setTitle('NEWCUSTOMER.CUSTOMER_DETAILS_TITLE', 3);

        $scope.$watch('vm.isValid', function (newval, oldval) {
            navigationService.pageValidityChanged(newval, oldval, 'navigation.refundReview.purchaseDetails');
        });

        fintraxApi.getCountries().then(function(countries){
            vm.countries = countries;
        });
    }
})();
