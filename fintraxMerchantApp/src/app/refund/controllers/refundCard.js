(function () {
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('refundCardController', refundCardController);

    function refundCardController(navigationService, commandService, newVoucherValue, $scope, translatedToastr,
        $window, creditCardValidationService, $stateParams, dateDropdownConstants, merchantConfig, $log) {
        /* jshint validthis: true */
        var vm = this;
        vm.voucher = newVoucherValue;
        vm.scanCreditCard = scanCreditCard;
        vm.isTopos = $stateParams.isTopos;
        vm.dateDropdowns = dateDropdownConstants;
        vm.years = getYearsDropdownOptions();
        setupRefundOptionsIfNeeded(vm);
        vm.maskedCard = maskedCard;
        vm.cardScanEnabled = false;

        vm.setExpireyDate = setExpireyDate;

        activate();

        function activate(){

            navigationService.pageClass('refundTypes');
            navigationService.setTitle('REFUND.CARD_DETAILS_TITLE', 3);

            commandService.addButton('COMMANDS.NEXT', next, 'right', true, function () { return $scope.myForm.$invalid; });
            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            $scope.$watch('vm.voucher.RefundOption.CardDetails.CardNumber', function (cardNumber, oldValue) {
                if (angular.isDefined(cardNumber)) {
                    var result = creditCardValidationService.validateCard(cardNumber);

                    //Prevent user entering too many digits
                    //var isnum = /^\d+$/.test(cardNumber); Now done with ng-pattern. pattern attr helps get numeric keyboard ios

                    if (result.isTooLong) {
                        vm.voucher.RefundOption.CardDetails.CardNumber = oldValue;
                        result = creditCardValidationService.validateCard(oldValue);
                    }

                    vm.cardType = result.cardType;
                    vm.lengthDesc = result.lengthDesc;

                    if(!vm.voucher._maskCardNumber) {

                        //cardNumber field doesn't exist if the number is masked
                        $scope.myForm.cardNumber.$setValidity('unrecognisedCardType', !!result.cardType);
                        $scope.myForm.cardNumber.$setValidity('cardLength', result.validLength);
                        $scope.myForm.cardNumber.$setValidity('luhnCheck', result.isValidLuhn);

                    }

                }
            });

            //disable card scan if it's not available
            if ($window.cordova) {

              if($window.CardIO){
                $window.CardIO.canScan(function(ableToScan){

                    vm.cardScanEnabled = ableToScan;
                });
              } else {

                vm.cardScanEnabled = false;
              }

            } //else {

              //show it when debugging locally
              //vm.cardScanEnabled = true;
            //}
        }

        function maskedCard(){
            var cardNumber = vm.voucher.RefundOption.CardDetails ? vm.voucher.RefundOption.CardDetails.CardNumber : '';
            var maskLength = cardNumber.length - 4;
            var masked = '';

            for(var i = 0; i<= maskLength; i++){
                masked += '*';
            }

            return masked + cardNumber.slice(-4);
        }

        function setExpireyDate() {

            if (angular.isDefined(vm.voucher.RefundOption) && angular.isDefined(vm.voucher.RefundOption.CardDetails)) {

                var month = vm.voucher.RefundOption.CardDetails.CardExpiryDateMonth + 1; // needed for javascript 0 indexed date
                var year  = vm.voucher.RefundOption.CardDetails.CardExpiryDateYear;

                if ( month < 10 ) {
                    month = '0' + month;
                }

                if (angular.isDefined(month) && angular.isDefined(year) && angular.isDefined($scope.myForm.month)) {

                    var now        = new Date();
                    var monthsDiff = month - now.getMonth();
                    var yearsDiff  = year - now.getFullYear();

                    $scope.myForm.month.$setValidity('expiresWithin3Months', monthsDiff + (12 * yearsDiff) >= 3);

                    vm.voucher.RefundOption.CardDetails.CardExpiryDate = month + '/' + String( year ).substring( 2 );
                }
            }
        }


        function getYearsDropdownOptions() {
            var years = [];
            var year = (new Date()).getFullYear() + 10;
            for (var y = year; y >= year - 10; y--) {
                years.push(y);
            }
            return years;
        }


        var gotSixhundredCodes = false;
        function setupRefundOptionsIfNeeded(vm) {

            merchantConfig.getMerchantConfig().then(function (mc) {

                vm.allPossibleRefundOptions =  mc.RefundOptions;

                if ($stateParams.isTopos) {

                    var ambiguousRefundOptions = ['0501', '0502', '0503', '0504', '0505', '0506', '0507', '0508', '0509',
                        '0601', '0602', '0603', '0604', '0605', '0702', '0703'];
                    vm.showRefundOptionsDropdown = false;

                    vm.refundOptions = mc.RefundOptions.filter(function (ro) {
                        return ambiguousRefundOptions.indexOf(ro.RefundCode) !== -1;
                    });

                    var gotFivehundredCodes = vm.refundOptions.filter(function (ro) { return ro.RefundCode.indexOf('050') === 0; }).length > 0;
                    gotSixhundredCodes = vm.refundOptions.filter(function (ro) { return ro.RefundCode.indexOf('060') === 0; }).length > 0;
                    var gotSevenhundredCodes = vm.refundOptions.filter(function (ro) { return ro.RefundCode.indexOf('070') === 0; }).length > 0;

                    //The rules: if we have an 070X code or a 050X code then we need to show refund option dropdown
                    //If we have only 060X codes then we can get the refund option from the card type
                    vm.showRefundOptionsDropdown = gotSevenhundredCodes || gotFivehundredCodes; // && gotSixhundredCodes);

                }
            });

        }

        function next() {

            if ($stateParams.isTopos && !vm.showRefundOptionsDropdown) {

                if(gotSixhundredCodes) {
                    vm.voucher.RefundOption.RefundOptionCode = vm.cardType.refundCode.substring(0, 1) + '6' + vm.cardType.refundCode.substring(2);
                }

            } else if(!$stateParams.isTopos) {

                //Assign the more specific refund option code according to the card type. (Why the server can't do this...?)
                vm.voucher.RefundOption.RefundOptionCode = vm.cardType.refundCode;
            }

            //check to see if the selected refund option code is available in the merchant config.
            //see PEA-736

            var matchingRefund = vm.allPossibleRefundOptions.filter(function (option) {
                return option.RefundCode === vm.voucher.RefundOption.RefundOptionCode;
            });

            if(matchingRefund.length === 0){

                translatedToastr.error('TOAST.NO_REFUND_AVAILABLE_FOR_CARD');

            } else {

                vm.voucher._maskCardNumber = true;

                navigationService.pageValidityChanged(true, undefined, 'navigation.refundReview.purchaseDetails');
                navigationService.gotoNextState();
            }

        }

        function scanCreditCard() {
            vm.status = '';
            if ($window.cordova) {

                $window.CardIO.canScan(function(ableToScan){

                    if(ableToScan){

                        $window.CardIO.scan({
                            'expiry': vm.isTopos,
                            'cvv': false,
                            'zip': false,
                            'suppressManual': true,
                            'suppressConfirm': false,
                            'hideLogo': true
                        }, cardIOSuccess, cardIOCancel);

                    } else {

                        cardIOCancel();
                    }
                });


            } else {
                translatedToastr.warning('TOAST.NO_CORDOVA', 'TOAST.ERROR');
            }
        }


        function cardIOSuccess(cardInformation) {

            vm.cardDetails = cardInformation;
            vm.voucher.RefundOption.CardDetails = vm.voucher.RefundOption.CardDetails || {};
            vm.voucher.RefundOption.CardDetails.CardNumber = cardInformation['card_number'];

            if(vm.isTopos) {

                //-1 because js dates are 0 indexed, and the return from cardio starts at 1!
                vm.voucher.RefundOption.CardDetails.CardExpiryDateMonth = cardInformation['expiry_month'] - 1;
                vm.voucher.RefundOption.CardDetails.CardExpiryDateYear = cardInformation['expiry_year'];
            }

            translatedToastr.success('TOAST.CREDIT_CARD_SCAN_SUCCESS');

            var result = creditCardValidationService.validateCard(cardInformation.cardNumber);

            vm.cardType = result.cardType;
            vm.lengthDesc = result.lengthDesc;

            next();
        }

        function cardIOCancel(errors) {

            $log.error(errors);
            translatedToastr.warning('TOAST.CREDIT_CARD_SCAN_ERROR', 'TOAST.WARNING');
        }
        
        
                

    }
})();
