(function () {
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('refundReviewShopperDetailsController', controller);

    function controller(newVoucherValue, navigationService, $stateParams) {
        /* jshint validthis: true */
        var vm = this;

        activate();

        function activate(){

            if ($stateParams.reportsVoucher) {

                navigationService.pageClass('refundReview reports-voucher-details');
            } else {

                navigationService.pageClass('refundReview');
            }

            vm.voucher = newVoucherValue;

        }

    }

})();