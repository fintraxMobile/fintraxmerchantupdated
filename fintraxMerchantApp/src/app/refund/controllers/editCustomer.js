﻿(function () {
    'use strict';
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('editCustomerController', editCustomerController);

    function editCustomerController(navigationService, commandService,
                                    newVoucherValue, $scope, $state,
                                    $stateParams, fintraxApi) {

        /* jshint validthis: true */
        var vm = this;
        vm.countries = [];

        activate();

        function activate() {
            vm.Tourist = angular.copy(newVoucherValue.Tourist);
            vm.Tourist.AddressAdditional = vm.Tourist.AddressAdditional || {};

            navigationService.setTitle($stateParams.title, $stateParams.sectionNumber);

            commandService.addSvgButton('back', back, 'left', true);
            commandService.addButton('COMMANDS.CONFIRM', confirm, 'right', true, function () {
                return !$scope.vm.isValid;
            });

            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            fintraxApi.getCountries().then(function(countries){
                vm.countries = countries;
            });
        }

        function back() {
            $state.go($stateParams.returnToState);
        }

        function confirm() {
            newVoucherValue.Tourist = vm.Tourist;
            $state.go($stateParams.returnToState);
        }


    }
})();
