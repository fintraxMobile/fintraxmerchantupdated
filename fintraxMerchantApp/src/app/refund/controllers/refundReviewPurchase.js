(function () {
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('refundReviewPurchaseController', refundReviewPurchaseController);

    function refundReviewPurchaseController(newVoucherValue, $rootScope, navigationService, commandService,
                                            $scope, $state, $stateParams) {
        /* jshint validthis: true */
        var vm = this;

        vm.receipts = newVoucherValue.Invoices;
        vm.isReport = false;
        vm.RefundTypeDesc=newVoucherValue.RefundOption.RefundTypeDesc

        activate();

        function activate() {
        	commandService.changeTotalCssClass(true);
        	commandService.showTotalAmount(true);

            if ($stateParams.reportsVoucher) {

                navigationService.pageClass('receiptList reports-voucher-details refund-review');
                vm.isReport = true;
                
                commandService.changeTotalCssClass(false);
                commandService.showTotalAmount(false);
               
            } else {

                navigationService.pageClass('receiptList refund-review');
                commandService.addButton('COMMANDS.EDIT', edit, 'right');
                $scope.$on('$destroy', removeEditButton);

                //because we need to update the UI if a user cancels out of an edit
                $rootScope.$broadcast('RECALCULATE_REFUND', { });
            }

        }

       
        
        function edit() {
            $state.go('navigation.editReceiptList');
        }

        function removeEditButton(){
            commandService.removeButtonThatHasText('COMMANDS.EDIT');
        }

    }
})();