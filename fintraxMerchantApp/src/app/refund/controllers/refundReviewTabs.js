(function () {
    angular
        .module('merchantApp.createVoucher.refund')
        .controller('refundReviewTabsController', refundReviewTabsController);


    function refundReviewTabsController(navigationService, $state, $scope, $stateParams, commandService,
                                        newVoucherValue, offlineQueue, fintraxApi, merchantConfig,
                                        translatedToastr, voucherStatusConstants, connectivityStatusService,
                                        pdfWrangler, printService, createVoucherService, createVoucherOfflineService,
                                        voucherErrorHandler, $translate, $window, utilsService, $rootScope) {
        /* jshint validthis: true */
        var vm = this;
        var currentMerchantConfig = {};

        activate();

        function activate() {

            connectivityStatusService.checkOnlineIfOnlineSetNav();

            $scope.$on('$destroy', commandService.restoreDefaultButtons);

            vm.purchaseDetailsSelected = $state.current.name === 'navigation.refundReview.purchaseDetails';

            if ($stateParams.reportsVoucher) {

                navigationService.setTitle('REPORTS.VOUCHER_DETAILS_TITLE');
                navigationService.pageClass('reports-voucher-details');

                commandService.removeAllButtons();
                commandService.addSvgButton('back', backToReports, 'left');


                var voucherStatus = voucherStatusConstants.filter(function (vsc)
                {
                    return vsc.statusId === newVoucherValue.VoucherStatus;
                })[0];

                if (voucherStatus.translationKey !== 'VOUCHER_STATUS.REFUNDED_CASH' &&
                    voucherStatus.translationKey !== 'VOUCHER_STATUS.REFUNDED_NON_CASH' &&
                    voucherStatus.translationKey !== 'VOUCHER_STATUS.VOIDED'
                ) {
                    commandService.addFooterButton('COMMANDS.VOID_VOUCHER', voidVoucher, commandService.BUTTON_TYPE.tertiary);
                    commandService.addButton('COMMANDS.REPRINT', reprint, 'right');
                }

                vm.tab1URL = 'navigation.voucherReview.purchaseDetails';
                vm.tab2URL = 'navigation.voucherReview.shopperDetails';

                merchantConfig.getMerchantConfig().then(function(conf){
                    currentMerchantConfig = conf;
                    setRefundAmountInFooter();
                });

            } else {

                navigationService.setTitle('REFUND.REFUND_REVIEW_TITLE', 4);
                commandService.addButton('COMMANDS.CONFIRM', confirm, 'right', true, isVoucherInvalid);
                commandService.vipSelectVisible(true);

                vm.tab1URL = 'navigation.refundReview.purchaseDetails';
                vm.tab2URL = 'navigation.refundReview.shopperDetails';
            }
        }

        function isVoucherInvalid(){
            return newVoucherValue.RefundAmount <= 0;
        }

        function setRefundAmountInFooter(){

            var footerContent = {
                    refund:
                        {
                            totalRefund: newVoucherValue.RefundAmount,
                            voucherDate:new Date(newVoucherValue.VoucherDateTime),
                            currencySymbol:currentMerchantConfig.Store.CurrencySymbol
                        }
                    };

            navigationService.footerInfo(footerContent);
        }

        function backToReports(){
            $state.go('navigation.reportcustom');
        }

        function reprint() {

            $rootScope.loading = true;

            pdfWrangler.generateVoucherPdf(currentMerchantConfig, newVoucherValue).then(function(rendered){

                var printResult = false;

                if(utilsService.isOnWindowsPlatform()){


                  printService.pdfToDomElements(rendered.dataUrl).then(function (domElement) {

                        var fragment = document.createDocumentFragment();
                        fragment.appendChild(domElement);

                        $rootScope.loading = false;
                        printResult = printService.print(fragment);

                        if (!printResult) {
                            translatedToastr.error('TOAST.UNABLE_TO_PRINT');
                        }

                  });

                } else {

                    $rootScope.loading = false;
                    printResult = printService.print(rendered.dataUrl);

                      if (!printResult) {
                          translatedToastr.error('TOAST.UNABLE_TO_PRINT');
                      }
                }

            });
        }


        function voidVoucher() {

            $translate(['REPORTS.CONFIRM_VOID', 'COMMANDS.CONFIRM']).then(function(translations){

                if($window.cordova){

                    $window.navigator.notification.confirm(translations['REPORTS.CONFIRM_VOID'], function(buttonClickedPosition){

                        //1 for confirmed, 2 for cancelled.

                        if(buttonClickedPosition === 1){
                            connectivityStatusService.isOnline().then(doVoidVoucher);
                        }

                    }, translations['COMMANDS.CONFIRM']);

                } else {
                    if($window.confirm(translations['REPORTS.CONFIRM_VOID'])){

                        connectivityStatusService.isOnline().then(doVoidVoucher);
                    }
                }

            });

        }

        function confirm() {

            if(navigationService.offline()) {

                proceed();
            } else {

                connectivityStatusService.checkOnlineIfOnlineSetNav().then(function(){

                    proceed();
                });
            }

        }

        function proceed(){
            //If the setting EnableTouristSignature then get customer signature, else if EnableMerchantSignature then
            //get merchant sig, else go directly to printvoucher.
            merchantConfig.getSetting('EnableTouristSignature').then(function (valEnableTouristSignature) {
                if (valEnableTouristSignature) {

                    navigationService.pageValidityChanged(true, undefined, 'navigation.customersignature');
                    navigationService.gotoNextState();
                } else {

                    merchantConfig.getSetting('EnableMerchantSignature').then(function (valEnableMerchantSignature) {

                        if (valEnableMerchantSignature) {

                            navigationService.pageValidityChanged(true, undefined, 'navigation.merchantsignature');
                            navigationService.gotoNextState();

                        } else {

                            //submit voucher as per customerSignature Controller
                            attemptVoucherCreation().then(createVoucherSuccess).catch(createVoucherFailure);
                        }

                    });
                }
            });
        }


        function doVoidVoucher(online) {
            console.log('Voiding voucher. Online: ' + online);

            if (offlineQueue.queueLength() > 0 || !online) {

                offlineQueue.enqueueVoidVoucher(newVoucherValue.VoucherBarcode);
            } else {

                fintraxApi.voidVoucher(newVoucherValue.VoucherBarcode).then(function () {
                    translatedToastr.success('API.VOUCHER_VOIDED_SUCCESS');
                }, function (err) {
                    console.log(err);
                    translatedToastr.error('API.UNABLE_TO_VOID_VOUCHER');
                });
            }
        }


        function createVoucherSuccess(result) {

            if(result.online){

                translatedToastr.success('TOAST.CREATE_VOUCHER_SUCCESS');
            } else {

                //if we're not online, it must have been queued
                translatedToastr.warning('TOAST.VOUCHER_QUEUED', null, { preventDuplicates:false });
            }

            //ending the workflow here prevents weird things happening after you change tabs
            navigationService.endWorkflow();
            $state.go('navigation.printvoucher');
        }

        function createVoucherFailure(result) {

            voucherErrorHandler.attemptToParseError(result);
        }

        function attemptVoucherCreation(){

            //means we can manipulate the voucher value in the createVoucher service without effecting the original
            var voucherClone = angular.copy(newVoucherValue);

            if(navigationService.offline()){

                //otherwise the previous successful online voucher creation result will be printed
                createVoucherService.clearVoucherCreationResult();

                //act on the original voucher object
                return createVoucherOfflineService.createVoucherOffline(newVoucherValue);
            } else {

                return createVoucherService.createVoucherOnline(voucherClone).then(function(result){

                    result.online = true;
                    return result;
                });
            }

        }
    }
})();
