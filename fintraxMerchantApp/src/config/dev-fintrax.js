//dev configuration, with fintrax hockey app ids
//this matches the "Premier Mobile (test)" apps in hockey

window.config = {} || window.config;

window.config = {
    auth0Merchant: {
        domain: 'fintrax-test.auth0.com',
        clientId: '67GMF33se7UupPDgvu4bsgW6pldruNQk',
        connection:'Merchants'
    },
    auth0Tourist: {
        domain: 'fintrax-test.auth0.com',
        clientId: '67GMF33se7UupPDgvu4bsgW6pldruNQk',
        connection:'Tourists'
    },
    fintraxApi: {
        url: 'https://apidev.fintrax.com:4856/api/'
    },
    hockeyApp: {
        iOS: 'd64f4fab613f48bbb0c8a9dc24434bb3',
        android: '01a952c8d14b4392a0a1a2f6a6d68aec',
        windows: ''
    }
};
