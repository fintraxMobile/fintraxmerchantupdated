//live configuration, with fintrax hockey app ids
//this matches the "Premier Mobile (preprod)" apps in hockey

window.config = {} || window.config;

window.config = {
    auth0Merchant: {
        domain: 'fintrax.auth0.com',
        clientId: 'qzrZqh3GGWtI70msMCNHxKSUvgebaEnZ',
        connection:'Merchants'
    },
    auth0Tourist: {
        domain: 'fintrax.auth0.com',
        clientId: 'qzrZqh3GGWtI70msMCNHxKSUvgebaEnZ',
        connection:'Tourists'
    },
    fintraxApi: {
        url: 'https://api.fintrax.com/api/'
    },
    hockeyApp: {
        iOS: '4aaaf08b66a349a1aff5eb557addf085',
        android: '7c60105f2f674962b1c722c5b7d89952',
        windows: ''
    },
    googleAnalytics: {
        key: 'UA-57875734-4'
    }
};
