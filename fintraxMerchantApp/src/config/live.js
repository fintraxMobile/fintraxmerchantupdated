window.config = {} || window.config;

window.config = {
    auth0Merchant: {
        domain: 'fintrax.auth0.com',
        clientId: '73CMFHGTcR4ZBIGkv5Qq5B2xgzZengrB',
        connection:'Merchants',
        audience:'https://api.fintrax.com'
    },
    auth0Tourist: {
        domain: 'fintrax.auth0.com',
        clientId: '73CMFHGTcR4ZBIGkv5Qq5B2xgzZengrB',
        connection:'Tourists'
    },
    fintraxApi: {
        //url: 'https://api.fintrax.com/api/'
        url: 'https://api.fintrax.com/v2/api/'
    },
    hockeyApp: {
        iOS: '7aab88f8f75e49dd8d945783afb9f01e',
        android: '84aa238211f041d292fad4ca913d7ce2',
        windows: ''
    },
    googleAnalytics: {
        key: 'UA-57875734-4'
    }
};
