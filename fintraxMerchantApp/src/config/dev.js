window.config = {} || window.config;

	window.config = {
    auth0Merchant: {
        domain: 'fintrax-test.auth0.com',
        clientId: 'gffHPs7hDqLF2krTkUC8s5ovkBuVP9pT',
        connection:'Merchants',
        audience:'https://apitest.fintrax.com'
    },
    auth0Tourist: {
        domain: 'fintrax-test.auth0.com',
        clientId: 'gffHPs7hDqLF2krTkUC8s5ovkBuVP9pT',
        connection:'Tourists'
    },
    fintraxApi: {
      //  url: 'https://apidev.fintrax.com:4856/api/'
    	url:'https://apidev.fintrax.com:4856/v2/api/'
    },
    hockeyApp: {
        iOS: 'c1fb328d49f59961c1fe7106f956ed54',
        android: '788eb4ee7ae14680c9d8b8909c5ef836',
        windows: ''
    }
};
