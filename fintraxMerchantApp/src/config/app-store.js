//live configuration, no hockeyapp

window.config = {} || window.config;

window.config = {
    auth0Merchant: {
        domain: 'fintrax.auth0.com',
        clientId: 'qzrZqh3GGWtI70msMCNHxKSUvgebaEnZ',
        connection:'Merchants'
    },
    auth0Tourist: {
        domain: 'fintrax.auth0.com',
        clientId: 'qzrZqh3GGWtI70msMCNHxKSUvgebaEnZ',
        connection:'Tourists'
    },
    fintraxApi: {
        url: 'https://api.fintrax.com/api/'
    },
    googleAnalytics: {
        key: 'UA-57875734-4'
    }
};
