//live configuration, with fintrax hockey app ids
//this matches the "Premier Mobile (preprod)" apps in hockey

window.config = {} || window.config;

window.config = {
    auth0Merchant: {
        domain: 'fintrax.auth0.com',
        clientId: 'qzrZqh3GGWtI70msMCNHxKSUvgebaEnZ',
        connection:'Merchants'
    },
    auth0Tourist: {
        domain: 'fintrax.auth0.com',
        clientId: 'qzrZqh3GGWtI70msMCNHxKSUvgebaEnZ',
        connection:'Tourists'
    },
    fintraxApi: {
        url: 'https://api.fintrax.com/api/'
    },
    hockeyApp: {
        iOS: '436f1b088b084ca8bd1fb1bb994c6b87',
        android: '',
        windows: ''
    },
    googleAnalytics: {
        key: 'UA-57875734-4'
    }
};
