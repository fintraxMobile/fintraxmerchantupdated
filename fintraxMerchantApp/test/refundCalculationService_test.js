'use strict';

describe('refund calculation service test', function(){

    describe('calculate refund, invalid codes, invalid amounts', function(){

        beforeEach(module('merchantApp.core'));

        var merchant = JSON.parse(window.__html__['test/fixtures/merchant_1.json']);

        it('returns INVALID_REFUND_CODE status, if refund code provided, but no match is found', inject(function(refundCalculationService){

            var voucher = JSON.parse(window.__html__['test/fixtures/voucher_1.json']);
            voucher.RefundOption.RefundOptionCode = 'bananas';

            var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

            expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.INVALID_REFUND_CODE);
            expect(calculationResult.totalRefund).toEqual(0);
        }));


        it('returns BELOW_TOTAL_THRESHOLD status, if below threshold amount', inject(function(refundCalculationService){

            var voucher = JSON.parse(window.__html__['test/fixtures/voucher_below_threshold.json']);
            var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

            expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.BELOW_TOTAL_THRESHOLD);
            expect(calculationResult.totalRefund).toEqual(0);
        }));


        it('returns BELOW_DAY_THRESHOLD status, if below threshold amount, and verify by day', inject(function(refundCalculationService){

            var merchant = JSON.parse(window.__html__['test/fixtures/merchant_1.json']);
            var voucher = JSON.parse(window.__html__['test/fixtures/voucher_below_threshold.json']);

            merchant.Store.VerifyThresholdByDay = true;
            var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

            expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.BELOW_DAY_THRESHOLD);
            expect(calculationResult.totalRefund).toEqual(0);
        }));

        it('returns BELOW_DAY_THRESHOLD status, if below threshold amount, and verify by day, for multiple day invoices',
            inject(function(refundCalculationService){

            var merchant = JSON.parse(window.__html__['test/fixtures/merchant_1.json']);
            var voucher = JSON.parse(window.__html__['test/fixtures/voucher_multiple_invoice_dates_below_threshold.json']);

            merchant.Store.VerifyThresholdByDay = true;
            var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

            expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.BELOW_DAY_THRESHOLD);
            expect(calculationResult.totalRefund).toEqual(0);
        }));

    });


    describe('calculate successful refund', function(){

        beforeEach(module('merchantApp.core'));

        it('returns success',
            inject(function(refundCalculationService){

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_1.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_1.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund).toEqual(144);
        }));


    });


    describe('calculate successful refund, simple3 merchant', function(){

        beforeEach(module('merchantApp.core'));

        //test #1 from bill
        it('[TEST #1] returns success for simple3, one invoice for today, two line items',
            inject(function(refundCalculationService){

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test1.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('7.21');
            }));



        /*

         Three invoices
         for today, yesterday and 124 days ago

         Invoice 1, 1 line item - yesterday
         Acessorios x 1   EUR 61.37  (should be included)

         Invoice 2, 1 line item - 124 days ago
         Discos x 1	EUR 61.36  (should be included)

         Invoice 3, 1 line item - today

         Discos x 2	EUR 20.00 total (should be included)
         Total Refund EUR 16.77
         Card

        */
        //test #2 from bill
        it('[TEST #2] returns success for simple3, one invoice for today, some for prior days',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test2.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('16.77');
            })
        );


        /*

         Two invoices
         for today, yesterday
         Invoice 1, 1 line item - yesterday
         Livros x 1   EUR 52.89  (should be included)
         Invoice 2, 1 line item -today
         Livros x 1	EUR 20  (should be included)
         Total Refund EUR 1.82
         Airport Cash

        */

        //test #3 from bill
        it('[TEST #3] returns success for simple3, one under threshold invoice for today, one over for previous days',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test3.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('1.82');
            })
        );

        /*
         Two invoices
         for yesterday and day before
         Invoice 1, 1 line item - yesterday
         ACESSORIOS x 1   EUR 61.37  (should be included)
         Invoice 2, 1 line item -day before yesterday
         DISCOS x 1	EUR 52.88  (shouldn't be included)
         Total Refund EUR 7.21
         Airport Cash
        */

        //test #4 from bill
        it('[TEST #4] returns success for simple3, one over threshold invoice for today, one under for previous days (airport cash)',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test4.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('7.21');
            })
        );

        /*

         Two invoices
         for today and yesterday
         Invoice 1, 1 line item - today
         ACESSORIOS x 1   EUR 61.37  (should be included)
         Invoice 2, 1 line item -  yesterday
         ACESSORIOS x 1	EUR 20  (shouldn't be included)
         Total Refund EUR 7.21
         Print

        */

        //test #5 from bill
        it('[TEST #5] returns success for simple3, one over threshold invoice for today, one under for previous days (print)',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test5.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('7.21');
            })
        );


        /*
         VIP 12.25% (but shouldn't be applied as under 250.01 for VIP 12.25% rate)
         One invoice
         for today
         Invoice 1, 1 line item - today
         ACESSORIOS x 1   EUR 250 (12.25% shouldn't be applied)
         Total Refund EUR 0.00
         Shouldn't be allowed (hasn't reached 250.01 threshold for VIP)
        */

        //test #6 from bill
        it('[TEST #6] returns success for simple3, VIP 12.25, under 250.01 threshold for VIP',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test6.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.NO_MATCHING_CHARGE_STRUCTURE);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('0.00');
            })
        );



        /*
         No VIP (should apply default charge structure rate)
         One invoice
         for today
         Invoice 1, 1 line item - today
         ACESSORIOS x 1   EUR 250 (11.75% should be applied)
         Total Refund EUR 28.75
         */

        //test #6a from bill
        it('[TEST #6a] returns success for simple3, no VIP',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test6a.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('29.38');
            })
        );

        /*
         VIP 12.25%
         One invoice
         for today
         Invoice 1, 1 line item - today
         ACESSORIOS x 1   EUR 250.01 (12.25% should be applied)

         Total Refund EUR 30.63
         uses 12.25% vip charge structure

        */

        //test #7 from bill
        it('[TEST #7] returns success for simple3, uses 12.25 VIP charge structure',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test7.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('30.63');
            })
        );

        /*
         No Vip
         One invoice
         for today
         Invoice 1, 1 line item - today
         ACESSORIOS x 1   EUR 250.01 (11.75% should be applied)
         Total Refund EUR 29.38
         uses default charge structure

        */

        //test #7a from bill
        it('[TEST #7a] returns success for simple3, no VIP, over threshold',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple3.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test7a.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('29.38');
            })
        );

        /*
         Two invoices
         Invoice 1, 1 line item £175.00 (use only ONE of the following invoices)
         4235234541415
         4235234541416
         4235234541417
         4235234541418
         4235234541419
         Invoice 2, 1 line item £11,500.00 (use only ONE of the following invoices)
         4235234541409
         4235234541410
         4235234541411
         4235234541412
         4235234541413
         4235234541414

         Price Inc VAT = £11675.00
         Price Exc VAT = £9729.16
         VAT = £1945.84
         Admin Fee = £300
         Total Refund = £1645.84
        */

        //test #7 from bill
        it('[TEST #8] returns success for account1, no VIP, advanced1 workflow, FeeType Fixed / Max',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 10);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_account1.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test8.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('1645.83');
            })
        );


        //test covering issue in https://fintraxitqatest.atlassian.net/browse/PEA-640
        it('[TEST #9] returns success for simple1, multiple invoices, checking the rounding',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 18);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_simple1.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test9.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('1.50');
            })
        );

        //test covering issue in https://fintraxitqatest.atlassian.net/browse/PEA-762
        it('[TEST #10] returns success for account2, multiple default charge structures, Amex TOPOS alternative charge structure',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 20);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_account2.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test10.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('576.00');
            })
        );

        //covers https://fintraxitqatest.atlassian.net/browse/PEA-752
        it('[TEST #11] advanced1, TOPOS refund fees for diners card',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2015, 10, 20);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_advanced1.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_test11.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('136.50');
            })
        );

        //covers https://fintraxitqatest.atlassian.net/browse/PEA-1148
        it('[TEST #12] Benetton refund returns expected refund amount',

            inject(function(refundCalculationService){

                //set today to be 'today' in voucher. remember month is zero indexed!
                var baseTime = new Date(2016, 8, 31);
                jasmine.clock().mockDate(baseTime);

                var merchant = JSON.parse(window.__html__['test/fixtures/merchant_benetton.json']);
                var voucher = JSON.parse(window.__html__['test/fixtures/voucher_benetton1.json']);

                var calculationResult = refundCalculationService.calculateRefund(merchant, voucher);

                expect(calculationResult.status).toEqual(refundCalculationService.CALCULATION_STATUS.OK);
                console.log(calculationResult.totalRefund);
                expect(calculationResult.totalRefund.toFixed(2)).toEqual('626.50');
            })
        );




    });

});
