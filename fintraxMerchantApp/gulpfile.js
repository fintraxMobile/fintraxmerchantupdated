var gulp = require('gulp');
var plug = require('gulp-load-plugins')();
var paths = require('./gulp.config.json');
var merge = require('merge-stream');
var plato = require('plato');
var del = require('del');
var log = plug.util.log;
var glob = require('glob');
var env = process.env;

/**
 * List the available gulp tasks
 */
gulp.task('help', plug.taskListing);

/**
 * Lint the code, create coverage report, and a visualizer
 * @return {Stream}
 */
gulp.task('analyze', function() {
    log('Analyzing source with JSHint and Plato');

    var jshint = analyzejshint([].concat(paths.js));
    //var jscs = analyzejscs([].concat(paths.js, paths.nodejs));

    startPlatoVisualizer();

    return merge(jshint);
});

/**
 * Create $templateCache from the html templates
 * @return {Stream}
 */
gulp.task('templatecache', function() {
    log('Creating an AngularJS $templateCache');

    return gulp
        .src(paths.htmltemplates)
        // .pipe(plug.bytediff.start())
        .pipe(plug.minifyHtml({
            empty: true
        }))
        // .pipe(plug.bytediff.stop(bytediffFormatter))
        .pipe(plug.angularTemplatecache('templates.js', {
            module: 'merchantApp.core',
            standalone: false,
            root: 'app/'
        }))
        .pipe(gulp.dest(paths.build));
});

/**
 * Minify and bundle the app's JavaScript
 * @return {Stream}
 */
gulp.task('js', ['analyze', 'templatecache'], function() {
    log('Bundling, minifying, and copying the app\'s JavaScript');

    var source = [].concat(paths.js, paths.build + 'templates.js');
    return gulp
        .src(source)
        .pipe(plug.concat('app.js'))
        .pipe(plug.ngAnnotate({
            add: true,
            single_quotes: true
        }))
       /*
       //no need to minify for cordova
        .pipe(plug.bytediff.start())
        .pipe(plug.uglify({
            mangle: true
        }))
        .pipe(plug.bytediff.stop(bytediffFormatter))
        */
        .pipe(gulp.dest(paths.build + "js"));
});

/**
 * Copy the Vendor JavaScript
 * @return {Stream}
 */
gulp.task('vendorjs', function() {
    log('Bundling, minifying, and copying the Vendor JavaScript');

    return gulp.src(paths.vendorjs, { base:'./src/' } )
        //.pipe(plug.concat('vendor.js'))
        //.pipe(plug.bytediff.start())
        //.pipe(plug.uglify())
        //.pipe(plug.bytediff.stop(bytediffFormatter))
        .pipe(gulp.dest(paths.build));
});

/**
 * Copy the Config JavaScript
 * @return {Stream}
 */
gulp.task('config', function() {
    log('Copying the Config JavaScript');

    return gulp.src(['./src/config/*.js'], { base:'./src/' } )
        .pipe(gulp.dest(paths.build));
});

function onPlumberError(error){
    log('plumber found a leak! ' + error);

    this.emit('end');
}

/**
 * Turn sass into css in the main app folder
 */
gulp.task('sass', function () {
	return gulp.src(paths.sass)
        .pipe(plug.plumber(onPlumberError))
	    .pipe(plug.sass())
	    .pipe(plug.autoprefixer({
            browsers: ['chrome >= 45', 'and_chr >= 45', 'ios >= 7', 'safari >= 9'],
            cascade: false
        }))
        .pipe(plug.plumber.stop())
    	.pipe(gulp.dest(paths.cssdest));
});

/**
 * Minify and bundle the CSS
 * @return {Stream}
 */
gulp.task('css', function() {
    log('Bundling, minifying, and copying the app\'s CSS');

    return gulp.src(paths.css)
        .pipe(plug.concat('all.min.css')) // Before bytediff or after
        .pipe(plug.bytediff.start())
        .pipe(plug.minifyCss({}))
        .pipe(plug.bytediff.stop(bytediffFormatter))
        .pipe(gulp.dest(paths.build + "css"));
});

/**
 * Minify and bundle the Vendor CSS
 * @return {Stream}
 */
gulp.task('vendorcss', function() {
    log('Compressing, bundling, copying vendor CSS');

    var vendorFilter = plug.filter(['**/*.css']);

    return gulp.src(paths.vendorcss)
        .pipe(vendorFilter)
        .pipe(plug.concat('vendor.min.css'))
        .pipe(plug.bytediff.start())
        .pipe(plug.minifyCss({}))
        .pipe(plug.bytediff.stop(bytediffFormatter))
        .pipe(gulp.dest(paths.build + "css"));
});

/**
 * Copy fonts
 * @return {Stream}
 */
gulp.task('fonts', function() {
    var dest = paths.build + 'fonts';
    log('Copying fonts');
    return gulp
        .src(paths.fonts)
        .pipe(gulp.dest(dest));
});


/**
 * Compress images
 * @return {Stream}
 */
gulp.task('images', function() {
    var dest = paths.build + 'images';
    log('copying images');
    return gulp
        .src(paths.images)
        .pipe(gulp.dest(dest));
});

/*
 * Create spritemap from flags
 */
gulp.task('sprite', function () {
    var spriteData = gulp.src(paths.flags).pipe(plug.spritesmith({
        imgName: 'flags.png',
        imgPath: '/images/flags.png',
        cssName: 'flags.css',
        algorithm: 'top-down',
        //cssVarMap: function (sprite) {
        //    sprite.name = 'flags-' + sprite.name;
        //}
        cssTemplate: function (data) {  //we need bespoke output for flags.
            var css = '';
            data.sprites.forEach(function (sprite) {
                css += '.icon-flags-' + sprite.name + '{\n'
                + 'background: url(../images/flags.png) no-repeat right ' + (sprite.offset_y - 6) + 'px;\n'
                + '}';
            });
            return css;
        }
    }));
    spriteData.img.pipe(gulp.dest(paths.flagspritemapdir));
    spriteData.css.pipe(gulp.dest(paths.flagsassdir));
});

/**
 * Languages
 * @return {Stream}
 */
gulp.task('languages', function () {
    var dest = paths.build + 'app/languages';
    log('copying languages');
    return gulp
        .src(paths.languages)
        .pipe(gulp.dest(dest));
});


/**
 * Inject all the files into the new index.html
 * rev, but no map
 * @return {Stream}
 */
gulp.task('inject', ['js', 'config', 'vendorjs', 'css', 'vendorcss'], function() {
    log('building index.html');

    var files = [paths.build + '**/*.js', paths.build + '**/*.css'];
    var index = './src/index.html';
    var indexFilter = plug.filter(['index.html']);

    var stream = gulp
        // Write the revisioned files
        .src([].concat(files, index)) // add all built min files and index.html

        // inject the files into index.html
        .pipe(indexFilter) // filter to index.html
        .pipe(plug.preprocess({context: { BUILD_TYPE: env.BUILD_TYPE}}))
        .pipe(inject('css/vendor.min.css', 'inject-vendor'))
        .pipe(inject('css/all.min.css'))
        //.pipe(inject('js/vendor.js', 'inject-vendor'))
        .pipe(inject('js/app.js'))
        .pipe(gulp.dest(paths.build)) // write the rev files
        .pipe(indexFilter.restore()) // remove filter, back to original stream

        // replace the files referenced in index.html with the rev'd files
        .pipe(gulp.dest(paths.build)); // write the index.html file changes


    function inject(path, name) {
        var pathGlob = paths.build + path;
        var options = {
            ignorePath: paths.build.substring(1),
            read: false,
            addRootSlash:false
        };
        if (name) {
            options.name = name;
        }
        return plug.inject(gulp.src(pathGlob), options);
    }
});

/**
 * Build the optimized app
 * @return {Stream}
 */
gulp.task('build', ['sass', 'inject', 'sprite', 'images', 'languages', 'fonts']);

/**
 * Execute JSHint on given source files
 * @param  {Array} sources
 * @param  {String} overrideRcFile
 * @return {Stream}
 */
function analyzejshint(sources, overrideRcFile) {
    var jshintrcFile = overrideRcFile || './.jshintrc';
    log('Running JSHint');
    log(sources);
    return gulp
        .src(sources)
        .pipe(plug.jshint(jshintrcFile))
        .pipe(plug.jshint.reporter('jshint-stylish'));
}

/**
 * Start Plato inspector and visualizer
 */
function startPlatoVisualizer() {
    log('Running Plato');

    var files = glob.sync('./src/app/**/*.js');
    var excludeFiles = /\/src\/client\/app\/.*\.spec\.js/;

    var options = {
        title: 'Plato Inspections Report',
        exclude: excludeFiles
    };
    var outputDir = './report/plato';

    plato.inspect(files, outputDir, options, platoCompleted);

    function platoCompleted(report) {
        var overview = plato.getOverviewReport(report);
        log(overview.summary);
    }
}

gulp.task('clean', function(cb) {
    log('Cleaning: ' + plug.util.colors.blue(paths.build));

    var delPaths = [].concat(paths.build, paths.report);
    del(delPaths, cb);
});

/**
 * Watch files and build
 */
gulp.task('watch', function() {
    log('Watching all files');

    var css = ['gulpfile.js'].concat(paths.css, paths.vendorcss);
    var sass = ['gulpfile.js'].concat(paths.sass);
    var images = ['gulpfile.js'].concat(paths.images);
    var js = ['gulpfile.js'].concat(paths.js);

    gulp
        .watch(js, ['js', 'vendorjs'])
        .on('change', logWatch);

    gulp
        .watch(sass,['sass'])
        .on('change', logWatch);
    gulp
        .watch(css, ['css', 'vendorcss'])
        .on('change', logWatch);

    gulp
        .watch(images, ['images'])
        .on('change', logWatch);

    function logWatch(event) {
        log('*** File ' + event.path + ' was ' + event.type + ', running tasks...');
    }
});


/**
 * Formatter for bytediff to display the size changes after processing
 * @param  {Object} data - byte data
 * @return {String}      Difference in bytes, formatted
 */
function bytediffFormatter(data) {
    var difference = (data.savings > 0) ? ' smaller.' : ' larger.';
    return data.fileName + ' went from ' +
        (data.startSize / 1000).toFixed(2) + ' kB to ' + (data.endSize / 1000).toFixed(2) + ' kB' +
        ' and is ' + formatPercent(1 - data.percent, 2) + '%' + difference;
}

/**
 * Format a number as a percentage
 * @param  {Number} num       Number to format as a percent
 * @param  {Number} precision Precision of the decimal
 * @return {String}           Formatted percentage
 */
function formatPercent(num, precision) {
    return (num * 100).toFixed(precision);
}


console.log('environment var is - ' + JSON.stringify(env));

console.log('building for - ' + env.BUILD_TYPE);
