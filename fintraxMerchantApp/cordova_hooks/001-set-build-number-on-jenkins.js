#!/usr/bin/env node

// Save hook under `project-root/cordova_hooks
// trigger using the 'hooks' config in config.xml
//
// Don't forget to install xml2js using npm
// `$ npm install xml2js`

var fs = require('fs');
var xml2js = require('xml2js');

console.log('Running build number cordova hook');

// Read config.xml
var config = fs.readFileSync('config.xml', 'utf8');

    // Parse XML to JS Obj
    xml2js.parseString(config, function (err, result) {

    if(err) {
      return console.log(err);
    }

    // Get JS Obj
    var obj = result;

    // ios-CFBundleVersion doen't exist in config.xml
    if(typeof obj['widget']['$']['ios-CFBundleVersion'] === 'undefined') {
      obj['widget']['$']['ios-CFBundleVersion'] = 0;
    }

    // android-versionCode doen't exist in config.xml
    if(typeof obj['widget']['$']['android-versionCode'] === 'undefined') {
      obj['widget']['$']['android-versionCode'] = 0;
    }

    if(process.env.BUILD_NUMBER){
      // Increment build numbers (separately for iOS and Android)
      obj['widget']['$']['ios-CFBundleVersion'] = process.env.BUILD_NUMBER;
      obj['widget']['$']['android-versionCode'] = process.env.BUILD_NUMBER;

      // Build XML from JS Obj
      var builder = new xml2js.Builder();
      var xml = builder.buildObject(obj);

      // Write config.xml
      fs.writeFileSync('config.xml', xml);
      console.log('Build number automaticaly set to ' + process.env.BUILD_NUMBER);

    } else {

      console.log('BUILD_NUMBER env var not found, assuming not on jenkins, skipping cordova build number step');

    }

});
