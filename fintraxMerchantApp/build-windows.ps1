# Windows build script
# Builds gulp for dev and live, then invokes cordova
# Will build outputs with version code as per windows-packageVersion in the root config.xml
Add-Type -A 'System.IO.Compression.FileSystem';
[xml]$CordovaConfig = Get-Content -Path config.xml
$BuildNumber = $CordovaConfig.widget.'windows-packageVersion'
$Path = 'platforms\windows\AppPackages\CordovaApp.Windows_' + $BuildNumber + '_anycpu_Test\*'
Write-Host 'Building for version '+ $BuildNumber
$ScriptDir = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

# Build dev
$env:BUILD_TYPE='dev'
Remove-Item platforms\windows\AppPackages -Recurse -Force
New-Item platforms\windows\AppPackages -type directory
Invoke-Expression 'gulp build'
Invoke-Expression 'cordova build windows --release'
Remove-Item release -Recurse -Force
New-Item release\dev -type directory
Copy-Item $Path release\dev -recurse
[IO.Compression.ZipFile]::CreateFromDirectory($ScriptDir + '\release\dev', $ScriptDir + '\release\dev.zip')

# Build live
$env:BUILD_TYPE='live'
Remove-Item platforms\windows\AppPackages -Recurse -Force
New-Item platforms\windows\AppPackages -type directory
Invoke-Expression 'gulp build'
Invoke-Expression 'cordova build windows --release'
New-Item release\live -type directory
Copy-Item $Path release\live -Recurse
[IO.Compression.ZipFile]::CreateFromDirectory($ScriptDir + '\release\live', $ScriptDir + '\release\live.zip')